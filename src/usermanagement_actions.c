/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "usermanagement.h"
#include "usermanagement_util.h"
#include "dm_usermanagement.h"
#include "passwd_hash.h"
#include "file_system.h"

#include <string.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_action.h>

#define ME "action"

#define TRACE_I_ACTION(f_name, e_name) \
    SAH_TRACEZ_INFO(ME ".action", "Called action handler '%s'", f_name)

static amxd_object_t* amxd_action_add_inst_is_created(amxd_object_t* const object,
                                                      amxc_var_t* data) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* instance = NULL;

    when_false(amxc_var_type_of(data) == AMXC_VAR_ID_HTABLE, exit);
    when_null(GET_ARG(data, "index"), exit);
    instance = amxd_object_get_instance(object, NULL, GET_UINT32(data, "index"));

exit:
    SAH_TRACEZ_OUT(ME);
    return instance;
}

static uint32_t find_available_id_if_in_object(amxd_object_t* object,
                                               amxc_var_t* params,
                                               const char* parameter_name,
                                               amxd_status_t* status) {
    SAH_TRACEZ_IN(ME);
    uint32_t id = GET_UINT32(usermanagement_get_config(), "starting_uid");
    const char* alias = GET_CHAR(params, "Alias");

    do {

        if(id == UINT32_MAX) {
            *status = amxd_status_invalid_value;
            goto exit;
        }

        id++;

    } while(amxd_object_findf(object, "[%s==%d && Alias!=%s]", parameter_name, id, alias) != NULL);

    *status = amxd_status_ok;

exit:
    SAH_TRACEZ_OUT(ME);
    return id;
}

static uint32_t find_available_id(amxd_object_t* object,
                                  const char* parameter_name,
                                  amxd_status_t* status) {
    SAH_TRACEZ_IN(ME);
    uint32_t id = GET_UINT32(usermanagement_get_config(), "starting_uid");

    do {

        if(id == UINT32_MAX) {
            *status = amxd_status_invalid_value;
            goto exit;
        }

        id++;

    } while(amxd_object_findf(object, "[%s==%d]", parameter_name, id) != NULL);

    *status = amxd_status_ok;

exit:
    SAH_TRACEZ_OUT(ME);
    return id;
}

static bool is_duplicate_uid_allowed(const amxd_object_t* object) {
    bool allowed = false;
    char* path = amxd_object_get_path(object, AMXD_OBJECT_NAMED);

    when_null(path, exit);
    allowed = (strcmp(path, "Users.User") == 0);
    free(path);
exit:
    return allowed;
}

amxd_status_t _check_uniq(amxd_object_t* object,
                          amxd_param_t* param,
                          amxd_action_t reason,
                          const amxc_var_t* const args,
                          UNUSED amxc_var_t* const retval,
                          void* priv) {
    SAH_TRACEZ_IN(ME);
    TRACE_I_ACTION("_check_uniq", event_name);

    const char* parameter_name = NULL;
    char* current_value = NULL;
    char* new_value = NULL;
    amxd_object_t* p_parent = NULL;
    amxd_status_t status = amxd_status_unknown_error;
    amxc_llist_t possible_instances = {0};
    amxc_var_t value;

    amxc_var_init(&value);

    when_null(object, exit);
    when_null(args, exit);
    when_false(reason == action_param_validate, exit);

    parameter_name = amxc_var_constcast(cstring_t, (amxc_var_t*) priv);
    amxd_param_get_value(param, &value);
    current_value = amxc_var_dyncast(cstring_t, &value);

    when_null_status(current_value, exit, status = amxd_status_invalid_value);
    new_value = amxc_var_dyncast(cstring_t, args);
    when_null_status(new_value, exit, status = amxd_status_invalid_value);
    when_true_status(strcmp(current_value, new_value) == 0, exit, status = amxd_status_ok);

    p_parent = amxd_object_get_parent(object);
    when_null(p_parent, exit);
    status = amxd_object_resolve_pathf(p_parent, &possible_instances, "[%s=='%s']", parameter_name, new_value);
    when_failed_trace(status, exit, WARNING, "amxd_object_resolve_pathf failed, using the expression: [%s=='%s']", parameter_name, amxc_var_constcast(cstring_t, args));
    when_true_status(amxc_llist_size(&possible_instances) > 0, exit, status = amxd_status_duplicate);
    status = amxd_status_ok;
exit:
    amxc_var_clean(&value);
    amxc_llist_clean(&possible_instances, amxc_string_list_it_free);
    free(current_value);
    free(new_value);
    SAH_TRACEZ_OUT(ME);
    return status;
}

amxd_status_t _add_instance(amxd_object_t* object,
                            amxd_param_t* param,
                            amxd_action_t reason,
                            const amxc_var_t* const args,
                            amxc_var_t* const retval,
                            void* priv) {
    SAH_TRACEZ_IN(ME);
    TRACE_I_ACTION("_add_instance", event_name);

    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t* params = NULL;
    amxd_object_t* instance = NULL;
    uint32_t uid = 0;
    const char* parameter_name = NULL;

    when_null(object, exit);
    when_null(retval, exit);
    when_null(args, exit);
    when_null(priv, exit);
    when_false(reason == action_object_add_inst, exit);

    parameter_name = amxc_var_constcast(cstring_t, (amxc_var_t*) priv);
    instance = amxd_action_add_inst_is_created(object, retval);
    params = GET_ARG(args, "parameters");

    if(instance != NULL) {
        uid = find_available_id_if_in_object(object, params, parameter_name, &status);
        when_failed(status, exit);
        amxd_param_t* p_uid = amxd_object_get_param_def(instance, parameter_name);
        amxc_var_set(uint32_t, &p_uid->value, uid);
        status = amxd_status_ok;
    } else {
        if(GET_ARG(params, parameter_name) == NULL) {
            uid = find_available_id(object, parameter_name, &status);
            when_failed(status, exit);
            amxc_var_add_key(uint32_t, params, parameter_name, uid);
        } else if(!is_duplicate_uid_allowed(object)) {
            if(amxd_object_findf(object, "[%s==%d]", parameter_name, GET_UINT32(params, parameter_name)) != NULL) {
                status = amxd_status_duplicate;
                goto exit;
            }
        }
        status = amxd_action_object_add_inst(object, param, reason, args, retval, priv);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

amxd_status_t _delete_instance_if_false(amxd_object_t* object,
                                        UNUSED amxd_param_t* param,
                                        amxd_action_t reason,
                                        const amxc_var_t* const args,
                                        UNUSED amxc_var_t* const retval,
                                        void* priv) {
    SAH_TRACEZ_IN(ME);
    TRACE_I_ACTION("_delete_instance_if_false", event_name);

    amxd_status_t status = amxd_status_unknown_error;
    const char* parameter_name = NULL;
    uint32_t instance_index = 0;
    amxd_object_t* instance_to_delete = NULL;
    bool delete_not_allowed = true;

    when_false_status(reason == action_object_del_inst, exit,
                      status = amxd_status_function_not_implemented);
    when_null_status(priv, exit, status = amxd_status_invalid_function_argument);

    parameter_name = amxc_var_constcast(cstring_t, (amxc_var_t*) priv);
    instance_index = GET_UINT32(args, "index");

    instance_to_delete = amxd_object_get_instance(object, NULL, instance_index);
    when_null_status(instance_to_delete, exit, status = amxd_status_object_not_found);
    delete_not_allowed = amxd_object_get_bool(instance_to_delete, parameter_name, &status);
    when_failed(status, exit);

    when_true_status(delete_not_allowed, exit, status = amxd_status_invalid_action);
    status = amxd_status_ok;

exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

amxd_status_t _check_home_directory_permission(amxd_object_t* object,
                                               UNUSED amxd_param_t* param,
                                               amxd_action_t reason,
                                               UNUSED const amxc_var_t* const args,
                                               UNUSED amxc_var_t* const retval,
                                               UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    TRACE_I_ACTION("_check_home_directory_permission", event_name);

    userdata_t userdata = {0};
    amxc_var_t params;
    amxd_status_t status = amxd_status_invalid_arg;

    amxc_var_init(&params);
    init_userdata(&userdata);

    when_false_status(reason == action_object_validate, exit, status = amxd_status_function_not_implemented);
    amxd_object_get_params(object, &params, amxd_dm_access_protected);
    when_str_empty_status(GET_CHAR(&params, "Alias"), exit, status = amxd_status_ok);

    fill_user_data_from_dm(&userdata, object);

    when_true_status(STRING_EMPTY(userdata.home_dir), exit, status = amxd_status_ok);
    when_true_status(is_shared_path(userdata.home_dir), exit, status = amxd_status_ok);
    when_true_status(is_forbidden_path(userdata.home_dir), exit, status = amxd_status_invalid_path);
    when_false_trace(fs_check_directory_permissions(userdata.home_dir, &userdata), exit, ERROR, "Failed set home dir(%s), permission denied", userdata.home_dir);

    status = amxd_status_ok;
exit:
    if(status == amxd_status_invalid_path) {
        SAH_TRACEZ_ERROR(ME, "User %s can't use protected directory: %s", userdata.username, userdata.home_dir);
    }
    amxc_var_clean(&params);
    clean_userdata(&userdata);
    SAH_TRACEZ_OUT(ME);
    return status;
}
