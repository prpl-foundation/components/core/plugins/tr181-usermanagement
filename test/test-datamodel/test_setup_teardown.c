/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "test_datamodel.h"
#include "dummy_backend.h"

#define ODL_MOCK_FILEPATH "../mock_odl/mock.odl"
#define ODL_DEFINITION_FILEPATH "../../odl/tr181-usermanagement_definition.odl"

#include "stdlib.h"

typedef struct _test_state_t {
    amxd_dm_t m_datamodel;
    amxo_parser_t m_parser;
    amxb_bus_ctx_t* mp_bus_ctx;
} test_state_t;

static test_state_t test_state = {
    .mp_bus_ctx = NULL,
};

amxd_dm_t* get_test_datamodel() {
    return &test_state.m_datamodel;
}

amxo_parser_t* get_test_parser() {
    return &test_state.m_parser;
}

amxb_bus_ctx_t* get_test_bus_ctx() {
    return test_state.mp_bus_ctx;
}

int test_setup_dummy_backend() {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&test_state.m_datamodel), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&test_state.m_parser), 0);

    test_register_dummy_be();

    root_obj = amxd_dm_get_root(&test_state.m_datamodel);
    assert_non_null(root_obj);

    // resolver_add_all_functions

    assert_int_equal(amxb_connect(&test_state.mp_bus_ctx, "dummy:/tmp/dummy.sock"), 0);

    amxo_connection_add(&test_state.m_parser,
                        amxb_get_fd(test_state.mp_bus_ctx),
                        connection_read,
                        "dummy:/tmp/dummy.sock",
                        AMXO_BUS,
                        test_state.mp_bus_ctx);
    amxb_register(test_state.mp_bus_ctx, &test_state.m_datamodel);

    assert_int_equal(amxo_parser_parse_file(&test_state.m_parser, ODL_MOCK_FILEPATH, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&test_state.m_parser, ODL_DEFINITION_FILEPATH, root_obj), 0);

    amxp_sigmngr_add_signal(NULL, "connection-deleted");

    while(amxp_signal_read() == 0) {
    }

    return 0;
}

int test_teardown_dummy_backend(UNUSED void** state) {
    amxb_free(&test_state.mp_bus_ctx);
    test_unregister_dummy_be();
    amxo_parser_clean(&test_state.m_parser);
    amxd_dm_clean(&test_state.m_datamodel);

    return 0;
}
