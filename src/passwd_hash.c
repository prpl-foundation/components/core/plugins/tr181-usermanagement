/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <errno.h>
#include <string.h>
#include <amxc/amxc_macros.h>

#include "usermanagement.h"
#include "passwd_hash.h"

#define ME "passwd"

static bool generate_salt(char salt[SALT_LENGTH]) {
    SAH_TRACEZ_IN(ME);
    bool rc = false;
    int i;
    char printables[] = SALT_CHAR_PRINTABLES;

    FILE* rand = fopen("/dev/urandom", "r");
    when_null_trace(rand, error, ERROR, "unable to open /dev/urandom: %s", strerror(errno));

    for(i = 0; i < SALT_LENGTH - 1; i++) {
        int c = fgetc(rand);
        when_true_trace(c < 0, error_close_rand, ERROR, "unable to read /dev/urandom: %s", strerror(errno));
        salt[i] = printables[c % (sizeof(printables) - 1)];
    }
    salt[SALT_LENGTH - 1] = '\0';

    rc = true;

error_close_rand:
    fclose(rand);
error:
    SAH_TRACEZ_OUT(ME);
    return rc;
}

const char* generate_password_hash(const char* const username, const char* const password) {
    SAH_TRACEZ_IN(ME);
    (void) username;
    char option[HASH_ALGORITHM_LENGTH + SALT_LENGTH];
    char salt[SALT_LENGTH];
    const char* hash = NULL;

    salt[0] = '\0';

    when_str_empty_status(password, exit, hash = PASSWORD_EMPTY_STR);
    when_true(strcmp(password, PASSWD_INVALID_STR) == 0, exit);
    when_false(generate_salt(salt), exit);

    if(strlen(salt) != 0) {
        snprintf(option, HASH_ALGORITHM_LENGTH + SALT_LENGTH, "$%.2s$%s", PASSWORD_HASH_ALGO_STR, salt);
    }

    errno = 0;
    hash = crypt(password, option);
    when_null_trace(hash, exit, ERROR, "Failed (%d) to hash password of %s. Locking password!", errno, username);

exit:
    if(hash == NULL) {
        hash = PASSWORD_LOCKED_STR;
    }
    SAH_TRACEZ_OUT(ME);
    return hash;
}

password_descriptor_t* password_descriptor_new(void) {
    SAH_TRACEZ_IN(ME);
    password_descriptor_t* password_entry = (password_descriptor_t*) calloc(1, sizeof(password_descriptor_t));
    when_null_oom(password_entry, exit, "password_entry");

exit:
    SAH_TRACEZ_OUT(ME);
    return password_entry;
}

void password_descriptor_delete(password_descriptor_t* password_entry) {
    SAH_TRACEZ_IN(ME);
    when_null(password_entry, exit);

    if((password_entry)->hash != NULL) {
        free((password_entry)->hash);
    }

    if((password_entry)->algo != NULL) {
        free((password_entry)->algo);
    }

    if((password_entry)->salt != NULL) {
        free((password_entry)->salt);
    }

    free(password_entry);
    password_entry = NULL;

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

password_descriptor_t* extract_password_descriptor(const char* password_entry) {
    SAH_TRACEZ_IN(ME);

    char* password_entry_copy = NULL;
    char* token = NULL;
    password_descriptor_t* password_desc = NULL;

    when_null_trace(password_entry, cleanup, ERROR, "password entry is NULL");

    password_desc = password_descriptor_new();
    when_null_trace(password_desc, cleanup, ERROR, "password descriptor allocation failed");

    password_entry_copy = strdup(password_entry);
    when_null_oom(password_entry_copy, cleanup, "password entry copy");

    token = strtok(password_entry_copy, "$");
    when_null_trace(token, cleanup, ERROR, "Error: password entry mal formatted");

    password_desc->algo = strdup(token);
    when_null_oom(password_desc->algo, cleanup, "algo");

    token = strtok(NULL, "$");
    when_null_trace(token, cleanup, ERROR, "Error: password entry mal formatted");
    password_desc->salt = strdup(token);
    when_null_oom(password_desc->salt, cleanup, "salt");

    token = strtok(NULL, "$");
    when_null_trace(token, cleanup, ERROR, "Error: password entry mal formatted");
    password_desc->hash = strdup(token);
    when_null_oom(password_desc->hash, cleanup, "hash");

    free(password_entry_copy);

    SAH_TRACEZ_OUT(ME);
    return password_desc;

cleanup:

    if(password_entry_copy != NULL) {
        free(password_entry_copy);
    }

    password_descriptor_delete(password_desc);

    SAH_TRACEZ_OUT(ME);
    return NULL;
}
