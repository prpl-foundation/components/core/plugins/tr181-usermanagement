include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src all

clean:
	$(MAKE) -C src clean
	$(MAKE) -C test clean

install: all
	$(INSTALL) -D -p -m 0644 odl/tr181-usermanagement_definition.odl $(DEST)/etc/amx/tr181-usermanagement/tr181-usermanagement_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(DEST)/etc/amx/tr181-device/extensions/01_device-usermanagement_mapping.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-usermanagement_user.odl $(DEST)/etc/amx/tr181-usermanagement/tr181-usermanagement_user.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-usermanagement_group.odl $(DEST)/etc/amx/tr181-usermanagement/tr181-usermanagement_group.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-usermanagement_role.odl $(DEST)/etc/amx/tr181-usermanagement/tr181-usermanagement_role.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-usermanagement_supportedshell.odl $(DEST)/etc/amx/tr181-usermanagement/tr181-usermanagement_supportedshell.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-usermanagement.odl $(DEST)/etc/amx/tr181-usermanagement/tr181-usermanagement.odl
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/tr181-usermanagement/tr181-usermanagement_defaults.d
	$(INSTALL) -D -p -m 0644 odl/tr181-usermanagement_defaults.d/* $(DEST)/etc/amx/tr181-usermanagement/tr181-usermanagement_defaults.d/
	$(INSTALL) -D -p -m 0660 acl/admin/$(COMPONENT).json $(DEST)$(ACLDIR)/admin/$(COMPONENT).json
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/usermanagement.so $(DEST)/usr/lib/amx/tr181-usermanagement/tr181-usermanagement.so
	$(INSTALL) -d -m 0755 $(DEST)$(BINDIR)
	ln -sfr $(DEST)$(BINDIR)/amxrt $(DEST)$(BINDIR)/tr181-usermanagement
	$(INSTALL) -D -p -m 0755 scripts/tr181-usermanagement.sh $(DEST)$(INITDIR)/$(COMPONENT)
	$(INSTALL) -d -m 0755 $(DEST)//usr/share/acl.d
	$(INSTALL) -D -p -m 0644 acl/ubus/*.json $(DEST)/usr/share/acl.d/

package: all
	$(INSTALL) -D -p -m 0644 odl/tr181-usermanagement_definition.odl $(PKGDIR)/etc/amx/tr181-usermanagement/tr181-usermanagement_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(PKGDIR)/etc/amx/tr181-device/extensions/01_device-usermanagement_mapping.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-usermanagement_user.odl $(PKGDIR)/etc/amx/tr181-usermanagement/tr181-usermanagement_user.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-usermanagement_group.odl $(PKGDIR)/etc/amx/tr181-usermanagement/tr181-usermanagement_group.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-usermanagement_role.odl $(PKGDIR)/etc/amx/tr181-usermanagement/tr181-usermanagement_role.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-usermanagement_supportedshell.odl $(PKGDIR)/etc/amx/tr181-usermanagement/tr181-usermanagement_supportedshell.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-usermanagement.odl $(PKGDIR)/etc/amx/tr181-usermanagement/tr181-usermanagement.odl
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/tr181-usermanagement/tr181-usermanagement_defaults.d
	$(INSTALL) -D -p -m 0644 odl/tr181-usermanagement_defaults.d/* $(PKGDIR)/etc/amx/tr181-usermanagement/tr181-usermanagement_defaults.d/
	$(INSTALL) -D -p -m 0660 acl/admin/$(COMPONENT).json $(PKGDIR)$(ACLDIR)/admin/$(COMPONENT).json
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/usermanagement.so $(PKGDIR)/usr/lib/amx/tr181-usermanagement/tr181-usermanagement.so
	$(INSTALL) -d -m 0755 $(PKGDIR)$(BINDIR)
	rm -f $(PKGDIR)$(BINDIR)/tr181-usermanagement
	ln -sfr $(PKGDIR)$(BINDIR)/amxrt $(PKGDIR)$(BINDIR)/tr181-usermanagement
	$(INSTALL) -D -p -m 0755 scripts/tr181-usermanagement.sh $(PKGDIR)$(INITDIR)/$(COMPONENT)
	$(INSTALL) -d -m 0755 $(PKGDIR)//usr/share/acl.d
	$(INSTALL) -D -p -m 0644 acl/ubus/*.json $(PKGDIR)/usr/share/acl.d/
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

doc:
	$(eval ODLFILES += odl/tr181-usermanagement_definition.odl)
	$(eval ODLFILES += odl/$(COMPONENT)_mapping.odl)
	$(eval ODLFILES += odl/tr181-usermanagement_user.odl)
	$(eval ODLFILES += odl/tr181-usermanagement_group.odl)
	$(eval ODLFILES += odl/tr181-usermanagement_role.odl)
	$(eval ODLFILES += odl/tr181-usermanagement_supportedshell.odl)
	$(eval ODLFILES += odl/tr181-usermanagement.odl)

	mkdir -p output/xml
	mkdir -p output/html
	mkdir -p output/confluence
	amxo-cg -Gxml,output/xml/$(COMPONENT).xml $(or $(ODLFILES), "")
	amxo-xml-to -x html -o output-dir=output/html -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml
	amxo-xml-to -x confluence -o output-dir=output/confluence -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml

test:
	$(MAKE) -C test run
	$(MAKE) -C test coverage

.PHONY: all clean changelog install package doc test