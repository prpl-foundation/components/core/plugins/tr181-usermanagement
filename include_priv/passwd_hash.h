/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef __PASSWD_HASH_H__
#define __PASSWD_HASH_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>

#ifdef DISABLE_SHA512
#define PASSWORD_HASH_ALGO_STR "1"
#else
#define PASSWORD_HASH_ALGO_STR "6"
#endif

// Related to special value use in /etc/shadow sysconf file.
#define PASSWORD_EMPTY_STR ""     // use to set user without password
#define PASSWORD_LOCKED_STR "*"   // use to lock a account in /etc/shadow
#define PASSWORD_DISABLE_STR "!"  // use to disable a user in /etc/shadow

// Data model particular values
#define PASSWORD_SPECIAL_STR "\a" // Use unpritable string to be able the be triggered when clear password is erase by the pluggin
#define PASSWD_INVALID_STR "\x7f" // can be use in the default odl to lock the user, it will result with "*" in /etc/shadow

#define HASH_ALGORITHM_LENGTH 5   // $hash_algo$
#define SALT_LENGTH 64
#define SALT_CHAR_PRINTABLES "1234567890" "abcdef"

typedef struct password_descriptor {
    char* algo;
    char* salt;
    char* hash;
} password_descriptor_t;

/**
 * Generate a password hash suited for the /etc/shadow file
 *
 * If no valid password is given we default to an invalid hash (e.g. "*" or "!").
 * Invalid hashes are used to lock/block the user's password.
 *
 * @param username The username of the corresponding hash (used for logging only)
 * @param password The password that has to be hashed.The return value points to static
 *                 data whose content is overwritten by each call.
 *
 * @return a char array containing the hash.
 **/
const char* generate_password_hash(const char* const username, const char* const password);

/**
   @brief
   Allocates a password descriptor.

   A password descriptor is a container that hold password informations like algorithm, salt and hash.

   @note
   The allocated memory must be freed when not used anymore.
   use @ref password_descriptor_delete to free the memory.

   @return
   When allocation is successfull, this functions returns a pointer that points to the new allocated password descriptor.
 */
password_descriptor_t* password_descriptor_new(void);

/**
   @brief
   Frees the previously allocated password desciptor.

   Frees the allocated memory and sets the pointer to NULL.

   @ref amxc_var_new

   @param var a pointer to the location where the pointer to the password descriptor is stored
 */
void password_descriptor_delete(password_descriptor_t* password_entry);

/**
   @brief
   Extract the password desciptor (algo, salt and hash)  from the password entry, typically the entry
   located in /etc/passwd or /etc/shadow.

   @param password_entry pointer to a string containing a entry for a user in /etc/passwd ("$algo$salt$hash")

   @return
   When extraction is successful, this functions return a pointer to a password descriptor that must be freed.
   On failure, a NULL pointer is returned.

   @ref password_descriptor_delete
   @ref password_descriptor_new
 */
password_descriptor_t* extract_password_descriptor(const char* password_entry);


#ifdef __cplusplus
}
#endif

#endif // __PASSWD_HASH_H__