/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "test_datamodel.h"
#include "util.h"

static int test_setup_plugin(UNUSED void** state) {
    assert_int_equal(_usermanagement_main(AMXO_START, get_test_datamodel(), get_test_parser()), 0);
    return 0;
}

static int test_teardown_plugin(UNUSED void** state) {
    assert_int_equal(_usermanagement_main(AMXO_STOP, get_test_datamodel(), get_test_parser()), 0);
    return 0;
}

int main(void) {

    int status = 0;

    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_usermanagement_start),
        cmocka_unit_test(test_usermanagement_stop),
        cmocka_unit_test(test_entrypoint_unhandled_reason)
    };

    const struct CMUnitTest tests_group[] = {
        cmocka_unit_test(test_check_delete_group),
        cmocka_unit_test(test_check_update_id),
        cmocka_unit_test(test_add_group),
        cmocka_unit_test(test_add_group_with_empty_role),
        cmocka_unit_test_setup_teardown(test_edit_group_id, test_setup_plugin, test_teardown_plugin),
        cmocka_unit_test_setup_teardown(test_edit_group_name, test_setup_plugin, test_teardown_plugin),
        cmocka_unit_test_setup_teardown(test_edit_group_role, test_setup_plugin, test_teardown_plugin),
    };

    const struct CMUnitTest tests_user[] = {
        cmocka_unit_test_setup_teardown(test_add_user_with_empty_username, test_setup_plugin, test_teardown_plugin),
        cmocka_unit_test_setup_teardown(test_add_user_with_default_hashed_password, test_setup_plugin, test_teardown_plugin),
        cmocka_unit_test_setup_teardown(test_add_user_with_default_clear_password, test_setup_plugin, test_teardown_plugin),
        cmocka_unit_test_setup_teardown(test_add_user_with_group_and_role, test_setup_plugin, test_teardown_plugin),
        cmocka_unit_test_setup_teardown(test_add_user_with_empty_group_and_role, test_setup_plugin, test_teardown_plugin),
        cmocka_unit_test_setup_teardown(test_edit_user_homedir, test_setup_plugin, test_teardown_plugin),
        cmocka_unit_test_setup_teardown(test_edit_user_shell, test_setup_plugin, test_teardown_plugin),
        cmocka_unit_test_setup_teardown(test_edit_user_password, test_setup_plugin, test_teardown_plugin),
        cmocka_unit_test_setup_teardown(test_edit_user_password_empty, test_setup_plugin, test_teardown_plugin),
        cmocka_unit_test_setup_teardown(test_edit_user_hashed_password, test_setup_plugin, test_teardown_plugin),
        cmocka_unit_test_setup_teardown(test_edit_user_role_participation, test_setup_plugin, test_teardown_plugin),
        cmocka_unit_test_setup_teardown(test_edit_user_group_participation, test_setup_plugin, test_teardown_plugin),
        cmocka_unit_test_setup_teardown(test_edit_username, test_setup_plugin, test_teardown_plugin),
        cmocka_unit_test_setup_teardown(test_edit_empty_username, test_setup_plugin, test_teardown_plugin),
        cmocka_unit_test_setup_teardown(test_edit_userid, test_setup_plugin, test_teardown_plugin),
        cmocka_unit_test_setup_teardown(test_disable_user, test_setup_plugin, test_teardown_plugin),
        cmocka_unit_test_setup_teardown(test_enable_user, test_setup_plugin, test_teardown_plugin),
        cmocka_unit_test_setup_teardown(test_add_user_with_bus_access_role, test_setup_plugin, test_teardown_plugin),
        cmocka_unit_test_setup_teardown(test_edit_user_bus_access_role, test_setup_plugin, test_teardown_plugin),
    };

    status = cmocka_run_group_tests_name("test usermanagement", tests, test_setup_dummy_backend, test_teardown_dummy_backend);
    status += cmocka_run_group_tests_name("test usermanagement group", tests_group, test_setup_dummy_backend, test_teardown_dummy_backend);
    status += cmocka_run_group_tests_name("test usermanagement password", tests_user, test_setup_dummy_backend, test_teardown_dummy_backend);

    return status;
}
