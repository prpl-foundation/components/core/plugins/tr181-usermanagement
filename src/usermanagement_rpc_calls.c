/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**********************************************************
* Include files
**********************************************************/

#include <syslog.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "usermanagement.h"
#include "usermanagement_util.h"
#include "rpc_check_credentials.h"
#include "dm_usermanagement.h"
#include "usermanagement_rpc_calls.h"
#include "passwd_hash.h"

#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <debug/sahtrace.h>
#include "sdw_entry.h"

/**********************************************************
* Macro definitions
**********************************************************/
#define ME "rpc"

/**********************************************************
* Type definitions
**********************************************************/

/**********************************************************
* Variable declarations
**********************************************************/

const char* string_check_credentials_status[check_credentials_num_of_status] = {
    "Credentials_Good",
    "Credentials_Bad_Requested_Username_Not_Supported",
    "Credentials_Bad_Requested_Password_Incorrect",
    "Credentials_Missing",
    "Error_Invalid_Input",
    "Error_Other"
};

/**********************************************************
* Function Prototypes
**********************************************************/

/**********************************************************
* Functions
**********************************************************/

/**
   @brief
   Get password from User object.

   @param[in] p_user_object pointer to a object containing the user parameters.
   @param[in] is_hashed boolean indicate whenever the password to retrieve is hashed or not.
   @param[out] ret_status amxd_status_ok when the there is no error in the function.

   @return
   Password in string format.
 */
static cstring_t get_password_from_user(amxd_object_t* p_user_object, amxd_status_t* ret_status) {
    SAH_TRACEZ_IN(ME);
    const char* parameter = get_hashed_password_path();
    const cstring_t user_password = GET_CHAR(amxd_object_get_param_value(p_user_object, parameter), NULL);
    if(user_password == NULL) {
        SAH_TRACEZ_ERROR(ME, "User's password is NULL.");
        *ret_status = amxd_status_parameter_not_found;
        goto exit;
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return (cstring_t) user_password;
}

/**
   @brief
   Validate username.

   @param[in] username Username in string format.

   @return
   true if it's a valid username input.
 */
static bool validate_str_username(const cstring_t username) {
    SAH_TRACEZ_IN(ME);
    bool valid_str = false;

    when_null_trace(username, exit, ERROR, "Username argument is NULL.");
    when_str_empty(username, exit);
    when_true(strlen(username) > 64, exit);
    valid_str = true;
exit:
    SAH_TRACEZ_OUT(ME);
    return valid_str;
}

/**
   @brief
   Validate password.

   @param[in] password Password in string format.
   @param[in] is_hashed Boolean indicate if password is hashed or not.

   @return
   true if it's a valid password input.
 */
static bool validate_str_password(const cstring_t password, bool is_hashed) {
    SAH_TRACEZ_IN(ME);
    bool valid_str = false;

    when_null_trace(password, exit, ERROR, "Password argument is NULL.");
    when_true(!is_hashed && strlen(password) > 64, exit);
    valid_str = true;
exit:
    SAH_TRACEZ_OUT(ME);
    return valid_str;
}

static const cstring_t compute_password(bool is_hashed, const cstring_t input_password, cstring_t user_password_expected, amxc_var_t* args) {
    SAH_TRACEZ_IN(ME);
    password_descriptor_t* password_descriptor = NULL;
    const cstring_t password_formatted = NULL;
    char option[HASH_ALGORITHM_LENGTH + SALT_LENGTH];

    when_null_trace(args, exit, ERROR, "args object is NULL.");

    if(is_hashed) {
        password_formatted = input_password;
    } else {
        password_descriptor = extract_password_descriptor(user_password_expected);
        if(password_descriptor == NULL) {
            SAH_TRACEZ_ERROR(ME, "Failed to retrieved algo / salt / hash from stored password");
            amxc_var_add_key(cstring_t, args, STATUS_KEY, CREDENTIAL_STATUS(error_other));
            goto exit;
        }

        if((password_descriptor->algo == NULL) && (password_descriptor->salt == NULL)) {
            SAH_TRACEZ_ERROR(ME, "Failed to retrieved algo / salt / hash from stored password");
            amxc_var_add_key(cstring_t, args, STATUS_KEY, CREDENTIAL_STATUS(error_other));
            goto exit;
        }

        snprintf(option, HASH_ALGORITHM_LENGTH + SALT_LENGTH, "$%s$%s", password_descriptor->algo, password_descriptor->salt);
        password_formatted = crypt(input_password, option);
    }

exit:
    if(password_descriptor != NULL) {
        password_descriptor_delete(password_descriptor);
    }

    SAH_TRACEZ_OUT(ME);
    return password_formatted;
}

static bool is_user_enabled(amxd_object_t* p_user_object) {
    SAH_TRACEZ_IN(ME);
    bool state = GET_BOOL(amxd_object_get_param_value(p_user_object, DM_ENABLE_NAME), NULL);
    SAH_TRACEZ_OUT(ME);
    return state;
}

amxd_status_t _CheckCredentialsDiagnostics(UNUSED amxd_object_t* object, UNUSED amxd_function_t* func, amxc_var_t* args, UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t ret_status = amxd_status_ok;
    amxd_object_t* p_user_object = NULL;
    amxd_object_t* users = NULL;
    bool valid_str = false;
    cstring_t user_password_expected = NULL;
    const char* password_formatted = NULL;
    const cstring_t input_username = GET_CHAR(args, CHECK_CREDENTIALS_USERNAME);
    const cstring_t input_password = GET_CHAR(args, CHECK_CREDENTIALS_PASSWORD);
    bool is_hashed = GET_BOOL(args, CHECK_CREDENTIALS_IS_HASHED);

    //Validate input arguments
    valid_str = validate_str_username(input_username);
    if(valid_str == false) {
        SAH_TRACEZ_ERROR(ME, "Failed to read the username input.");
        amxc_var_add_key(cstring_t, args, STATUS_KEY, CREDENTIAL_STATUS(error_invalid_input));
        goto exit;
    }

    valid_str = validate_str_password(input_password, is_hashed);
    if(valid_str == false) {
        SAH_TRACEZ_ERROR(ME, "Failed to read the password input.");
        amxc_var_add_key(cstring_t, args, STATUS_KEY, CREDENTIAL_STATUS(error_invalid_input));
        goto exit;
    }

    //Get Users object that contains the list of Users
    users = amxd_dm_findf(usermanagement_get_dm(), "Users.User.");
    if(users == NULL) {
        SAH_TRACEZ_ERROR(ME, "Users object is NULL.");
        amxc_var_add_key(cstring_t, args, STATUS_KEY, CREDENTIAL_STATUS(error_other));
        goto exit;
    }

    //Find user with the username
    p_user_object = amxd_object_findf(users, "[" DM_USERNAME " == '%s']", input_username);
    if(p_user_object == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to find user with Username = %s.", input_username);
        amxc_var_add_key(cstring_t, args, STATUS_KEY, CREDENTIAL_STATUS(credentials_missing));
        goto exit;
    }

    //Check is the user is enabled
    if(!is_user_enabled(p_user_object)) {
        SAH_TRACEZ_WARNING(ME, "Warning user %s is disabled", input_username);
        amxc_var_add_key(cstring_t, args, STATUS_KEY, CREDENTIAL_STATUS(error_other));
        ret_status = amxd_status_ok;
        goto exit;
    }

    //Compare the passwords
    user_password_expected = get_password_from_user(p_user_object, &ret_status);
    if((ret_status != amxd_status_ok) || (user_password_expected == NULL)) {
        SAH_TRACEZ_ERROR(ME, "Failed to get the Password parameter from the User object.");
        amxc_var_add_key(cstring_t, args, STATUS_KEY, CREDENTIAL_STATUS(error_other));
        ret_status = amxd_status_ok;
        goto exit;
    }

    // manage case where the user is disabled or locked
    if((*user_password_expected == PASSWORD_LOCKED_STR[0]) || (*user_password_expected == PASSWORD_DISABLE_STR[0])) {
        SAH_TRACEZ_WARNING(ME, "Warning user %s is locked or disabled", input_username);
        amxc_var_add_key(cstring_t, args, STATUS_KEY, CREDENTIAL_STATUS(password_incorrect));
        ret_status = amxd_status_ok;
        goto exit;
    }

    //Case user with empty password
    if(STRING_EMPTY(user_password_expected)) {
        // given password is also empty
        if(STRING_EMPTY(input_password)) {
            amxc_var_add_key(cstring_t, args, STATUS_KEY, CREDENTIAL_STATUS(credentials_good));
        } else {
            amxc_var_add_key(cstring_t, args, STATUS_KEY, CREDENTIAL_STATUS(password_incorrect));
        }
        goto exit;
    }

    //Manage password comparison
    password_formatted = compute_password(is_hashed, input_password, user_password_expected, args);
    when_null(password_formatted, exit);

    if(strcmp(password_formatted, user_password_expected) != 0) {
        SAH_TRACEZ_ERROR(ME, "Password incorrect.");
        amxc_var_add_key(cstring_t, args, STATUS_KEY, CREDENTIAL_STATUS(password_incorrect));
        goto exit;
    }

    amxc_var_add_key(cstring_t, args, STATUS_KEY, CREDENTIAL_STATUS(credentials_good));
exit:
    SAH_TRACEZ_OUT(ME);
    return ret_status;
}

amxd_status_t _CreateNewUser(UNUSED amxd_object_t* object, UNUSED amxd_function_t* func, amxc_var_t* args, UNUSED amxc_var_t* ret) {
    amxd_status_t ret_status = amxd_status_unknown_error;
    amxd_object_t* users = NULL;
    amxd_trans_t trans;
    amxc_string_t alias;
    const cstring_t input_password = GET_CHAR(args, CHECK_CREDENTIALS_PASSWORD);
    const cstring_t input_username = GET_CHAR(args, CHECK_CREDENTIALS_USERNAME);
    bool is_hashed = GET_BOOL(args, CHECK_CREDENTIALS_IS_HASHED);

    amxd_trans_init(&trans);
    amxc_string_init(&alias, 0);
    if(!validate_str_username(input_username)) {
        SAH_TRACEZ_ERROR(ME, "Failed to read the username input.");
        amxc_var_add_key(cstring_t, args, STATUS_KEY, CREDENTIAL_STATUS(error_invalid_input));
        goto exit;
    }

    if(!validate_str_password(input_password, is_hashed)) {
        SAH_TRACEZ_ERROR(ME, "Failed to read the password input.");
        amxc_var_add_key(cstring_t, args, STATUS_KEY, CREDENTIAL_STATUS(error_invalid_input));
        goto exit;
    }
    users = amxd_dm_findf(usermanagement_get_dm(), "Users.User.");
    if(users == NULL) {
        SAH_TRACEZ_ERROR(ME, "Users object is NULL.");
        amxc_var_add_key(cstring_t, args, STATUS_KEY, CREDENTIAL_STATUS(error_other));
        goto exit;
    }
    amxc_string_setf(&alias, "%s-user", input_username);
    amxd_trans_select_object(&trans, users);
    amxd_trans_add_inst(&trans, 0, amxc_string_get(&alias, 0));
    amxd_trans_set_bool(&trans, "Enable", true);
    amxd_trans_set_cstring_t(&trans, DM_USERNAME, input_username);

    if(is_hashed) {
        amxd_trans_set_cstring_t(&trans, get_hashed_password_path(), input_password);
    } else {
        amxd_trans_set_cstring_t(&trans, DM_PASSWD_NAME, input_password);
    }

    amxd_trans_set_cstring_t(&trans, "RoleParticipation", "Users.Role.guest-role."); // webui-role as placeholder
    amxd_trans_set_bool(&trans, "StaticUser", false);
    amxd_trans_set_cstring_t(&trans, "GroupParticipation", DM_USER_GROUP_PARTICIPATION_DEFAULT);
    amxd_trans_set_cstring_t(&trans, "Shell", DM_USER_SUPPORTED_SHELL_DEFAULT);
    ret_status = amxd_trans_apply(&trans, usermanagement_get_dm());
    SAH_TRACEZ_NOTICE(ME, "CreateNewUser: ret status: %d", ret_status);
    ret_status = (ret_status == amxd_status_ok) ? amxd_status_ok : amxd_status_unknown_error; // hide error rv for security purpose
    amxc_var_add_key(int32_t, args, STATUS_KEY, ret_status);
    ret_status = 0;
exit:
    amxd_trans_clean(&trans);
    amxc_string_clean(&alias);
    SAH_TRACEZ_OUT(ME);
    return ret_status;
}
