/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef __COMMON_TEST_DATA_H__
#define __COMMON_TEST_DATA_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "sysconf.h"

#define PASSWD_FILENAME "/etc/passwd"
#define SHADOW_FILENAME "/etc/shadow"
#define GROUP_FILENAME "/etc/group"

#define PASSWD_BACKUP_FILENAME "/etc/passwd.testbackup.kasgfab"
#define SHADOW_BACKUP_FILENAME "/etc/shadow.testbackup.aewtrhj"
#define GROUP_BACKUP_FILENAME "/etc/group.testbackup.bsifvbg"

#define BUFFER_SIZE 512
#define SECONDS_PER_DAY (60 * 60 * 24)

#define TESTGROUP_ONE_GID 555
#define TESTGROUP_ONE_NAME "testgroup_one"
#define TESTGROUP_ONE_GRP_ENTRY "testgroup_one:x:555:"

#define TESTGROUP_TWO_GID 666
#define TESTGROUP_TWO_NAME "testgroup_two"
#define TESTGROUP_TWO_GRP_ENTRY "testgroup_two:x:666:"

UNUSED static gid_t testuser_one_gid_list[1] = {
    TESTGROUP_ONE_GID
};

UNUSED static gid_t testuser_two_gid_list[2] = {
    TESTGROUP_ONE_GID,
    TESTGROUP_TWO_GID
};

#define TESTUSER_ONE_ID 999
#define TESTUSER_ONE_NAME "testuser_one"
#define TESTUSER_ONE_PASSWORD "password"
#define TESTUSER_ONE_HASHED_PASSWORD "$6$ab$WfYjcVtm04.lEYV07CdYGA5G9xet7/eU/m3ApNyi7sD.pE7qFDG1ek7dRQpI2KCf9ESl1WoIH04x.DMDvmIed1"
#define TESTUSER_ONE_PRIM_GID TESTGROUP_ONE_GID
#define TESTUSER_ONE_GROUPS_SIZE 1
#define TESTUSER_ONE_GROUPS_PTR (&testuser_one_gid_list[0])
#define TESTUSER_ONE_HOMEDIR "/"
#define TESTUSER_ONE_SHELL "/bin/bash"
#define TESTUSER_ONE_PASSWD_ENTRY "testuser_one:x:999:555:testuser_one:/:/bin/bash"

#define TESTUSER_TWO_ID 888
#define TESTUSER_TWO_NAME "testuser_two"
#define TESTUSER_TWO_PASSWORD "qwerty"
#define TESTUSER_TWO_HASHED_PASSWORD "$6$ab$TT/fUGaPFjQWSXe2Dyelvm899lgsAsiUMjAenZbF0gbXMikktHkPKd1.YluavuJbOcLQuGN9i1oggyKdePE3o/"
#define TESTUSER_TWO_PRIM_GID TESTGROUP_TWO_GID
#define TESTUSER_TWO_GROUPS_SIZE 2
#define TESTUSER_TWO_GROUPS_PTR (&testuser_two_gid_list[0])
#define TESTUSER_TWO_HOMEDIR "/"
#define TESTUSER_TWO_SHELL "/bin/true"
#define TESTUSER_TWO_PASSWD_ENTRY "testuser_two:x:888:666:testuser_two:/:/bin/true"

#define TESTUSER_THREE_ID 333
#define TESTUSER_THREE_NAME "testuser_three"
#define TESTUSER_THREE_PASSWORD "qwerty"
#define TESTUSER_THREE_HASHED_PASSWORD "$6$ab$TT/fUGaPFjQWSXe2Dyelvm899lgsAsiUMjAenZbF0gbXMikktHkPKd1.YluavuJbOcLQuGN9i1oggyKdePE3o/"


#define TEST_NEW_PASSWORD "new_password"
#define TEST_NEW_HASHED_PASSWORD "$6$ab$4HoE3jt9i1tTjc3U/tcgSbhuFD7Rs6yoiGJ1j7OTSJcJqnwSyqc1xR9ULqLGPkmIlThMolzqbqO42CvujV4sd/"

groupdata_t* get_testgroup_one();
groupdata_t* get_testgroup_two();

userdata_t* get_testuser_one();
userdata_t* get_testuser_two();

#ifdef __cplusplus
}
#endif

#endif // __COMMON_TEST_DATA_H__