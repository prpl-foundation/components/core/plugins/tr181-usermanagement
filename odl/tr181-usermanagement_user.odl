%define {
    select Users {
        /**
         * This object contains parameters relating to the user characteristics.
         *
         * At most one enabled entry in this table can exist with a given value for Username,
         * or with a given value for UserID.
         *
         * @version 1.0
         */
        %persistent object User[] {
            counted with UserNumberOfEntries;
            on action add-inst call add_instance "UserID";
            on action del-inst call delete_instance_if_false "StaticUser";
            on action validate call check_home_directory_permission;

            /**
             * A non-volatile unique key used to reference this instance.
             * Alias provides a mechanism for a Controller to label this instance
             * for future reference.
             *
             * The following mandatory constraints MUST be reinforced:
             *  - The value MUST NOT be empty.
             *  - The value MUST start with a letter.
             *  - If the value is not assigned by the Controller at creation time,
             *    the agent MUST assign a value with a "cpe-" prefix.
             *
             * The value MUST NOT change once it's been assigned.
             *
             * @version 1.0
             */
            %unique %key %persistent string Alias {
                on action validate call check_maximum_length 64;
                on action validate call matches_regexp "^[[:alpha:]]";
            }

            /**
             * Enables/disables this user object instance.
             *
             * If the User being configured is currently accessing the device
             * then a disable MUST apply to the next user session and the current
             * user session MUST NOT be abruptly terminated.
             *
             * @version 1.0
             */
            %persistent bool Enable = false {
                userflags %usersetting;
            }

            /*
             * Unique Identifier of the user. Depending on the implementation
             * this can be the unique identifier assigned by the underlying system.
             *
             * @version 1.0
             */
            %persistent uint32 UserID {
                userflags %usersetting;
            }

            /**
             * Name of the current user. MUST NOT be an empty string for an enabled entry.
             *
             * @version 1.0
             */
            %unique %persistent string Username {
                on action validate call check_maximum_length 64;
                on action validate call check_uniq "Username";
                userflags %usersetting;
            }

            /**
             * The user's clear password.
             *
             * When read, this parameter returns an empty string, regardless of
             * the actual value.
             * This field is exclusive with the HashedPassword.
             *
             * @version 1.0
             */
            %persistent string Password = "" {
                default "";
                on action validate call check_maximum_length 64;
                on action read call hide_value;
            }

            /**
             * The user's hashed password.
             *
             * When read, this parameter returns an empty string, regardless of
             * the actual value.
             * This field is exclusive with the password.
             *
             * @version 1.0
             */
            %persistent string '${prefix_}HashedPassword' = "" {
                on action read call hide_value;
                userflags %usersetting;
            }

            /*
             * Comma-separated list (maximum list length 1024) of strings.
             * Each list item MUST be the Path Name of a row in the Group table.
             * The Groups of which this User is a member.
             *
             * @version 1.0
             */
            %persistent csv_string GroupParticipation {
                on action validate call check_maximum_length 1024;
                on action validate call csv_values_matches_regexp "^(Device\.)?(Users\.Group\..+)$";
                on action validate call check_is_object_ref { "'Device.Users.'" = "Users." };
                on action write call set_object_ref;
                on action destroy call destroy_object_ref;
                userflags %usersetting;
            }

            /*
             * Comma-separated list (maximum list length 1024) of strings.
             * Each list item MUST be the Path Name of a row in the Role table.
             * The Roles which are assigned to this user.
             *
             * @version 1.0
             */
            %persistent csv_string RoleParticipation {
                on action validate call check_maximum_length 1024;
                on action validate call csv_values_matches_regexp "^(Device\.)?(Users\.Role\..+)$";
                on action validate call check_is_object_ref { "'Device.Users.'" = "Users." };
                on action write call set_object_ref;
                on action destroy call destroy_object_ref;
                userflags %usersetting;
            }

            /*
             * A static user is a fixed user that is always available in the system.
             * When set to true, the user cannot be removed.
             *
             * @version 1.0
             */
            %persistent bool StaticUser = false {
                userflags %usersetting;
            }

            /**
             * String describing the default language for the local configuration
             * interface, specified according to [RFC3066].
             *
             * If an empty string, UserInterface.CurrentLanguage is used.
             *
             * @version 1.0
             */
            %persistent string Language {
                on action validate call check_maximum_length 16;
                /*
                 * Language format specified by [RFC3066], or the empty string
                 */
                on action validate call
                    matches_regexp "(^$)|(^[[:alpha:]]{1,8}(-[[:alnum:]]{1,8})*$)";
                userflags %usersetting;
            }

            /*
             * The value MUST be the Path Name of a row in the SupportedShell table.
             * An empty string means Shell access disabled.
             *
             * @version 1.0
             */
            %persistent string Shell {
                on action validate call matches_regexp "^(Device\.)?(Users.SupportedShell\..+)$";
                on action validate call check_is_object_ref;
                on action write call set_object_ref;
                on action destroy call destroy_object_ref;
                userflags %usersetting;
            }

            /*
             * A string representing the home directory of the user.
             * The value MUST be a valid directory.
             *
             * @version 1.0
             *
             */
            %persistent string '${prefix_}HomeDirectory' = "/var" {
                on action validate call check_maximum_length 64;
                on action validate call matches_regexp "^(\/[[:alnum:]_.\-]*)*$";
                userflags %usersetting;
            }

            /**
            * Internal parameter used by the plugin itself to identify if an update has been initiated internally.
            * @version 1.0
            */
            %private bool IsInternalUpdateTrigger = false;
        }
    }
}

%populate {

    on event "dm:instance-added" of "Users.User." call sync_user_to_system;

    on event "dm:object-changed" call edit_user_name_on_system
        filter 'path matches "Users\.User\.[0-9]+\.$" &&
                contains("parameters.Username")';
    on event "dm:object-changed" call edit_user_id_on_system
        filter 'path matches "Users\.User\.[0-9]+\.$" &&
                contains("parameters.UserID")';
    on event "dm:object-changed" call edit_user_password_on_system
        filter 'path matches "Users\.User\.[0-9]+\.$" &&
                (contains("parameters.Password") ||
                contains("parameters.${prefix_}HashedPassword"))';
    on event "dm:object-changed" call edit_user_role_participation_on_system
        filter 'path matches "Users\.User\.[0-9]+\.$" &&
                contains("parameters.RoleParticipation")';
    on event "dm:object-changed" call edit_user_group_participation_on_system
        filter 'path matches "Users\.User\.[0-9]+\.$" &&
                contains("parameters.GroupParticipation")';
    on event "dm:object-changed" call edit_user_home_directory_on_system
        filter 'path matches "Users\.User\.[0-9]+\.$" &&
                contains("parameters.${prefix_}HomeDirectory")';
    on event "dm:object-changed" call edit_user_shell_on_system
        filter 'path matches "Users\.User\.[0-9]+\.$" &&
                contains("parameters.Shell")';
    on event "dm:object-changed" call edit_user_enable
        filter 'path matches "Users\.User\.[0-9]+\.$" &&
                contains("parameters.Enable")';
    on event "dm:object-changed" call edit_user_fallback
        filter 'path matches "Users\.User\.[0-9]+\.$"';

    on event "dm:instance-removed" of "Users.User." call del_user_from_system;
}
