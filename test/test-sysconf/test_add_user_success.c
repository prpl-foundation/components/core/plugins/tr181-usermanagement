/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "test_sysconf.h"

void test_add_user_passwd(UNUSED void** state) {
    filebuffer_t orig_passwd_file;
    filebuffer_t new_passwd_file;

    filebuffer_init_filename(&orig_passwd_file, PASSWD_FILENAME);

    assert_int_equal(sysconf_add_group(get_testgroup_one()), 0);
    assert_int_equal(sysconf_add_user(get_testuser_one()), 0);

    filebuffer_init_filename(&new_passwd_file, PASSWD_FILENAME);

    char* new_data = filebuffer_get_str_copy(&new_passwd_file, orig_passwd_file.m_size, 0);
    const char* expected = TESTUSER_ONE_PASSWD_ENTRY;

    assert_int_equal(strncmp(new_data, expected, strlen(expected)), 0);
    assert_int_equal(memcmp(orig_passwd_file.mp_buffer, new_passwd_file.mp_buffer, orig_passwd_file.m_size), 0);

    free(new_data);
    filebuffer_clean(&orig_passwd_file);
    filebuffer_clean(&new_passwd_file);
}

void test_add_user_shadow(UNUSED void** state) {
    filebuffer_t orig_shadow_file;
    filebuffer_t new_shadow_file;

    filebuffer_init_filename(&orig_shadow_file, SHADOW_FILENAME);

    assert_int_equal(sysconf_add_group(get_testgroup_one()), 0);
    assert_int_equal(sysconf_add_user(get_testuser_one()), 0);

    filebuffer_init_filename(&new_shadow_file, SHADOW_FILENAME);

    char* new_data = filebuffer_get_str_copy(&new_shadow_file, orig_shadow_file.m_size, 0);
    const char* expected_prefix = TESTUSER_ONE_NAME ":";

    char* expected_postfix = calloc(BUFFER_SIZE, sizeof(char));
    sprintf(expected_postfix, ":%ld:0:99999:7:::", time(NULL) / SECONDS_PER_DAY);

    amxc_string_t last_line_str;
    amxc_llist_t last_line_splitted;
    amxc_string_init(&last_line_str, strlen(new_data));
    amxc_llist_init(&last_line_splitted);
    amxc_string_set(&last_line_str, new_data);
    amxc_string_split_to_llist(&last_line_str, &last_line_splitted, ':');

    const amxc_string_t* passwd_hash_str = amxc_string_get_from_llist(&last_line_splitted, 1);
    const char* passwd_hash = amxc_string_get(passwd_hash_str, 0);

    int passwd_hash_start = strlen(expected_prefix);
    int passwd_hash_len = amxc_string_text_length(passwd_hash_str);

    char option[HASH_ALGORITHM_LENGTH + SALT_LENGTH];
    amxc_llist_t passwd_entry_splitted;
    amxc_llist_init(&passwd_entry_splitted);
    amxc_string_split_to_llist(passwd_hash_str, &passwd_entry_splitted, '$');
    const amxc_string_t* algo_str = amxc_string_get_from_llist(&passwd_entry_splitted, 1);
    const char* algo = amxc_string_get(algo_str, 0);
    const amxc_string_t* salt_str = amxc_string_get_from_llist(&passwd_entry_splitted, 2);
    const char* salt = amxc_string_get(salt_str, 0);
    snprintf(option, HASH_ALGORITHM_LENGTH + SALT_LENGTH, "$%s$%s", algo, salt);

    assert_int_equal(strncmp(new_data, expected_prefix, strlen(expected_prefix)), 0);
    assert_int_equal(strcmp(crypt(TESTUSER_ONE_PASSWORD, option), passwd_hash), 0);
    assert_int_equal(strncmp(new_data + passwd_hash_start + passwd_hash_len, expected_postfix, strlen(expected_postfix)), 0);

    assert_int_equal(memcmp(orig_shadow_file.mp_buffer, new_shadow_file.mp_buffer, orig_shadow_file.m_size), 0);

    free(new_data);
    free(expected_postfix);
    amxc_string_clean(&last_line_str);
    amxc_llist_clean(&last_line_splitted, amxc_string_list_it_free);
    amxc_llist_clean(&passwd_entry_splitted, amxc_string_list_it_free);
    filebuffer_clean(&orig_shadow_file);
    filebuffer_clean(&new_shadow_file);
}

void test_add_user_disabled_shadow(UNUSED void** state) {
    filebuffer_t orig_shadow_file;
    filebuffer_t new_shadow_file;

    filebuffer_init_filename(&orig_shadow_file, SHADOW_FILENAME);

    userdata_t testuser = *get_testuser_one();
    testuser.enable = false;

    assert_int_equal(sysconf_add_group(get_testgroup_one()), 0);
    assert_int_equal(sysconf_add_user(&testuser), 0);

    filebuffer_init_filename(&new_shadow_file, SHADOW_FILENAME);

    char* new_data = filebuffer_get_str_copy(&new_shadow_file, orig_shadow_file.m_size, 0);

    char* expected_postfix = calloc(BUFFER_SIZE, sizeof(char));
    sprintf(expected_postfix, TESTUSER_ONE_NAME ":!" TESTUSER_ONE_HASHED_PASSWORD ":%ld:0:99999:7:::\n", time(NULL) / SECONDS_PER_DAY);

    assert_int_equal(strncmp(new_data, expected_postfix, strlen(expected_postfix)), 0);

    assert_int_equal(memcmp(orig_shadow_file.mp_buffer, new_shadow_file.mp_buffer, orig_shadow_file.m_size), 0);

    free(new_data);
    free(expected_postfix);
    filebuffer_clean(&orig_shadow_file);
    filebuffer_clean(&new_shadow_file);
}

void test_add_user_group(UNUSED void** state) {
    filebuffer_t orig_group_file;
    filebuffer_t new_group_file;

    assert_int_equal(sysconf_add_group(get_testgroup_one()), 0);
    filebuffer_init_filename(&orig_group_file, GROUP_FILENAME);

    assert_int_equal(sysconf_add_user(get_testuser_one()), 0);
    filebuffer_init_filename(&new_group_file, GROUP_FILENAME);

    char* new_data = filebuffer_get_str_copy(&new_group_file, orig_group_file.m_size - 1, 0);
    const char* expected = TESTUSER_ONE_NAME;

    assert_int_equal(strncmp(new_data, expected, strlen(expected)), 0);
    assert_int_equal(memcmp(orig_group_file.mp_buffer, new_group_file.mp_buffer, orig_group_file.m_size - 1), 0);

    free(new_data);
    filebuffer_clean(&orig_group_file);
    filebuffer_clean(&new_group_file);
}

void test_add_user_invalid_passwd_shadow(UNUSED void** state) {
    filebuffer_t orig_shadow_file;
    filebuffer_t new_shadow_file;

    filebuffer_init_filename(&orig_shadow_file, SHADOW_FILENAME);

    userdata_t testuser = *get_testuser_one();
    testuser.hashed_password = PASSWORD_LOCKED_STR;

    assert_int_equal(sysconf_add_group(get_testgroup_one()), 0);
    assert_int_equal(sysconf_add_user(&testuser), 0);

    filebuffer_init_filename(&new_shadow_file, SHADOW_FILENAME);

    char* new_data = filebuffer_get_str_copy(&new_shadow_file, orig_shadow_file.m_size, 0);

    char* expected_postfix = calloc(BUFFER_SIZE, sizeof(char));
    sprintf(expected_postfix, TESTUSER_ONE_NAME ":*:%ld:0:99999:7:::\n", time(NULL) / SECONDS_PER_DAY);

    assert_int_equal(strncmp(new_data, expected_postfix, strlen(expected_postfix)), 0);

    assert_int_equal(memcmp(orig_shadow_file.mp_buffer, new_shadow_file.mp_buffer, orig_shadow_file.m_size), 0);

    free(new_data);
    free(expected_postfix);
    filebuffer_clean(&orig_shadow_file);
    filebuffer_clean(&new_shadow_file);
}

void test_add_user_disabled_invalid_passwd_shadow(UNUSED void** state) {
    filebuffer_t orig_shadow_file;
    filebuffer_t new_shadow_file;

    filebuffer_init_filename(&orig_shadow_file, SHADOW_FILENAME);

    userdata_t testuser = *get_testuser_one();
    testuser.hashed_password = PASSWORD_LOCKED_STR;
    testuser.enable = false;

    assert_int_equal(sysconf_add_group(get_testgroup_one()), 0);
    assert_int_equal(sysconf_add_user(&testuser), 0);

    filebuffer_init_filename(&new_shadow_file, SHADOW_FILENAME);

    char* new_data = filebuffer_get_str_copy(&new_shadow_file, orig_shadow_file.m_size, 0);

    char* expected_postfix = calloc(BUFFER_SIZE, sizeof(char));
    sprintf(expected_postfix, TESTUSER_ONE_NAME ":!*:%ld:0:99999:7:::\n", time(NULL) / SECONDS_PER_DAY);

    assert_int_equal(strncmp(new_data, expected_postfix, strlen(expected_postfix)), 0);

    assert_int_equal(memcmp(orig_shadow_file.mp_buffer, new_shadow_file.mp_buffer, orig_shadow_file.m_size), 0);

    free(new_data);
    free(expected_postfix);
    filebuffer_clean(&orig_shadow_file);
    filebuffer_clean(&new_shadow_file);
}


void test_add_user_same_id_and_name(UNUSED void** state) {

    // this use case appears when sysconf is out of sync with data model

    userdata_t same_user;
    snapshot_t snapshot;

    assert_int_equal(sysconf_add_group(get_testgroup_one()), 0);
    assert_int_equal(sysconf_add_group(get_testgroup_two()), 0);
    assert_int_equal(sysconf_add_user(get_testuser_one()), 0);
    assert_int_equal(sysconf_add_user(get_testuser_two()), 0);

    snapshot_init(&snapshot);

    same_user = *get_testuser_one();
    same_user.user_id = TESTUSER_TWO_ID;

    assert_int_equal(sysconf_add_user(&same_user), 0);

    assert_true(snapshot_state_changed(&snapshot));
    snapshot_clean(&snapshot);
}

void test_add_user_same_id(UNUSED void** state) {

    userdata_t user_with_same_id;
    snapshot_t snapshot;

    assert_int_equal(sysconf_add_group(get_testgroup_one()), 0);
    assert_int_equal(sysconf_add_group(get_testgroup_two()), 0);
    assert_int_equal(sysconf_add_user(get_testuser_one()), 0);

    snapshot_init(&snapshot);

    user_with_same_id = *get_testuser_two();
    user_with_same_id.user_id = TESTUSER_ONE_ID;

    assert_int_equal(sysconf_add_user(&user_with_same_id), 0);

    assert_true(snapshot_state_changed(&snapshot));
    snapshot_clean(&snapshot);
}