/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "test_datamodel.h"
#include "test_environment.h"
#include "dm_usermanagement.h"

#include <amxd/amxd_transaction.h>

void test_check_update_id(UNUSED void** state) {
    groupdata_t group_data;
    group_dm_object_t group_dm_object;
    amxd_object_t* p_group_dm_obj = NULL;

    group_data.group_id = 1112;
    group_data.groupname = "third-group";

    group_dm_object.m_alias = "thirdgroup";
    group_dm_object.m_index = 66;
    group_dm_object.m_group_id = group_data.group_id;
    group_dm_object.m_group_name = group_data.groupname;
    group_dm_object.m_enable = true;
    group_dm_object.m_static_group = true;
    group_dm_object.m_role_participation = "";

    add_group_to_dm(get_test_datamodel(), &group_dm_object, &p_group_dm_obj);

    amxc_var_t str_var;
    amxc_var_init(&str_var);
    amxc_var_set_type(&str_var, AMXC_VAR_ID_CSTRING);
    amxc_var_set(cstring_t, &str_var, "GroupID");

    amxd_dm_t* p_dm = get_test_datamodel();
    amxd_object_t* p_root = amxd_dm_get_root(p_dm);
    amxd_object_t* p_group_obj = amxd_object_findf(p_root, "Users.Group.");

    // test same id
    group_dm_object.m_alias = "fourthgroup";
    group_dm_object.m_group_name = "fourthgroup";
    group_dm_object.m_index = 67;

    amxc_var_t* p_event_variant = convert_group_to_amxc_var(&group_dm_object, true);

    assert_int_equal(_add_instance(p_group_obj,
                                   NULL,
                                   action_object_add_inst,
                                   p_event_variant,
                                   p_event_variant,
                                   &str_var),
                     amxd_status_duplicate);

    amxc_var_delete(&p_event_variant);

    // test same index
    group_data.group_id = 1112;
    group_data.groupname = "third-group";

    group_dm_object.m_alias = "thirdgroup";
    group_dm_object.m_index = 66;
    group_dm_object.m_group_id = group_data.group_id;
    group_dm_object.m_group_name = group_data.groupname;

    p_event_variant = convert_group_to_amxc_var(&group_dm_object, true);

    assert_int_equal(_add_instance(p_group_obj,
                                   NULL,
                                   action_object_add_inst,
                                   p_event_variant,
                                   p_event_variant,
                                   &str_var),
                     amxd_status_ok);

    amxc_var_delete(&p_event_variant);

    //test no id
    group_data.groupname = "fourth-group";

    group_dm_object.m_alias = "fourthgroup";
    group_dm_object.m_index = 68;
    group_dm_object.m_group_name = group_data.groupname;

    p_event_variant = convert_group_to_amxc_var(&group_dm_object, false);

    assert_int_equal(_add_instance(p_group_obj,
                                   NULL,
                                   action_object_add_inst,
                                   p_event_variant,
                                   p_event_variant,
                                   &str_var),
                     amxd_status_ok);

    amxd_object_delete(&p_group_dm_obj);
    amxc_var_delete(&p_event_variant);
    amxc_var_clean(&str_var);
}