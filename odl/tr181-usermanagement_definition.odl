%define {

    /*
     * An object that contains the User, Group, Role and SupportedShell tables.
     *
     * @version 1.0
     */
    %persistent object Users {
        /**
        * [ASYNC] Checks whether the input Username and Password are valid for allowing access to the user interface on the device. If not valid, then an indication of why they are not valid is output.
        * @param Username The user name for logging in to the user interface on the device.
        * @param Password The password for logging in to the user interface on the device.
        * @param Hashed: Optional, boolean that indicate if password given is hashed or not (default is false).
        *
        * @note For parameters, we deliberately avoid using "%mandatory" and "%strict" so that we can consolidate all errors through the unified Status output variant.
        *
        * @version V1.0
        */
        %async void CheckCredentialsDiagnostics(%in string Username, %in string Password, %in bool IsHashed = false, %out string Status);


        /**
        * Create a User instance on the device.
        * @param Username The user name.
        * @param Password The password clear or hashed.
        * @param Hashed: Optional, boolean that indicate if password given is hashed or not (default is false).
        *
        * @version V1.0
        */
        %protected void CreateNewUser(%in string Username, %in string Password, %in bool IsHashed = false, %out string Status);
    }
}

include "tr181-usermanagement_supportedshell.odl";
include "tr181-usermanagement_role.odl";
include "tr181-usermanagement_group.odl";
include "tr181-usermanagement_user.odl";
