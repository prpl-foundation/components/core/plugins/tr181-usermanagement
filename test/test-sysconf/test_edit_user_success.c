/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "test_sysconf.h"

static void setup_single_user_test_env() {
    assert_int_equal(sysconf_add_group(get_testgroup_one()), 0);
    assert_int_equal(sysconf_add_user(get_testuser_one()), 0);
}

void test_change_user_shell(UNUSED void** state) {
    snapshot_t orig_snapshot;
    snapshot_t new_snapshot;
    userdata_t edit_data;

    setup_single_user_test_env();

    snapshot_init(&orig_snapshot);

    edit_data.user_id = TESTUSER_ONE_ID;
    edit_data.username = TESTUSER_ONE_NAME;
    edit_data.previous_username = NULL,
    edit_data.hashed_password = TESTUSER_ONE_HASHED_PASSWORD;
    edit_data.primary_group_id = 0;
    edit_data.all_group_ids.size = 0;
    edit_data.all_group_ids.list = NULL;
    edit_data.home_dir = NULL;
    edit_data.shell_path = "/bin/false";
    edit_data.role_paths = NULL;
    edit_data.enable = true;

    assert_int_equal(sysconf_edit_user(&edit_data), 0);

    snapshot_init(&new_snapshot);

    assert_true(filebuffer_equals(&orig_snapshot.m_shadow_snapshot, &new_snapshot.m_shadow_snapshot));
    assert_true(filebuffer_equals(&orig_snapshot.m_group_snapshot, &new_snapshot.m_group_snapshot));

    const char* expected_last_line = TESTUSER_ONE_NAME ":x:999:555:"TESTUSER_ONE_NAME ":"TESTUSER_ONE_HOMEDIR ":/bin/false\n";
    int start_last_line = new_snapshot.m_passwd_snapshot.m_size - strlen(expected_last_line);
    char* actual_last_line = filebuffer_get_str_copy(&new_snapshot.m_passwd_snapshot, start_last_line, 0);

    assert_string_equal(actual_last_line, expected_last_line);
    assert_int_equal(memcmp(orig_snapshot.m_passwd_snapshot.mp_buffer,
                            new_snapshot.m_passwd_snapshot.mp_buffer,
                            new_snapshot.m_passwd_snapshot.m_size - strlen(expected_last_line)), 0);

    free(actual_last_line);
    snapshot_clean(&orig_snapshot);
    snapshot_clean(&new_snapshot);
}

void test_change_user_homedir(UNUSED void** state) {
    snapshot_t orig_snapshot;
    snapshot_t new_snapshot;
    userdata_t edit_data;

    setup_single_user_test_env();

    snapshot_init(&orig_snapshot);

    edit_data.user_id = TESTUSER_ONE_ID;
    edit_data.username = TESTUSER_ONE_NAME;
    edit_data.previous_username = NULL,
    edit_data.hashed_password = TESTUSER_ONE_HASHED_PASSWORD;
    edit_data.primary_group_id = 0;
    edit_data.all_group_ids.size = 0;
    edit_data.all_group_ids.list = NULL;
    edit_data.home_dir = "/root";
    edit_data.shell_path = NULL;
    edit_data.role_paths = NULL;
    edit_data.enable = true;

    assert_int_equal(sysconf_edit_user(&edit_data), 0);

    snapshot_init(&new_snapshot);

    assert_true(filebuffer_equals(&orig_snapshot.m_shadow_snapshot, &new_snapshot.m_shadow_snapshot));
    assert_true(filebuffer_equals(&orig_snapshot.m_group_snapshot, &new_snapshot.m_group_snapshot));

    const char* expected_last_line = TESTUSER_ONE_NAME ":x:999:555:"TESTUSER_ONE_NAME ":/root:"TESTUSER_ONE_SHELL "\n";
    int start_last_line = new_snapshot.m_passwd_snapshot.m_size - strlen(expected_last_line);
    char* actual_last_line = filebuffer_get_str_copy(&new_snapshot.m_passwd_snapshot, start_last_line, 0);

    assert_string_equal(actual_last_line, expected_last_line);
    assert_int_equal(memcmp(orig_snapshot.m_passwd_snapshot.mp_buffer,
                            new_snapshot.m_passwd_snapshot.mp_buffer,
                            new_snapshot.m_passwd_snapshot.m_size - strlen(expected_last_line)), 0);

    free(actual_last_line);
    snapshot_clean(&orig_snapshot);
    snapshot_clean(&new_snapshot);
}

void test_change_user_groups(UNUSED void** state) {
    snapshot_t orig_snapshot;
    snapshot_t new_snapshot;
    userdata_t edit_data;

    setup_single_user_test_env();
    assert_int_equal(sysconf_add_group(get_testgroup_two()), 0);

    snapshot_init(&orig_snapshot);

    gid_t new_group_id = TESTGROUP_TWO_GID;
    edit_data.user_id = TESTUSER_ONE_ID;
    edit_data.username = TESTUSER_ONE_NAME;
    edit_data.previous_username = NULL,
    edit_data.hashed_password = TESTUSER_ONE_HASHED_PASSWORD;
    edit_data.primary_group_id = new_group_id;
    edit_data.all_group_ids.size = 1;
    edit_data.all_group_ids.list = &new_group_id;
    edit_data.home_dir = NULL;
    edit_data.shell_path = NULL;
    edit_data.role_paths = NULL;
    edit_data.enable = true;

    assert_int_equal(sysconf_edit_user(&edit_data), 0);

    snapshot_init(&new_snapshot);

    assert_true(filebuffer_equals(&orig_snapshot.m_shadow_snapshot, &new_snapshot.m_shadow_snapshot));

    const char* expected_group_postfix = TESTGROUP_ONE_GRP_ENTRY "\n"TESTGROUP_TWO_GRP_ENTRY TESTUSER_ONE_NAME "\n";
    int start_group_postfix = new_snapshot.m_group_snapshot.m_size - strlen(expected_group_postfix);
    char* actual_group_postfix = filebuffer_get_str_copy(&new_snapshot.m_group_snapshot, start_group_postfix, 0);

    const char* expected_passwd_postfix = TESTUSER_ONE_NAME ":x:999:666:"TESTUSER_ONE_NAME ":"TESTUSER_ONE_HOMEDIR ":/bin/bash\n";
    int start_passwd_postfix = new_snapshot.m_passwd_snapshot.m_size - strlen(expected_passwd_postfix);
    char* actual_passwd_postfix = filebuffer_get_str_copy(&new_snapshot.m_passwd_snapshot, start_passwd_postfix, 0);

    assert_string_equal(actual_group_postfix, expected_group_postfix);
    assert_string_equal(actual_passwd_postfix, expected_passwd_postfix);
    assert_int_equal(memcmp(orig_snapshot.m_group_snapshot.mp_buffer,
                            new_snapshot.m_group_snapshot.mp_buffer,
                            new_snapshot.m_group_snapshot.m_size - strlen(expected_group_postfix)), 0);
    assert_int_equal(memcmp(orig_snapshot.m_passwd_snapshot.mp_buffer,
                            new_snapshot.m_passwd_snapshot.mp_buffer,
                            new_snapshot.m_passwd_snapshot.m_size - strlen(expected_passwd_postfix)), 0);

    free(actual_group_postfix);
    free(actual_passwd_postfix);
    snapshot_clean(&orig_snapshot);
    snapshot_clean(&new_snapshot);
}

void test_change_user_password(UNUSED void** state) {
    snapshot_t orig_snapshot;
    snapshot_t new_snapshot;
    userdata_t edit_data;

    setup_single_user_test_env();

    snapshot_init(&orig_snapshot);

    edit_data.user_id = TESTUSER_ONE_ID;
    edit_data.username = TESTUSER_ONE_NAME;
    edit_data.previous_username = NULL,
    edit_data.password = TEST_NEW_PASSWORD;
    edit_data.hashed_password = TEST_NEW_HASHED_PASSWORD;
    edit_data.primary_group_id = 0;
    edit_data.all_group_ids.size = 0;
    edit_data.all_group_ids.list = NULL;
    edit_data.home_dir = NULL;
    edit_data.shell_path = NULL;
    edit_data.role_paths = NULL;
    edit_data.enable = true;

    assert_int_equal(sysconf_edit_user(&edit_data), 0);

    snapshot_init(&new_snapshot);

    assert_true(filebuffer_equals(&orig_snapshot.m_passwd_snapshot, &new_snapshot.m_passwd_snapshot));
    assert_true(filebuffer_equals(&orig_snapshot.m_group_snapshot, &new_snapshot.m_group_snapshot));

    const char* expected_shadow_prefix = TESTUSER_ONE_NAME ":";
    char* expected_shadow_postfix = calloc(BUFFER_SIZE, sizeof(char));
    sprintf(expected_shadow_postfix, ":%ld:0:99999:7:::\n", time(NULL) / SECONDS_PER_DAY);

    int last_shadow_line_start = new_snapshot.m_shadow_snapshot.m_size - 2; // skip the final endline
    for(; last_shadow_line_start >= 0; last_shadow_line_start--) {
        if(new_snapshot.m_shadow_snapshot.mp_buffer[last_shadow_line_start] == '\n') {
            break;
        }
    }
    last_shadow_line_start++;
    char* last_line = filebuffer_get_str_copy(&new_snapshot.m_shadow_snapshot, last_shadow_line_start, 0);
    amxc_string_t last_line_str;
    amxc_llist_t last_line_splitted;
    amxc_string_init(&last_line_str, strlen(last_line));
    amxc_llist_init(&last_line_splitted);
    amxc_string_set(&last_line_str, last_line);
    amxc_string_split_to_llist(&last_line_str, &last_line_splitted, ':');

    const amxc_string_t* passwd_hash_str = amxc_string_get_from_llist(&last_line_splitted, 1);
    const char* passwd_hash = amxc_string_get(passwd_hash_str, 0);

    char option[HASH_ALGORITHM_LENGTH + SALT_LENGTH];
    amxc_llist_t passwd_entry_splitted;
    amxc_llist_init(&passwd_entry_splitted);
    amxc_string_split_to_llist(passwd_hash_str, &passwd_entry_splitted, '$');
    const amxc_string_t* algo_str = amxc_string_get_from_llist(&passwd_entry_splitted, 1);
    const char* algo = amxc_string_get(algo_str, 0);
    const amxc_string_t* salt_str = amxc_string_get_from_llist(&passwd_entry_splitted, 2);
    const char* salt = amxc_string_get(salt_str, 0);
    snprintf(option, HASH_ALGORITHM_LENGTH + SALT_LENGTH, "$%s$%s", algo, salt);


    int passwd_hash_start = strlen(expected_shadow_prefix);
    int passwd_hash_len = amxc_string_text_length(passwd_hash_str);
    assert_int_equal(strncmp(last_line, expected_shadow_prefix, strlen(expected_shadow_prefix)), 0);
    assert_string_equal(crypt(TEST_NEW_PASSWORD, option), passwd_hash);
    assert_string_equal(last_line + passwd_hash_start + passwd_hash_len, expected_shadow_postfix);

    assert_int_equal(memcmp(orig_snapshot.m_shadow_snapshot.mp_buffer,
                            new_snapshot.m_shadow_snapshot.mp_buffer,
                            new_snapshot.m_shadow_snapshot.m_size - strlen(last_line)), 0);

    free(expected_shadow_postfix);
    free(last_line);

    amxc_string_clean(&last_line_str);
    amxc_llist_clean(&last_line_splitted, &amxc_string_list_it_free);
    amxc_llist_clean(&passwd_entry_splitted, amxc_string_list_it_free);
    snapshot_clean(&orig_snapshot);
    snapshot_clean(&new_snapshot);
}

void test_change_user_empty_password(UNUSED void** state) {
    snapshot_t orig_snapshot;
    snapshot_t new_snapshot;
    userdata_t edit_data;

    setup_single_user_test_env();

    snapshot_init(&orig_snapshot);

    edit_data.user_id = TESTUSER_ONE_ID;
    edit_data.username = TESTUSER_ONE_NAME;
    edit_data.password = NULL;
    edit_data.previous_username = NULL,
    edit_data.hashed_password = PASSWORD_EMPTY_STR;
    edit_data.primary_group_id = 0;
    edit_data.all_group_ids.size = 0;
    edit_data.all_group_ids.list = NULL;
    edit_data.home_dir = NULL;
    edit_data.shell_path = NULL;
    edit_data.role_paths = NULL;
    edit_data.enable = true;

    assert_int_equal(sysconf_edit_user(&edit_data), 0);

    snapshot_init(&new_snapshot);

    assert_true(filebuffer_equals(&orig_snapshot.m_passwd_snapshot, &new_snapshot.m_passwd_snapshot));
    assert_true(filebuffer_equals(&orig_snapshot.m_group_snapshot, &new_snapshot.m_group_snapshot));

    const char* expected_shadow_prefix = TESTUSER_ONE_NAME ":";
    char* expected_shadow_postfix = calloc(BUFFER_SIZE, sizeof(char));
    sprintf(expected_shadow_postfix, ":%ld:0:99999:7:::\n", time(NULL) / SECONDS_PER_DAY);

    int last_shadow_line_start = new_snapshot.m_shadow_snapshot.m_size - 2; // skip the final endline
    for(; last_shadow_line_start >= 0; last_shadow_line_start--) {
        if(new_snapshot.m_shadow_snapshot.mp_buffer[last_shadow_line_start] == '\n') {
            break;
        }
    }
    last_shadow_line_start++;
    char* last_line = filebuffer_get_str_copy(&new_snapshot.m_shadow_snapshot, last_shadow_line_start, 0);
    amxc_string_t last_line_str;
    amxc_llist_t last_line_splitted;
    amxc_string_init(&last_line_str, strlen(last_line));
    amxc_llist_init(&last_line_splitted);
    amxc_string_set(&last_line_str, last_line);
    amxc_string_split_to_llist(&last_line_str, &last_line_splitted, ':');

    const amxc_string_t* passwd_hash_str = amxc_string_get_from_llist(&last_line_splitted, 1);
    const char* passwd_hash = amxc_string_get(passwd_hash_str, 0);

    int passwd_hash_start = strlen(expected_shadow_prefix);
    int passwd_hash_len = amxc_string_text_length(passwd_hash_str);
    assert_int_equal(strncmp(last_line, expected_shadow_prefix, strlen(expected_shadow_prefix)), 0);
    assert_string_equal(passwd_hash, PASSWORD_EMPTY_STR);
    assert_string_equal(last_line + passwd_hash_start + passwd_hash_len, expected_shadow_postfix);

    assert_int_equal(memcmp(orig_snapshot.m_shadow_snapshot.mp_buffer,
                            new_snapshot.m_shadow_snapshot.mp_buffer,
                            new_snapshot.m_shadow_snapshot.m_size - strlen(last_line)), 0);

    free(expected_shadow_postfix);
    free(last_line);

    amxc_string_clean(&last_line_str);
    amxc_llist_clean(&last_line_splitted, &amxc_string_list_it_free);
    snapshot_clean(&orig_snapshot);
    snapshot_clean(&new_snapshot);
}

void test_change_username(UNUSED void** state) {
    snapshot_t orig_snapshot;
    snapshot_t new_snapshot;
    userdata_t edit_data;

    setup_single_user_test_env();

    snapshot_init(&orig_snapshot);

    edit_data.user_id = TESTUSER_ONE_ID;
    edit_data.username = TESTUSER_TWO_NAME;
    edit_data.previous_username = TESTUSER_ONE_NAME,
    edit_data.hashed_password = TESTUSER_ONE_HASHED_PASSWORD;
    edit_data.primary_group_id = 0;
    edit_data.all_group_ids.size = 0;
    edit_data.all_group_ids.list = NULL;
    edit_data.home_dir = NULL;
    edit_data.shell_path = "/bin/false";
    edit_data.role_paths = NULL;
    edit_data.enable = true;

    assert_int_equal(sysconf_edit_user(&edit_data), 0);

    snapshot_init(&new_snapshot);

    const char* expected_last_line = TESTUSER_TWO_NAME ":x:999:555:"TESTUSER_TWO_NAME ":"TESTUSER_ONE_HOMEDIR ":/bin/false\n";
    int start_last_line = new_snapshot.m_passwd_snapshot.m_size - strlen(expected_last_line);
    char* actual_last_line = filebuffer_get_str_copy(&new_snapshot.m_passwd_snapshot, start_last_line, 0);

    assert_string_equal(actual_last_line, expected_last_line);
    assert_int_equal(memcmp(orig_snapshot.m_passwd_snapshot.mp_buffer,
                            new_snapshot.m_passwd_snapshot.mp_buffer,
                            new_snapshot.m_passwd_snapshot.m_size - strlen(expected_last_line)), 0);

    free(actual_last_line);
    snapshot_clean(&orig_snapshot);
    snapshot_clean(&new_snapshot);
}

void test_change_userid(UNUSED void** state) {
    snapshot_t orig_snapshot;
    snapshot_t new_snapshot;
    userdata_t edit_data;

    setup_single_user_test_env();

    snapshot_init(&orig_snapshot);

    edit_data.user_id = 1234;
    edit_data.username = TESTUSER_ONE_NAME;
    edit_data.previous_username = NULL,
    edit_data.hashed_password = TESTUSER_ONE_HASHED_PASSWORD;
    edit_data.primary_group_id = 0;
    edit_data.all_group_ids.size = 0;
    edit_data.all_group_ids.list = NULL;
    edit_data.home_dir = NULL;
    edit_data.shell_path = "/bin/false";
    edit_data.role_paths = NULL;
    edit_data.enable = true;

    assert_int_equal(sysconf_edit_user(&edit_data), 0);

    snapshot_init(&new_snapshot);

    assert_true(filebuffer_equals(&orig_snapshot.m_shadow_snapshot, &new_snapshot.m_shadow_snapshot));
    assert_true(filebuffer_equals(&orig_snapshot.m_group_snapshot, &new_snapshot.m_group_snapshot));

    const char* expected_last_line = TESTUSER_ONE_NAME ":x:1234:555:"TESTUSER_ONE_NAME ":"TESTUSER_ONE_HOMEDIR ":/bin/false\n";
    int start_last_line = new_snapshot.m_passwd_snapshot.m_size - strlen(expected_last_line);
    char* actual_last_line = filebuffer_get_str_copy(&new_snapshot.m_passwd_snapshot, start_last_line, 0);

    assert_string_equal(actual_last_line, expected_last_line);
    assert_int_equal(memcmp(orig_snapshot.m_passwd_snapshot.mp_buffer,
                            new_snapshot.m_passwd_snapshot.mp_buffer,
                            new_snapshot.m_passwd_snapshot.m_size - strlen(expected_last_line)), 0);

    free(actual_last_line);
    snapshot_clean(&orig_snapshot);
    snapshot_clean(&new_snapshot);
}
