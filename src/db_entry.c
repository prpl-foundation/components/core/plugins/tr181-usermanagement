/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>

#include "db_entry.h"

#define ME "db"

db_entry_t* db_entry_new(const char* const p_filename) {
    SAH_TRACEZ_IN(ME);
    db_entry_t* new_db_entry = (db_entry_t*) calloc(1, sizeof(db_entry_t));
    int db_init_success = -1;

    when_null_oom(new_db_entry, exit, "new_db_entry");

    db_init_success = db_entry_init(new_db_entry, p_filename);

    if(db_init_success != 0) {
        free(new_db_entry);
        new_db_entry = NULL;
        SAH_TRACEZ_ERROR(ME, "Failed to initialize new db_entry");
        goto exit;
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return new_db_entry;
}

int32_t db_entry_init(db_entry_t* const p_db_entry, const char* const p_filename) {
    SAH_TRACEZ_IN(ME);
    int32_t retval = -1;

    when_null_trace(p_db_entry, exit, ERROR, "No db entry provided");

    if(p_filename != NULL) {
        p_db_entry->mp_output_file = fopen(p_filename, "w");
    } else {
        p_db_entry->mp_output_file = fopen("/dev/null", "a");
    }

    when_null_trace(p_db_entry->mp_output_file, exit, ERROR, "Failed to open provided file path");
    if(p_filename != NULL) {
        flockfile(p_db_entry->mp_output_file);
    }

    retval = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return retval;
}

void db_entry_clean(db_entry_t* p_this) {
    SAH_TRACEZ_IN(ME);
    when_null_trace(p_this, exit, ERROR, "No db entry provided");
    when_null_trace(p_this->mp_output_file, exit, ERROR, "No output file found");
    funlockfile(p_this->mp_output_file);
    fclose(p_this->mp_output_file);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void db_entry_delete(db_entry_t* p_to_delete) {
    SAH_TRACEZ_IN(ME);

    when_null_trace(p_to_delete, exit, ERROR, "No entry to delete provided");
    when_null_trace(p_to_delete->mp_vtable, exit, ERROR, "No vtable provided");
    when_null_trace(p_to_delete->mp_vtable->clean, exit, ERROR, "No clean function provided");

    p_to_delete->mp_vtable->clean(p_to_delete);
    free(p_to_delete);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

int32_t db_entry_entry_get_next(db_entry_t* p_this, bool* p_entry_found) {
    SAH_TRACEZ_IN(ME);
    int32_t rv = -1;

    when_null_trace(p_this, exit, ERROR, "No db entry provided");
    when_null_trace(p_this->mp_vtable, exit, ERROR, "No vtable provided");
    when_null_trace(p_this->mp_vtable->entry_get_next, exit, ERROR, "No next function provided");
    rv = p_this->mp_vtable->entry_get_next(p_this, p_entry_found);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int32_t db_entry_entry_put(db_entry_t* p_this) {
    SAH_TRACEZ_IN(ME);
    int32_t rv = -1;

    when_null_trace(p_this, exit, ERROR, "No db entry provided");
    when_null_trace(p_this->mp_vtable, exit, ERROR, "No vtable provided");
    when_null_trace(p_this->mp_vtable->entry_put, exit, ERROR, "No put function provided");
    rv = p_this->mp_vtable->entry_put(p_this);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

bool db_entry_entry_matches(const db_entry_t* p_this, const amxc_var_t* p_data) {
    SAH_TRACEZ_IN(ME);
    bool rv = false;

    when_null_trace(p_this, exit, ERROR, "No db entry provided");
    when_null_trace(p_this->mp_vtable, exit, ERROR, "No vtable provided");
    when_null_trace(p_this->mp_vtable->entry_matches, exit, ERROR, "No match function provided");
    rv = p_this->mp_vtable->entry_matches(p_this, p_data);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

bool db_entry_entry_matches_any(const db_entry_t* p_this,
                                const amxc_llist_t* p_data_list) {
    SAH_TRACEZ_IN(ME);
    bool rv = false;

    amxc_llist_for_each(it, p_data_list) {
        amxc_var_t* p_data_var = amxc_var_from_llist_it(it);
        if(db_entry_entry_matches(p_this, p_data_var)) {
            rv = false;
            goto exit;
        }
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

bool db_entry_entry_matches_all(const db_entry_t* p_this,
                                const amxc_llist_t* p_data_list) {
    SAH_TRACEZ_IN(ME);
    bool rv = true;
    amxc_llist_for_each(it, p_data_list) {
        amxc_var_t* p_data_var = amxc_var_from_llist_it(it);
        if(!db_entry_entry_matches(p_this, p_data_var)) {
            rv = false;
            goto exit;
        }
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

uint32_t db_entry_entry_matches_count(const db_entry_t* p_this,
                                      const amxc_llist_t* p_data_list) {
    SAH_TRACEZ_IN(ME);
    uint32_t count = 0;
    amxc_llist_for_each(it, p_data_list) {
        amxc_var_t* p_data_var = amxc_var_from_llist_it(it);
        if(db_entry_entry_matches(p_this, p_data_var)) {
            count++;
        } else {
            count--;
        }
    }
    SAH_TRACEZ_OUT(ME);
    return count;
}

int32_t db_entry_matches_any_entry(db_entry_t* p_this,
                                   const amxc_var_t* p_data,
                                   bool* p_result) {
    SAH_TRACEZ_IN(ME);
    int32_t retval = -1;
    bool has_an_entry = false;
    when_null_trace(p_result, exit, ERROR, "No boolean variable provided");
    *p_result = false;
    db_entry_stream_reset(p_this);
    when_failed_trace(db_entry_entry_get_next(p_this, &has_an_entry),
                      exit,
                      ERROR,
                      "Failed to get next database entry");
    while(has_an_entry) {
        if(db_entry_entry_matches(p_this, p_data)) {
            *p_result = true;
            retval = 0;
            goto exit;
        }
        when_failed_trace(db_entry_entry_get_next(p_this, &has_an_entry),
                          exit,
                          ERROR,
                          "Failed to get next database entry");
    }
    retval = 0;
exit:
    db_entry_stream_reset(p_this);
    SAH_TRACEZ_OUT(ME);
    return retval;
}

int32_t db_entry_matches_all_entries(db_entry_t* p_this,
                                     const amxc_var_t* p_data,
                                     bool* p_result) {
    SAH_TRACEZ_IN(ME);
    int32_t retval = -1;
    bool has_an_entry = false;
    when_null_trace(p_result, exit, ERROR, "No boolean variable provided");
    *p_result = true;
    db_entry_stream_reset(p_this);
    when_failed_trace(db_entry_entry_get_next(p_this, &has_an_entry),
                      exit,
                      ERROR,
                      "Failed to get next database entry");
    while(has_an_entry) {
        if(!db_entry_entry_matches(p_this, p_data)) {
            *p_result = false;
            retval = 0;
            goto exit;
        }
        when_failed_trace(db_entry_entry_get_next(p_this, &has_an_entry),
                          exit,
                          ERROR,
                          "Failed to get next database entry");
    }
    retval = 0;
exit:
    db_entry_stream_reset(p_this);
    SAH_TRACEZ_OUT(ME);
    return retval;
}

int32_t db_entry_matching_entry_count(db_entry_t* p_this,
                                      const amxc_var_t* p_data,
                                      uint32_t* p_result) {
    SAH_TRACEZ_IN(ME);
    int32_t retval = -1;
    bool has_an_entry = false;
    *p_result = 0;
    db_entry_stream_reset(p_this);

    retval = db_entry_entry_get_next(p_this, &has_an_entry);
    when_failed_trace(retval, exit, ERROR, "Failed to get next database entry");

    while(has_an_entry) {
        if(db_entry_entry_matches(p_this, p_data)) {
            (*p_result)++;
        }
        retval = db_entry_entry_get_next(p_this, &has_an_entry);
        when_failed_trace(retval, exit, ERROR, "Failed to get next database entry");
    }
    retval = 0;

exit:
    db_entry_stream_reset(p_this);
    SAH_TRACEZ_OUT(ME);
    return retval;

}

int32_t db_entry_entry_update(db_entry_t* p_this, const amxc_var_t* p_data) {
    SAH_TRACEZ_IN(ME);
    int32_t rv = -1;

    when_null_trace(p_this, exit, ERROR, "No db entry provided");
    when_null_trace(p_this->mp_vtable, exit, ERROR, "No vtable provided");
    when_null_trace(p_this->mp_vtable->entry_update, exit, ERROR, "No update function provided");
    rv = p_this->mp_vtable->entry_update(p_this, p_data);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int32_t db_entry_entry_create_new(db_entry_t* p_this, const amxc_var_t* p_data) {
    SAH_TRACEZ_IN(ME);
    int32_t rv = -1;

    when_null_trace(p_this, exit, ERROR, "No db entry provided");
    when_null_trace(p_this->mp_vtable, exit, ERROR, "No vtable provided");
    when_null_trace(p_this->mp_vtable->entry_create, exit, ERROR, "No create function provided");
    rv = p_this->mp_vtable->entry_create(p_this, p_data);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int32_t db_entry_stream_open(db_entry_t* p_this) {
    SAH_TRACEZ_IN(ME);
    int32_t rv = -1;

    when_null_trace(p_this, exit, ERROR, "No db entry provided");
    when_null_trace(p_this->mp_vtable, exit, ERROR, "No vtable provided");
    when_null_trace(p_this->mp_vtable->stream_open, exit, ERROR, "No function to open a stream provided");
    rv = p_this->mp_vtable->stream_open(p_this);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int32_t db_entry_stream_close(db_entry_t* p_this) {
    SAH_TRACEZ_IN(ME);
    int32_t rv = -1;

    when_null_trace(p_this, exit, ERROR, "No db entry provided");
    when_null_trace(p_this->mp_vtable, exit, ERROR, "No vtable provided");
    when_null_trace(p_this->mp_vtable->stream_close, exit, ERROR, "No function to close a stream provided");
    rv = p_this->mp_vtable->stream_close(p_this);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int32_t db_entry_stream_reset(db_entry_t* p_this) {
    SAH_TRACEZ_IN(ME);
    int32_t rv = -1;

    when_null_trace(p_this, exit, ERROR, "No db entry provided");
    when_null_trace(p_this->mp_vtable, exit, ERROR, "No vtable provided");
    when_null_trace(p_this->mp_vtable->stream_reset, exit, ERROR, "No function to reset a stream provided");
    rv = p_this->mp_vtable->stream_reset(p_this);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}
