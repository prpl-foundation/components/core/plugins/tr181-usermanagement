/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>

#include <amxp/amxp_dir.h>

#include "usermanagement.h"
#include "file_system.h"

#define ME "fs"

bool fs_is_directory_exist(const char* const dir_path) {
    struct stat st;
    return (stat(dir_path, &st) == 0 && S_ISDIR(st.st_mode));
}

bool fs_check_directory_permissions(const char* const dir_path, const userdata_t* const userdata) {
    bool allowed = false;
    struct stat path_stat;

    errno = 0;
    if(stat(dir_path, &path_stat) != 0) {
        if(errno != ENOENT) {
            SAH_TRACEZ_ERROR(ME, "Error while stat directory: %s", strerror(errno));
            goto exit;
        } else {
            allowed = true;
            goto exit;
        }
    }

    when_false_trace(path_stat.st_uid == userdata->user_id, exit, WARNING,
                     "Warning User %s does not owned the directory: %s (%u/%u)", userdata->username, dir_path, path_stat.st_uid, userdata->user_id);
    when_false_trace((path_stat.st_mode & S_IWUSR), exit, WARNING,
                     "Warning User %s does not have the write permissions for the directory: %s",
                     userdata->username, dir_path);
    SAH_TRACEZ_INFO(ME, "User %s has the required permissions for the directory: %s", userdata->username, dir_path);
    allowed = true;

exit:
    return allowed;
}

static int32_t remove_directory(const char* path) {
    DIR* dir = NULL;
    struct dirent* entry = NULL;
    char full_path[PATH_MAX] = {0};
    int32_t retval = -1;

    when_null_trace((dir = opendir(path)), exit, ERROR, "Error while processing directories deletion %s", strerror(errno));

    while((entry = readdir(dir)) != NULL) {
        if((strcmp(entry->d_name, ".") == 0) || (strcmp(entry->d_name, "..") == 0)) {
            continue;
        }

        snprintf(full_path, sizeof(full_path), "%s/%s", path, entry->d_name);
        if(entry->d_type == DT_DIR) {
            remove_directory(full_path);
        } else {
            if(unlink(full_path) != 0) {
                SAH_TRACEZ_WARNING(ME, "Error while processing unlink path %s", strerror(errno));
            }
        }
    }

    closedir(dir);
    when_failed_trace(rmdir(path), exit, ERROR, "Error while deleting top directory %s", strerror(errno));
    retval = 0;
exit:
    return retval;
}

int32_t fs_remove_directory(const char* path, const userdata_t* const userdata) {
    int32_t retval = -1;
    struct stat path_stat;

    errno = 0;
    when_true_trace((stat(path, &path_stat) != 0) && (errno != ENOENT), exit, ERROR,
                    "Error while stat directory: %s", strerror(errno));
    when_false(fs_check_directory_permissions(path, userdata), exit);
    when_failed(remove_directory(path), exit);

    retval = 0;
exit:
    return retval;
}

int32_t fs_create_owned_directory(const char* const dir_path, const userdata_t* const userdata) {
    int32_t retval = -1;

    when_str_empty(dir_path, exit);
    when_failed_trace(amxp_dir_owned_make(dir_path, 0775, userdata->user_id, userdata->primary_group_id),
                      exit, ERROR, "Directory [%s] creation failed for %s (%u/%u)",
                      dir_path, userdata->username, userdata->user_id, userdata->primary_group_id);
    retval = 0;
exit:
    return retval;
}

int32_t fs_chown_directory(const char* const dir_path, const userdata_t* const userdata) {
    int32_t retval = -1;

    when_str_empty(dir_path, exit);

    errno = 0;
    when_failed_trace(chown(dir_path, userdata->user_id, userdata->primary_group_id) != 0, exit, ERROR,
                      "Change owner failed %s for %s (%s)", dir_path, userdata->username, strerror(errno));
    retval = 0;
exit:
    return retval;

}
