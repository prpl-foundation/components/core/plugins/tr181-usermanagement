/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "convert_to_amxc.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

void fill_group_parameters(amxc_var_t* const p_to_fill, const group_dm_object_t* p_group, bool enable_id) {
    amxc_var_add_key(cstring_t, p_to_fill, "Alias", p_group->m_alias);
    amxc_var_add_key(bool, p_to_fill, "Enable", p_group->m_enable);

    if(enable_id) {
        amxc_var_add_key(uint32_t, p_to_fill, "GroupID", p_group->m_group_id);
    }

    amxc_var_add_key(cstring_t, p_to_fill, "Groupname", p_group->m_group_name);
    amxc_var_add_key(csv_string_t, p_to_fill, "RoleParticipation", p_group->m_role_participation);
    amxc_var_add_key(bool, p_to_fill, "StaticGroup", p_group->m_static_group);
}

amxc_var_t* convert_group_to_amxc_var(const group_dm_object_t* p_group, bool enable_id) {
    amxc_var_t* p_variant;
    amxc_var_t* p_keys_variant;
    amxc_var_t* p_param_variant;

    amxc_var_new(&p_variant);
    amxc_var_set_type(p_variant, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(uint32_t, p_variant, "index", p_group->m_index);
    p_keys_variant = amxc_var_add_key(amxc_htable_t, p_variant, "keys", NULL);

    amxc_var_add_key(cstring_t, p_keys_variant, "Alias", p_group->m_alias);

    if(enable_id) {
        amxc_var_add_key(uint32_t, p_keys_variant, "GroupID", p_group->m_group_id);
    }

    amxc_var_add_key(cstring_t, p_keys_variant, "Groupname", p_group->m_group_name);

    amxc_var_add_key(cstring_t, p_variant, "name", p_group->m_alias);
    amxc_var_add_key(cstring_t, p_variant, "object", "Users.Group.");
    p_param_variant = amxc_var_add_key(amxc_htable_t, p_variant, "parameters", NULL);

    fill_group_parameters(p_param_variant, p_group, enable_id);

    amxc_var_add_key(cstring_t, p_variant, "path", "Users.Group.");

    return p_variant;
}

/**
 * @brief Produce the variant required to invoke the edition C dm:object-changed callback for groups.
 *
 * @param p_user dm group object
 * @param param_name parameter name on which dm:object-changed appears
 * @param before old param value
 * @param after new param value
 * @return variant that will be used to call C dm:object-changed callback
 */
amxc_var_t* convert_group_to_amxc_var_edit_string(const group_dm_object_t* p_group, const char* param_name, const char* before, const char* after) {
    amxc_var_t* p_variant;
    amxc_string_t path;
    amxc_string_t index_path;

    amxc_string_init(&path, 0);
    amxc_string_init(&index_path, 0);

    amxc_var_new(&p_variant);
    amxc_var_set_type(p_variant, AMXC_VAR_ID_HTABLE);

    amxc_string_setf(&path, "Users.Group.%s", p_group->m_alias);
    amxc_string_setf(&index_path, "Users.Group.%d", p_group->m_index);

    amxc_var_add_key(cstring_t, p_variant, "object", amxc_string_get(&path, 0));
    amxc_var_add_key(cstring_t, p_variant, "path", amxc_string_get(&index_path, 0));

    amxc_var_t* param_variant = amxc_var_add_key(amxc_htable_t, p_variant, "parameters", NULL);
    amxc_var_t* change_variant = amxc_var_add_key(amxc_htable_t, param_variant, param_name, NULL);
    amxc_var_add_key(cstring_t, change_variant, "from", before);
    amxc_var_add_key(cstring_t, change_variant, "to", after);

    amxc_string_clean(&path);
    amxc_string_clean(&index_path);

    return p_variant;
}

void fill_user_parameters(amxc_var_t* const p_to_fill, const user_dm_object_t* p_user) {
    amxc_var_add_key(cstring_t, p_to_fill, "Alias", p_user->m_alias);
    amxc_var_add_key(uint32_t, p_to_fill, "UserID", p_user->m_user_id);
    amxc_var_add_key(cstring_t, p_to_fill, "Username", p_user->m_username);
    amxc_var_add_key(bool, p_to_fill, "Enable", p_user->m_enable);
    amxc_var_add_key(cstring_t, p_to_fill, "GroupParticipation", p_user->m_group_participation);
    amxc_var_add_key(cstring_t, p_to_fill, "RoleParticipation", p_user->m_role_participation);
    amxc_var_add_key(cstring_t, p_to_fill, "Language", p_user->m_language);
    amxc_var_add_key(cstring_t, p_to_fill, "Password", p_user->m_password);
    amxc_var_add_key(cstring_t, p_to_fill, "X_PRPL-COM_HashedPassword", p_user->m_hashed_password);
    amxc_var_add_key(bool, p_to_fill, "StaticUser", p_user->m_static_user);
    amxc_var_add_key(cstring_t, p_to_fill, "X_PRPL-COM_HomeDirectory", p_user->m_homedir);
    amxc_var_add_key(cstring_t, p_to_fill, "Shell", p_user->m_shell);
}

amxc_var_t* convert_user_to_amxc_var(const user_dm_object_t* p_user) {
    amxc_var_t* p_variant;
    amxc_var_t* p_keys_variant;
    amxc_var_t* p_param_variant;

    amxc_var_new(&p_variant);
    amxc_var_set_type(p_variant, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(uint32_t, p_variant, "index", p_user->m_index);
    p_keys_variant = amxc_var_add_key(amxc_htable_t, p_variant, "keys", NULL);

    amxc_var_add_key(cstring_t, p_keys_variant, "Alias", p_user->m_alias);
    amxc_var_add_key(uint32_t, p_keys_variant, "UserID", p_user->m_user_id);
    amxc_var_add_key(cstring_t, p_keys_variant, "Username", p_user->m_username);

    amxc_var_add_key(cstring_t, p_variant, "name", p_user->m_alias);
    amxc_var_add_key(cstring_t, p_variant, "object", "Users.User.");
    p_param_variant = amxc_var_add_key(amxc_htable_t, p_variant, "parameters", NULL);

    fill_user_parameters(p_param_variant, p_user);

    amxc_var_add_key(cstring_t, p_variant, "path", "Users.User.");

    return p_variant;
}

/**
 * @brief Produce the variant required to invoke the edition C dm:object-changed callback for users.
 *
 * @param p_user dm user object
 * @param param_name parameter name on which dm:object-changed appears
 * @param before old param value
 * @param after new param value
 * @return variant that will be used to call C dm:object-changed callback
 */
amxc_var_t* convert_user_to_amxc_var_edit_string(const user_dm_object_t* p_user, const char* param_name, const char* before, const char* after) {
    amxc_var_t* p_variant;
    amxc_string_t path;
    amxc_string_t index_path;

    amxc_string_init(&path, 0);
    amxc_string_init(&index_path, 0);

    amxc_var_new(&p_variant);
    amxc_var_set_type(p_variant, AMXC_VAR_ID_HTABLE);

    amxc_string_setf(&path, "Users.User.%s", p_user->m_alias);
    amxc_string_setf(&index_path, "Users.User.%d", p_user->m_index);

    amxc_var_add_key(cstring_t, p_variant, "object", amxc_string_get(&path, 0));
    amxc_var_add_key(cstring_t, p_variant, "path", amxc_string_get(&index_path, 0));

    amxc_var_t* param_variant = amxc_var_add_key(amxc_htable_t, p_variant, "parameters", NULL);
    amxc_var_t* change_variant = amxc_var_add_key(amxc_htable_t, param_variant, param_name, NULL);
    amxc_var_add_key(cstring_t, change_variant, "from", before);
    amxc_var_add_key(cstring_t, change_variant, "to", after);

    amxc_string_clean(&path);
    amxc_string_clean(&index_path);

    return p_variant;
}

void fill_shell_parameters(amxc_var_t* const p_to_fill, const shell_dm_object_t* p_shell) {
    amxc_var_add_key(cstring_t, p_to_fill, "Alias", p_shell->m_alias);
    amxc_var_add_key(cstring_t, p_to_fill, "Name", p_shell->m_name);
    amxc_var_add_key(bool, p_to_fill, "Enable", p_shell->m_enable);
}
