/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "test_environment.h"

#include <amxd/amxd_object.h>

#include <stdarg.h>
#include <setjmp.h>
#include <stdlib.h>
#include <cmocka.h>

static const group_dm_object_t default_group = {
    .m_index = 0,
    .m_alias = DEFAULT_GROUP_ALIAS,
    .m_group_id = DEFAULT_GROUP_ID,
    .m_group_name = DEFAULT_GROUPNAME,
    .m_enable = true,
    .m_static_group = false,
    .m_role_participation = "",
};

static const group_dm_object_t no_group = {
    .m_index = 26,
    .m_alias = "nogroup-group",
    .m_group_id = 65534,
    .m_group_name = "nogroup",
    .m_enable = true,
    .m_static_group = false,
    .m_role_participation = "",
};

static const shell_dm_object_t default_shell = {
    .m_index = 0,
    .m_alias = DEFAULT_SHELL_ALIAS,
    .m_name = DEFAULT_SHELL_NAME,
    .m_enable = true,
};

void add_group_to_dm(amxd_dm_t* p_dm, const group_dm_object_t* p_group, amxd_object_t** pp_new_group) {
    amxd_object_t* p_root_obj = amxd_dm_get_root(p_dm);
    amxd_object_t* p_grp_template_obj = amxd_object_findf(p_root_obj, "Users.Group.");
    amxc_var_t* p_param_variant;

    amxc_var_new(&p_param_variant);
    amxc_var_set_type(p_param_variant, AMXC_VAR_ID_HTABLE);
    fill_group_parameters(p_param_variant, p_group, true);

    amxd_status_t status = amxd_object_add_instance(pp_new_group, p_grp_template_obj, p_group->m_alias, p_group->m_index, p_param_variant);
    assert_int_equal(status, amxd_status_ok);

    amxc_var_delete(&p_param_variant);
}

void add_user_to_dm(amxd_dm_t* p_dm, const user_dm_object_t* p_user, amxd_object_t** pp_user_obj) {
    amxd_object_t* p_root_obj = amxd_dm_get_root(p_dm);
    amxd_object_t* p_user_template_obj = amxd_object_findf(p_root_obj, "Users.User.");
    amxc_var_t* p_param_variant;

    amxc_var_new(&p_param_variant);
    amxc_var_set_type(p_param_variant, AMXC_VAR_ID_HTABLE);
    fill_user_parameters(p_param_variant, p_user);

    amxd_status_t status = amxd_object_add_instance(pp_user_obj, p_user_template_obj, p_user->m_alias, p_user->m_index, p_param_variant);
    assert_int_equal(status, amxd_status_ok);

    amxc_var_delete(&p_param_variant);
}

void add_shell_to_dm(amxd_dm_t* p_dm, const shell_dm_object_t* p_shell, amxd_object_t** pp_shell_object) {
    amxd_object_t* p_root_obj = amxd_dm_get_root(p_dm);
    amxd_object_t* p_shell_template_obj = amxd_object_findf(p_root_obj, "Users.SupportedShell.");
    amxc_var_t* p_param_variant;

    amxc_var_new(&p_param_variant);
    amxc_var_set_type(p_param_variant, AMXC_VAR_ID_HTABLE);
    fill_shell_parameters(p_param_variant, p_shell);

    amxd_status_t status = amxd_object_add_instance(pp_shell_object, p_shell_template_obj, p_shell->m_alias, p_shell->m_index, p_param_variant);
    assert_int_equal(status, amxd_status_ok);

    amxc_var_delete(&p_param_variant);
}

static amxd_object_t* p_default_group_obj = NULL;
static amxd_object_t* p_no_group_obj = NULL;
static amxd_object_t* p_default_shell_obj = NULL;

void setup_default_dm(amxd_dm_t* p_dm) {
    add_group_to_dm(p_dm, &default_group, &p_default_group_obj);
    add_group_to_dm(p_dm, &no_group, &p_no_group_obj);
    add_shell_to_dm(p_dm, &default_shell, &p_default_shell_obj);
}

void teardown_default_dm() {
    amxd_object_delete(&p_default_group_obj);
    amxd_object_delete(&p_no_group_obj);
    amxd_object_delete(&p_default_shell_obj);
}