/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxb/amxb.h>
#include <amxb/amxb_register.h>
#include <amxb/amxb_connect.h>
#include <amxo/amxo.h>
#include <amxo/amxo.h>

#include "test_rpcs.h"
#include "usermanagement.h"
#include "dm_usermanagement.h"
#include "rpc_check_credentials.h"
#include "usermanagement_rpc_calls.h"
#include "common_test_data.h"
#include "sdw_entry.h"
#include "dummy_backend.h"
#include "convert_to_amxc.h"
#include "passwd_hash.h"
#include "mock_passwd_hash.h"

#define GET_KEY(v, k) amxc_var_get_key(v, k, AMXC_VAR_FLAG_DEFAULT)
#define CHAR(v) amxc_var_constcast(cstring_t, v)
#define UINT32(v) amxc_var_constcast(uint32_t, v)

#define LONG_WORD "abcdefghijklmnopqrstuvabcdefghijklmnopqrstuv\
                   abcdefghijklmnopqrstuvabcdefghijklmnopqrstuv\
                   abcdefghijklmnopqrstuvabcdefghijklmnopqrstuv\
                   abcdefghijklmnopqrstuvabcdefghijklmnopqrstuv"

#define DEFAULT_GROUP_PATH "Users.Group.test-grp."
#define DEFAULT_SHELL_PATH "Users.SupportedShell.bash."

#define PASSWORD_HASHED "$algo$salt$hash"
#define WRONG_PASSWORD_HASHED "$algo$salt$wronghash"

static const char* odl_defs = "../../odl/tr181-usermanagement_definition.odl";
static const char* mock_odl_defs = "../mock_odl/mock.odl";

static user_dm_object_t test_user_1_dm_object = {
    .m_alias = NULL,
    .m_index = 0,
    .m_user_id = TESTUSER_ONE_ID,
    .m_username = TESTUSER_ONE_NAME,
    .m_password = NULL,
    .m_hashed_password = NULL,
    .m_group_participation = DEFAULT_GROUP_PATH,
    .m_role_participation = "Users.Role.webui-role.",
    .m_shell = DEFAULT_SHELL_PATH,
    .m_homedir = "/",
    .m_enable = true,
    .m_static_user = false,
    .m_language = "",
};

static user_dm_object_t test_user_2_dm_object = {
    .m_alias = NULL,
    .m_index = 0,
    .m_user_id = TESTUSER_TWO_ID,
    .m_username = TESTUSER_TWO_NAME,
    .m_password = NULL,
    .m_hashed_password = NULL,
    .m_group_participation = DEFAULT_GROUP_PATH,
    .m_role_participation = "",
    .m_shell = DEFAULT_SHELL_PATH,
    .m_homedir = "/",
    .m_enable = true,
    .m_static_user = false,
    .m_language = "",
};

static user_dm_object_t test_user_3_dm_object = {
    .m_alias = NULL,
    .m_index = 0,
    .m_user_id = TESTUSER_THREE_ID,
    .m_username = TESTUSER_THREE_NAME,
    .m_password = NULL,
    .m_hashed_password = NULL,
    .m_group_participation = DEFAULT_GROUP_PATH,
    .m_role_participation = "",
    .m_shell = DEFAULT_SHELL_PATH,
    .m_homedir = "/",
    .m_enable = false,
    .m_static_user = false,
    .m_language = "",
};

static amxd_dm_t dm;
static amxo_parser_t parser;
static amxc_var_t args;
static amxb_bus_ctx_t* bus_ctx = NULL;
static amxc_var_t ret;

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

static void init_variables() {
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_init(&args);
    amxc_var_init(&ret);
}

static void clean_variables() {
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

static void common_end_test() {
    handle_events();

    clean_variables();
}


amxd_status_t __wrap_amxd_trans_apply(amxd_trans_t* const trans, amxd_dm_t* const dm);

static void convert_user_object_to_var(amxc_var_t* const p_to_fill, const user_dm_object_t* p_user) {
    amxc_var_add_key(cstring_t, p_to_fill, "Alias", p_user->m_alias);
    amxc_var_add_key(uint32_t, p_to_fill, "UserID", p_user->m_user_id);
    amxc_var_add_key(cstring_t, p_to_fill, "Username", p_user->m_username);
    amxc_var_add_key(bool, p_to_fill, "Enable", p_user->m_enable);
    amxc_var_add_key(cstring_t, p_to_fill, "GroupParticipation", p_user->m_group_participation);
    amxc_var_add_key(cstring_t, p_to_fill, "RoleParticipation", p_user->m_role_participation);
    amxc_var_add_key(cstring_t, p_to_fill, "Language", p_user->m_language);
    amxc_var_add_key(cstring_t, p_to_fill, "Password", p_user->m_password);
    amxc_var_add_key(cstring_t, p_to_fill, "X_PRPL-COM_HashedPassword", p_user->m_hashed_password);
    amxc_var_add_key(bool, p_to_fill, "StaticUser", p_user->m_static_user);
    amxc_var_add_key(cstring_t, p_to_fill, "Shell", p_user->m_shell);
}

static amxd_status_t users_add_method(const user_dm_object_t* p_user) {
    int ret_status = amxd_status_ok;
    const char* object = "Users.User.";
    uint32_t index = 0;
    amxc_var_t values;

    amxc_var_init(&values);
    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    convert_user_object_to_var(&values, p_user);

    init_variables();

    ret_status = amxb_add(bus_ctx, object, index, NULL, &values, &ret, 5);
    if(ret_status != 0) {
        printf("Add User failed - retval = %d\n", ret_status);
        goto exit;
    }

exit:
    amxc_var_clean(&values);
    return ret_status;
}

static amxd_status_t users_check_credentials(const cstring_t username, const cstring_t password, const bool isHashed) {
    amxd_status_t ret_status = amxd_status_ok;

    amxd_object_t* users = amxd_dm_findf(&dm, "Users.");
    if(users == NULL) {
        ret_status = amxd_status_object_not_found;
        goto exit;
    }

    init_variables();

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    if(username != NULL) {
        amxc_var_add_key(cstring_t, &args, CHECK_CREDENTIALS_USERNAME, username);
    }
    if(password != NULL) {
        amxc_var_add_key(cstring_t, &args, CHECK_CREDENTIALS_PASSWORD, password);
    }

    amxc_var_add_key(bool, &args, CHECK_CREDENTIALS_IS_HASHED, isHashed);

    ret_status = amxd_object_invoke_function(users, "CheckCredentialsDiagnostics", &args, &ret);
    if(ret_status != amxd_status_ok) {
        goto exit;
    }

exit:
    return ret_status;
}

static amxd_status_t users_create_user(const cstring_t username, const cstring_t password, const bool isHashed) {
    amxd_status_t ret_status = amxd_status_ok;

    amxd_object_t* users = amxd_dm_findf(&dm, "Users.");
    if(users == NULL) {
        ret_status = amxd_status_object_not_found;
        goto exit;
    }

    init_variables();

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    if(username != NULL) {
        amxc_var_add_key(cstring_t, &args, CHECK_CREDENTIALS_USERNAME, username);
    }
    if(password != NULL) {
        amxc_var_add_key(cstring_t, &args, CHECK_CREDENTIALS_PASSWORD, password);
    }

    amxc_var_add_key(bool, &args, CHECK_CREDENTIALS_IS_HASHED, isHashed);

    ret_status = amxd_object_invoke_function(users, "CreateNewUser", &args, &ret);
    if(ret_status != amxd_status_ok) {
        goto exit;
    }

exit:
    return ret_status;
}

static void users_remove_all_method() {
    amxd_object_t* users = NULL;

    users = amxd_dm_findf(usermanagement_get_dm(), "Users.User.");
    amxd_object_for_each(instance, it, users) {
        amxd_object_t* user = amxc_container_of(it, amxd_object_t, it);
        amxd_object_delete(&user);
    }
}

int setup(UNUSED void** state) {

    test_user_1_dm_object.m_hashed_password = PASSWORD_HASHED;
    test_user_1_dm_object.m_password = NULL;
    test_user_2_dm_object.m_hashed_password = PASSWORD_HASHED;
    test_user_2_dm_object.m_password = NULL;
    test_user_3_dm_object.m_hashed_password = "!" PASSWORD_HASHED;
    test_user_3_dm_object.m_password = NULL;

    assert_int_equal(users_add_method(&test_user_1_dm_object), amxd_status_ok);
    assert_int_equal(users_add_method(&test_user_2_dm_object), amxd_status_ok);
    assert_int_equal(users_add_method(&test_user_3_dm_object), amxd_status_ok);
    return 0;
}

int teardown(UNUSED void** state) {
    users_remove_all_method();
    return 0;
}

int test_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);
    assert_int_equal(test_register_dummy_be(), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser,
                        amxb_get_fd(bus_ctx),
                        connection_read,
                        "dummy:/tmp/dummy.sock",
                        AMXO_BUS,
                        bus_ctx);
    amxb_register(bus_ctx, &dm);

    assert_int_equal(amxo_resolver_ftab_add(&parser,
                                            "Users.CheckCredentialsDiagnostics",
                                            AMXO_FUNC(_CheckCredentialsDiagnostics)
                                            ), 0);

    assert_int_equal(amxo_resolver_ftab_add(&parser,
                                            "Users.CreateNewUser",
                                            AMXO_FUNC(_CreateNewUser)
                                            ), 0);

    assert_int_equal(amxo_parser_parse_file(&parser, mock_odl_defs, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);

    assert_int_equal(_usermanagement_main(0, &dm, &parser), 0);

    handle_events();

    return 0;
}

int test_teardown(UNUSED void** state) {
    assert_int_equal(_usermanagement_main(1, &dm, &parser), 0);

    amxb_free(&bus_ctx);
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);
    clean_variables();

    test_unregister_dummy_be();
    return 0;
}

void test_check_credential_null_username(UNUSED void** state) {
    //Test when username is NULL
    assert_int_equal(users_check_credentials(NULL, TESTUSER_ONE_PASSWORD, false), amxd_status_ok);
    assert_string_equal(CHAR(GET_KEY(&args, STATUS_KEY)), CREDENTIAL_STATUS(error_invalid_input));

    common_end_test();
}

void test_check_credential_empty_username(UNUSED void** state) {
    //Test when username is empty
    assert_int_equal(users_check_credentials("", TESTUSER_ONE_PASSWORD, false), amxd_status_ok);
    assert_string_equal(CHAR(GET_KEY(&args, STATUS_KEY)), CREDENTIAL_STATUS(error_invalid_input));

    common_end_test();
}

void test_check_credential_long_username(UNUSED void** state) {
    //Test when username is longusers_create_user
    assert_int_equal(users_check_credentials(LONG_WORD, TESTUSER_ONE_PASSWORD, false), amxd_status_ok);
    assert_string_equal(CHAR(GET_KEY(&args, STATUS_KEY)), CREDENTIAL_STATUS(error_invalid_input));

    common_end_test();
}

void test_check_credential_null_password(UNUSED void** state) {
    //Test when password is NULL
    assert_int_equal(users_check_credentials(TESTUSER_ONE_NAME, NULL, false), amxd_status_ok);
    assert_string_equal(CHAR(GET_KEY(&args, STATUS_KEY)), CREDENTIAL_STATUS(error_invalid_input));

    common_end_test();
}

void test_check_credential_long_password(UNUSED void** state) {
    //Test when password is long
    assert_int_equal(users_check_credentials(TESTUSER_ONE_NAME, LONG_WORD, false), amxd_status_ok);
    assert_string_equal(CHAR(GET_KEY(&args, STATUS_KEY)), CREDENTIAL_STATUS(error_invalid_input));

    common_end_test();
}

void test_check_credential_nonexisting_user(UNUSED void** state) {
    //Test when username doesn't exist
    assert_int_equal(users_check_credentials("dont_exist", TESTUSER_ONE_PASSWORD, false), amxd_status_ok);
    assert_string_equal(CHAR(GET_KEY(&args, STATUS_KEY)), CREDENTIAL_STATUS(credentials_missing));

    common_end_test();
}

void test_check_credential_wrong_password(UNUSED void** state) {
    //Test when password is wrong
    will_return(__wrap_crypt, "wrong_password");
    will_return(__wrap_crypt, WRONG_PASSWORD_HASHED);

    assert_int_equal(users_check_credentials(TESTUSER_ONE_NAME, "wrong_password", false), amxd_status_ok);
    assert_string_equal(CHAR(GET_KEY(&args, STATUS_KEY)), CREDENTIAL_STATUS(password_incorrect));

    common_end_test();
}

void test_check_credential_wrong_hashed_password(UNUSED void** state) {
    //Test when password is wrong
    assert_int_equal(users_check_credentials(TESTUSER_ONE_NAME, "wrong_hashed_password", true), amxd_status_ok);
    assert_string_equal(CHAR(GET_KEY(&args, STATUS_KEY)), CREDENTIAL_STATUS(password_incorrect));

    common_end_test();
}

void test_check_credential_allowed_user(UNUSED void** state) {
    //Test when user is allowed to access the UI
    will_return(__wrap_crypt, TESTUSER_ONE_PASSWORD);
    will_return(__wrap_crypt, PASSWORD_HASHED);

    assert_int_equal(users_check_credentials(TESTUSER_ONE_NAME, TESTUSER_ONE_PASSWORD, false), amxd_status_ok);
    assert_string_equal(CHAR(GET_KEY(&args, STATUS_KEY)), CREDENTIAL_STATUS(credentials_good));

    common_end_test();
}

void test_check_credential_allowed_user_hashed_password(UNUSED void** state) {
    //Test when user is allowed to access the UI
    assert_int_equal(users_check_credentials(TESTUSER_ONE_NAME, PASSWORD_HASHED, true), amxd_status_ok);
    assert_string_equal(CHAR(GET_KEY(&args, STATUS_KEY)), CREDENTIAL_STATUS(credentials_good));

    common_end_test();
}

void test_check_credential_disabled_user(UNUSED void** state) {

    assert_int_equal(users_check_credentials(TESTUSER_THREE_NAME, TESTUSER_THREE_PASSWORD, false), amxd_status_ok);
    assert_string_equal(CHAR(GET_KEY(&args, STATUS_KEY)), CREDENTIAL_STATUS(error_other));

    common_end_test();
}

amxd_status_t __wrap_amxd_trans_apply(UNUSED amxd_trans_t* const trans,
                                      UNUSED amxd_dm_t* const dm) {
    amxd_status_t retval = (amxd_status_t) mock();
    return retval;
}

void test_create_new_user_with_clear_password(UNUSED void** state) {
    will_return(__wrap_amxd_trans_apply, amxd_status_ok);
    assert_int_equal(users_create_user(TESTUSER_THREE_NAME, TESTUSER_THREE_PASSWORD, false), amxd_status_ok);
    assert_int_equal(UINT32(GET_KEY(&args, STATUS_KEY)), 0);

    common_end_test();
}

void test_create_new_user_with_hashed_password(UNUSED void** state) {
    will_return(__wrap_amxd_trans_apply, amxd_status_ok);
    assert_int_equal(users_create_user(TESTUSER_THREE_NAME, TESTUSER_THREE_HASHED_PASSWORD, true), amxd_status_ok);
    assert_int_equal(UINT32(GET_KEY(&args, STATUS_KEY)), 0);

    common_end_test();
}
