/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "mock_sysconf.h"
#include <stdio.h>
#include <stdarg.h>
#include <setjmp.h>
#include <stdlib.h>
#include <cmocka.h>

static void assert_string_equal_null(const char* str1, const char* str2) {
    if((str1 == NULL) && (str2 == NULL)) {
        return;
    }
    if((str1 == NULL) || (str2 == NULL)) {
        fail();
        return;
    }
    assert_string_equal(str1, str2);
}

static void assert_userdata_equals(const userdata_t* p_expected, const userdata_t* p_actual) {
    if((p_expected == NULL) && (p_actual == NULL)) {
        return;
    }
    if((p_expected == NULL) || (p_actual == NULL)) {
        fail();
    }
    assert_string_equal_null(p_expected->username, p_actual->username);
    assert_int_equal(p_expected->user_id, p_actual->user_id);
    assert_string_equal_null(p_expected->password, p_actual->password);
    assert_string_equal_null(p_expected->hashed_password, p_actual->hashed_password);
    assert_string_equal_null(p_expected->home_dir, p_actual->home_dir);
    assert_string_equal_null(p_expected->shell_path, p_actual->shell_path);
    assert_int_equal(p_expected->primary_group_id, p_actual->primary_group_id);
    assert_int_equal(p_expected->all_group_ids.size, p_actual->all_group_ids.size);
    size_t size_of_gid_list = p_expected->all_group_ids.size;
    assert_memory_equal(p_expected->all_group_ids.list, p_actual->all_group_ids.list, size_of_gid_list * sizeof(gid_t));
}

static void assert_groupdata_equals(const groupdata_t* p_expected, const groupdata_t* p_actual) {
    if((p_expected == NULL) && (p_actual == NULL)) {
        return;
    }
    if((p_expected == NULL) || (p_actual == NULL)) {
        fail();
    }
    assert_int_equal(p_expected->group_id, p_actual->group_id);
    assert_string_equal_null(p_expected->groupname, p_actual->groupname);
    assert_string_equal_null(p_expected->role_paths, p_actual->role_paths);
}


bool __wrap_sysconf_check_userdata_new(const userdata_t* const user_to_check) {
    userdata_t* expected_userdata = (userdata_t*) mock();
    bool retval = (bool) mock();

    assert_userdata_equals(expected_userdata, user_to_check);

    return retval;
}

bool __wrap_sysconf_check_userdata_del(const userdata_t* const user_to_check) {
    userdata_t* expected_userdata = (userdata_t*) mock();
    bool retval = (bool) mock();

    assert_userdata_equals(expected_userdata, user_to_check);

    return retval;
}


bool __wrap_sysconf_check_groupdata_new(const groupdata_t* const group_to_check) {
    groupdata_t* expected_userdata = (groupdata_t*) mock();
    bool retval = (bool) mock();

    assert_groupdata_equals(expected_userdata, group_to_check);

    return retval;
}

bool __wrap_sysconf_check_groupdata_del(const groupdata_t* const group_to_check) {
    groupdata_t* expected_userdata = (groupdata_t*) mock();
    bool retval = (bool) mock();

    assert_groupdata_equals(expected_userdata, group_to_check);

    return retval;
}


int32_t __wrap_sysconf_add_user(const userdata_t* const user_to_add) {
    userdata_t* expected_userdata = (userdata_t*) mock();
    int32_t retval = (int32_t) mock();

    assert_userdata_equals(expected_userdata, user_to_add);

    return retval;
}

int32_t __wrap_sysconf_edit_user(const userdata_t* const user_to_edit) {
    userdata_t* expected_userdata = (userdata_t*) mock();
    int32_t retval = (int32_t) mock();

    assert_userdata_equals(expected_userdata, user_to_edit);

    return retval;
}

int32_t __wrap_sysconf_del_user(const userdata_t* const user_to_delete) {
    userdata_t* expected_userdata = (userdata_t*) mock();
    int32_t retval = (int32_t) mock();

    assert_userdata_equals(expected_userdata, user_to_delete);

    return retval;
}


int32_t __wrap_sysconf_add_group(const groupdata_t* const group_to_add) {
    groupdata_t* expected_userdata = (groupdata_t*) mock();
    int32_t retval = (int32_t) mock();

    assert_groupdata_equals(expected_userdata, group_to_add);

    return retval;
}

int32_t __wrap_sysconf_edit_group(const groupdata_t* const group_to_edit) {
    groupdata_t* expected_userdata = (groupdata_t*) mock();
    int32_t retval = (int32_t) mock();

    assert_groupdata_equals(expected_userdata, group_to_edit);

    return retval;
}

int32_t __wrap_sysconf_del_group(const groupdata_t* const group_to_delete) {
    groupdata_t* expected_userdata = (groupdata_t*) mock();
    int32_t retval = (int32_t) mock();

    assert_groupdata_equals(expected_userdata, group_to_delete);

    return retval;
}
