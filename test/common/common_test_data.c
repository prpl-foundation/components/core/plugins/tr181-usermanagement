/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "common_test_data.h"

static groupdata_t testgroup_one = {
    .group_id = TESTGROUP_ONE_GID,
    .groupname = TESTGROUP_ONE_NAME,
};

static groupdata_t testgroup_two = {
    .group_id = TESTGROUP_TWO_GID,
    .groupname = TESTGROUP_TWO_NAME
};

static userdata_t testuser_one = {
    .user_id = TESTUSER_ONE_ID,
    .username = TESTUSER_ONE_NAME,
    .previous_username = NULL,
    .password = TESTUSER_ONE_PASSWORD,
    .hashed_password = TESTUSER_ONE_HASHED_PASSWORD,
    .home_dir = TESTUSER_ONE_HOMEDIR,
    .shell_path = TESTUSER_ONE_SHELL,
    .primary_group_id = TESTUSER_ONE_PRIM_GID,
    .all_group_ids.size = 1,
    .all_group_ids.list = TESTUSER_ONE_GROUPS_PTR,
    .enable = true,
};

static userdata_t testuser_two = {
    .user_id = TESTUSER_TWO_ID,
    .username = TESTUSER_TWO_NAME,
    .previous_username = NULL,
    .password = TESTUSER_TWO_PASSWORD,
    .hashed_password = TESTUSER_TWO_HASHED_PASSWORD,
    .home_dir = TESTUSER_TWO_HOMEDIR,
    .shell_path = TESTUSER_TWO_SHELL,
    .primary_group_id = TESTUSER_TWO_PRIM_GID,
    .all_group_ids.size = TESTUSER_TWO_GROUPS_SIZE,
    .all_group_ids.list = TESTUSER_TWO_GROUPS_PTR,
    .enable = true,
};

groupdata_t* get_testgroup_one() {
    return &testgroup_one;
}

groupdata_t* get_testgroup_two() {
    return &testgroup_two;
}

userdata_t* get_testuser_one() {
    return &testuser_one;
}

userdata_t* get_testuser_two() {
    return &testuser_two;
}
