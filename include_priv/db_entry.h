/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef __DB_ENTRY_H__
#define __DB_ENTRY_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdio.h>

#include "usermanagement.h"

struct _vtable_t;

typedef struct _db_entry_t {
    const struct _vtable_t* mp_vtable;
    FILE* mp_output_file;
} db_entry_t;

typedef int32_t (* entry_get_next_fn_t)(db_entry_t*, bool*);
typedef int32_t (* entry_put_fn_t)(db_entry_t*);
typedef bool (* entry_matches_fn_t)(const db_entry_t*, const amxc_var_t*);
typedef int32_t (* entry_update_fn_t)(db_entry_t*, const amxc_var_t*);
typedef int32_t (* entry_create_new_fn_t)(db_entry_t*, const amxc_var_t*);
typedef int32_t (* stream_open_fn_t)(db_entry_t*);
typedef int32_t (* stream_close_fn_t)(db_entry_t*);
typedef int32_t (* stream_reset_fn_t)(db_entry_t*);
typedef void (* clean_fn_t)(db_entry_t*);

typedef struct _vtable_t {
    entry_get_next_fn_t entry_get_next;
    entry_put_fn_t entry_put;
    entry_matches_fn_t entry_matches;
    entry_update_fn_t entry_update;
    entry_create_new_fn_t entry_create;
    stream_open_fn_t stream_open;
    stream_close_fn_t stream_close;
    stream_reset_fn_t stream_reset;
    clean_fn_t clean;
} vtable_t;

db_entry_t* db_entry_new(const char* const p_filename);
int32_t db_entry_init(db_entry_t* const p_db_entry, const char* const p_filename);
void db_entry_clean(db_entry_t* p_this);
void db_entry_delete(db_entry_t* p_to_delete);
int32_t db_entry_entry_get_next(db_entry_t* p_this, bool* p_entry_found);
int32_t db_entry_entry_put(db_entry_t* p_this);
bool db_entry_entry_matches(const db_entry_t* p_this, const amxc_var_t* p_data);
bool db_entry_entry_matches_any(const db_entry_t* p_this, const amxc_llist_t* p_data_list);
bool db_entry_entry_matches_all(const db_entry_t* p_this, const amxc_llist_t* p_data_list);
uint32_t db_entry_entry_matches_count(const db_entry_t* p_this, const amxc_llist_t* p_data_list);
int32_t db_entry_matches_any_entry(db_entry_t* p_this, const amxc_var_t* p_data, bool* p_result);
int32_t db_entry_matches_all_entries(db_entry_t* p_this, const amxc_var_t* p_data, bool* p_result);
int32_t db_entry_matching_entry_count(db_entry_t* p_this, const amxc_var_t* p_data, uint32_t* p_result);
int32_t db_entry_entry_update(db_entry_t* p_this, const amxc_var_t* p_data);
int32_t db_entry_entry_create_new(db_entry_t* p_this, const amxc_var_t* p_data);
int32_t db_entry_stream_open(db_entry_t* p_this);
int32_t db_entry_stream_close(db_entry_t* p_this);
int32_t db_entry_stream_reset(db_entry_t* p_this);

#ifdef __cplusplus
}
#endif

#endif // __DB_ENTRY_H__