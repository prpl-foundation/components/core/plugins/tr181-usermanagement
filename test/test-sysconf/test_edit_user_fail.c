/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "test_sysconf.h"

static void setup_single_user_test_env() {
    assert_int_equal(sysconf_add_group(get_testgroup_one()), 0);
    assert_int_equal(sysconf_add_user(get_testuser_one()), 0);
}

void test_change_user_nonexistent_shell(UNUSED void** state) {
    snapshot_t orig_snapshot;
    userdata_t edit_data;

    setup_single_user_test_env();

    snapshot_init(&orig_snapshot);

    edit_data.user_id = TESTUSER_ONE_ID;
    edit_data.username = TESTUSER_ONE_NAME;
    edit_data.previous_username = NULL,
    edit_data.hashed_password = NULL;
    edit_data.primary_group_id = 0;
    edit_data.all_group_ids.size = 0;
    edit_data.all_group_ids.list = NULL;
    edit_data.home_dir = NULL;
    edit_data.shell_path = "/this/path/does/not/exist";
    edit_data.role_paths = NULL;
    edit_data.enable = true;

    assert_int_not_equal(sysconf_edit_user(&edit_data), 0);

    assert_false(snapshot_state_changed(&orig_snapshot));

    snapshot_clean(&orig_snapshot);
}

void test_change_user_nonexistent_groups(UNUSED void** state) {
    snapshot_t orig_snapshot;
    userdata_t edit_data;

    setup_single_user_test_env();

    snapshot_init(&orig_snapshot);

    gid_t new_group_id = TESTGROUP_TWO_GID;
    edit_data.user_id = TESTUSER_ONE_ID;
    edit_data.username = TESTUSER_ONE_NAME;
    edit_data.previous_username = NULL,
    edit_data.hashed_password = NULL;
    edit_data.primary_group_id = new_group_id;
    edit_data.all_group_ids.size = 1;
    edit_data.all_group_ids.list = &new_group_id;
    edit_data.home_dir = NULL;
    edit_data.shell_path = NULL;
    edit_data.role_paths = NULL;
    edit_data.enable = true;

    assert_int_not_equal(sysconf_edit_user(&edit_data), 0);

    assert_false(snapshot_state_changed(&orig_snapshot));

    snapshot_clean(&orig_snapshot);
}