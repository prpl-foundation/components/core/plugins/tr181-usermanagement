/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "groupdata.h"

#include "dm_usermanagement.h"
#include "usermanagement.h"
#include "usermanagement_util.h"

#define ME "dm"

void init_groupdata(groupdata_t* const to_init) {
    SAH_TRACEZ_IN(ME);
    when_null_trace(to_init, exit, ERROR, "No groupdata to initialize provided");

    to_init->group_id = 0;
    to_init->groupname = NULL;
    to_init->previous_groupname = NULL;
    to_init->role_paths = NULL;

exit:
    SAH_TRACEZ_OUT(ME);
}

void clean_groupdata(UNUSED groupdata_t* const to_clean) {
}

void fill_groupdata_from_dm(groupdata_t* groupdata, amxd_object_t* p_group_object) {
    SAH_TRACEZ_IN(ME);
    when_null(groupdata, exit);
    when_null(p_group_object, exit);

    groupdata->group_id = GET_UINT32(amxd_object_get_param_value(p_group_object, DM_GROUPID), NULL);
    groupdata->groupname = GET_CHAR(amxd_object_get_param_value(p_group_object, DM_GROUPAME), NULL);
    groupdata->role_paths = GET_CHAR(amxd_object_get_param_value(p_group_object, DM_ROLE_PARTICIPATION_NAME), NULL);
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

int32_t fill_groupdata(groupdata_t* const to_fill,
                       const amxc_var_t* const data) {
    SAH_TRACEZ_IN(ME);
    int32_t rv = -1;
    const amxc_var_t* const parameters = amxc_var_get_path(data,
                                                           "parameters",
                                                           AMXC_VAR_FLAG_DEFAULT);
    when_null_trace(to_fill, exit, ERROR, "No group data to fill provided");

    to_fill->groupname = GET_CHAR(parameters, "Groupname");
    to_fill->group_id = GET_UINT32(parameters, "GroupID");
    to_fill->role_paths = GET_CHAR(parameters, "RoleParticipation");
    rv = 0;
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int32_t fill_groupdata_role_paths(groupdata_t* to_fill, const amxc_var_t* const p_role_paths) {
    SAH_TRACEZ_IN(ME);
    int retval = -1;

    when_null_trace(to_fill, exit, ERROR, "No groupdata provided");
    when_null_trace(p_role_paths, exit, INFO, "No groupdata to role paths provided");

    to_fill->role_paths = GET_CHAR(p_role_paths, NULL);
    retval = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return retval;
}

uint32_t get_group_gid(const char* group_path, amxd_status_t* status) {
    SAH_TRACEZ_IN(ME);
    const char* path = STRING_EMPTY(group_path) ? DM_USER_GROUP_PARTICIPATION_DEFAULT:group_path;
    amxd_object_t* group_object = amxd_dm_findf(usermanagement_get_dm(), "%s", path);
    uint32_t id = amxd_object_get_value(uint32_t, group_object, "GroupID", status);
    SAH_TRACEZ_OUT(ME);
    return id;
}

