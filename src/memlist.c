/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "memlist.h"

#include <stdlib.h>
#include <string.h>

#define ME "misc"

amxc_llist_t* memlist_to_amxc_llist(char** list) {
    SAH_TRACEZ_IN(ME);
    amxc_llist_t* memlist = NULL;

    amxc_llist_new(&memlist);

    for(size_t i = 0; list[i] != NULL; i++) {
        amxc_var_t* str_variant;

        amxc_var_new(&str_variant);
        amxc_var_set_type(str_variant, AMXC_VAR_ID_CSTRING);
        amxc_var_set(cstring_t, str_variant, list[i]);
        amxc_llist_append(memlist, &str_variant->lit);
    }

    SAH_TRACEZ_OUT(ME);
    return memlist;
}

char** memlist_from_amxc_llist(const amxc_llist_t* const list) {
    SAH_TRACEZ_IN(ME);
    char** memlist = NULL;
    size_t memlist_len = 0;
    size_t index = 0;

    if(list == NULL) {
        goto exit;
    }

    memlist_len = amxc_llist_size(list);
    memlist = (char**) calloc(memlist_len + 1, sizeof(char*));
    when_null_oom(memlist, exit, "memlist");

    amxc_llist_for_each(it, list) {
        amxc_var_t* str = amxc_var_from_llist_it(it);
        char* member = strdup(GET_CHAR(str, NULL));
        memlist[index] = member;
        index++;
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return memlist;
}
