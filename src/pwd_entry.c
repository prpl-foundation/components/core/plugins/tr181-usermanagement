/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "pwd_entry.h"

#include <stdlib.h>
#include <grp.h>
#include <errno.h>
#include <string.h>
#include <shadow.h>

#define ME "passwd"

#define DEFAULT_HOMEDIR "/var"
#define DEFAULT_SHELL ""

static void pwd_entry_set_pwent(pwd_entry_t* p_this,
                                passwd_sys_t* newpwent,
                                bool should_be_freed) {
    SAH_TRACEZ_IN(ME);
    when_null_trace(p_this, exit, ERROR, "No password entry found");
    if(p_this->m_free_pwent && (p_this->mp_pwent != NULL)) {
        free(p_this->mp_pwent->pw_name);
        free(p_this->mp_pwent->pw_gecos);
        free(p_this->mp_pwent->pw_passwd);
        free(p_this->mp_pwent->pw_dir);
        free(p_this->mp_pwent->pw_shell);
        free(p_this->mp_pwent);
    }
    p_this->mp_pwent = newpwent;
    p_this->m_free_pwent = should_be_freed;

exit:
    SAH_TRACEZ_OUT(ME);
}

static int32_t pwd_entry_entry_get_next(db_entry_t* p_this, bool* p_entry_found) {
    SAH_TRACEZ_IN(ME);
    int32_t retval = -1;
    pwd_entry_t* pwd_entry = (pwd_entry_t*) p_this;

    when_null_trace(p_entry_found, exit, ERROR, "No boolean variable provided");
    *p_entry_found = false;
    errno = 0;
    when_null_trace(pwd_entry, exit, ERROR, "No password entry found");
    pwd_entry_set_pwent(pwd_entry, getpwent(), false);
    when_true_trace((pwd_entry->mp_pwent == NULL) && (errno != 0),
                    exit,
                    ERROR,
                    "Failed to open passwd entry: %s",
                    strerror(errno));
    if(pwd_entry->mp_pwent != NULL) {
        *p_entry_found = true;
    }
    retval = 0;
exit:
    SAH_TRACEZ_OUT(ME);
    return retval;
}

static int32_t pwd_entry_entry_put(db_entry_t* p_this) {
    SAH_TRACEZ_IN(ME);
    int32_t retval = -1;
    pwd_entry_t* pwd_entry = (pwd_entry_t*) p_this;

    when_null_trace(pwd_entry, exit, ERROR, "No password entry found");
    when_failed_trace(putpwent(pwd_entry->mp_pwent, pwd_entry->m_db_entry.mp_output_file),
                      exit,
                      ERROR,
                      "Failed to store passwd database entry: %s",
                      strerror(errno));

    retval = 0;
exit:
    SAH_TRACEZ_OUT(ME);
    return retval;
}

static bool pwd_entry_entry_matches(const db_entry_t* p_this, const amxc_var_t* p_data) {
    SAH_TRACEZ_IN(ME);
    bool rv = false;
    pwd_entry_t* pwd_entry = (pwd_entry_t*) p_this;
    const char* const username = GET_CHAR(p_data, USERNAME_NAME);
    const char* const previous_username = GET_CHAR(p_data, PREVIOUS_USERNAME_NAME);

    when_null_trace(pwd_entry, exit, ERROR, "No password entry found");
    when_null_trace(pwd_entry->mp_pwent, exit, ERROR, "No password structure found");
    when_null_trace(pwd_entry->mp_pwent->pw_name, exit, ERROR, "No password name found");

    if(((username != NULL) && (strcmp(pwd_entry->mp_pwent->pw_name, username) == 0)) ||
       ((previous_username != NULL) && (strcmp(pwd_entry->mp_pwent->pw_name, previous_username) == 0))) {
        rv = true;
        if(!STRING_EMPTY(previous_username)) {
            SAH_TRACEZ_INFO(ME, "Username change from %s to %s", previous_username, username);
        }
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int32_t pwd_entry_entry_update(db_entry_t* p_this, const amxc_var_t* p_data) {
    SAH_TRACEZ_IN(ME);
    pwd_entry_t* pwd_entry = (pwd_entry_t*) p_this;
    passwd_sys_t* new_pwent = NULL;
    uint32_t user_id = GET_UINT32(p_data, USERID_NAME);
    bool data_contains_grp = amxc_var_get_key(p_data,
                                              USERGROUPID_NAME,
                                              AMXC_VAR_FLAG_DEFAULT) != NULL;
    uint32_t group_id = GET_UINT32(p_data, USERGROUPID_NAME);
    const char* const user_name = GET_CHAR(p_data, USERNAME_NAME);
    const char* const home_dir = GET_CHAR(p_data, USERHOMEDIR_NAME);
    const char* const shell = GET_CHAR(p_data, USERSHELL_NAME);

    when_null_trace(pwd_entry, exit, ERROR, "No password entry found");
    new_pwent = (passwd_sys_t*) calloc(1, sizeof(passwd_sys_t));
    when_null_trace(new_pwent, exit, ERROR, "Failed to allocate password structure");

    new_pwent->pw_uid = user_id;
    new_pwent->pw_name = strdup(user_name);
    new_pwent->pw_gecos = strdup(user_name);
    new_pwent->pw_passwd = strdup("x");

    if(home_dir != NULL) {
        new_pwent->pw_dir = strdup(home_dir);
    } else {
        new_pwent->pw_dir = strdup(pwd_entry->mp_pwent->pw_dir);
    }

    if(data_contains_grp) {
        new_pwent->pw_gid = group_id;
    } else {
        new_pwent->pw_gid = pwd_entry->mp_pwent->pw_gid;
    }

    if(shell != NULL) {
        new_pwent->pw_shell = strdup(shell);
    } else {
        new_pwent->pw_shell = strdup(pwd_entry->mp_pwent->pw_shell);
    }

    pwd_entry_set_pwent(pwd_entry, new_pwent, true);

exit:
    SAH_TRACEZ_OUT(ME);
    return 0;
}

static int32_t pwd_entry_entry_create_new(db_entry_t* p_this, const amxc_var_t* p_data) {
    SAH_TRACEZ_IN(ME);
    pwd_entry_t* pwd_entry = (pwd_entry_t*) p_this;
    passwd_sys_t* new_pwent = NULL;
    uint32_t user_id = GET_UINT32(p_data, USERID_NAME);
    uint32_t group_id = GET_UINT32(p_data, USERGROUPID_NAME);
    const char* const user_name = GET_CHAR(p_data, USERNAME_NAME);
    const char* const home_dir = GET_CHAR(p_data, USERHOMEDIR_NAME);
    const char* const shell = GET_CHAR(p_data, USERSHELL_NAME);

    when_null_trace(pwd_entry, exit, ERROR, "No password entry found");
    new_pwent = (passwd_sys_t*) calloc(1, sizeof(passwd_sys_t));
    when_null_trace(new_pwent, exit, ERROR, "Failed to allocate password structure");

    new_pwent->pw_uid = user_id;
    new_pwent->pw_name = strdup(user_name);
    new_pwent->pw_gecos = strdup(user_name);
    new_pwent->pw_gid = group_id;
    new_pwent->pw_passwd = strdup("x");

    if(home_dir != NULL) {
        new_pwent->pw_dir = strdup(home_dir);
    } else {
        new_pwent->pw_dir = strdup(DEFAULT_HOMEDIR);
    }

    if(shell != NULL) {
        new_pwent->pw_shell = strdup(shell);
    } else {
        new_pwent->pw_shell = strdup(DEFAULT_SHELL);
    }

    pwd_entry_set_pwent(pwd_entry, new_pwent, true);

exit:
    SAH_TRACEZ_OUT(ME);
    return 0;
}

static int32_t pwd_entry_stream_open(UNUSED db_entry_t* p_this) {
    SAH_TRACEZ_IN(ME);

    setpwent();

    SAH_TRACEZ_OUT(ME);
    return 0;
}

static int32_t pwd_entry_stream_close(UNUSED db_entry_t* p_this) {
    SAH_TRACEZ_IN(ME);

    endpwent();

    SAH_TRACEZ_OUT(ME);
    return 0;
}

static int32_t pwd_entry_stream_reset(UNUSED db_entry_t* p_this) {
    SAH_TRACEZ_IN(ME);

    setpwent();

    SAH_TRACEZ_OUT(ME);
    return 0;
}

static void pwd_entry_clean(db_entry_t* p_this) {
    SAH_TRACEZ_IN(ME);
    pwd_entry_t* pwd_entry = (pwd_entry_t*) p_this;

    when_null_trace(pwd_entry, exit, ERROR, "No password entry found");
    db_entry_clean(p_this);

    endpwent();
    pwd_entry_set_pwent(pwd_entry, NULL, false);

    ulckpwdf();

exit:
    SAH_TRACEZ_OUT(ME);
}

static const vtable_t pwd_vtable = {
    .entry_get_next = &pwd_entry_entry_get_next,
    .entry_put = &pwd_entry_entry_put,
    .entry_matches = &pwd_entry_entry_matches,
    .entry_update = &pwd_entry_entry_update,
    .entry_create = &pwd_entry_entry_create_new,
    .stream_open = &pwd_entry_stream_open,
    .stream_close = &pwd_entry_stream_close,
    .stream_reset = &pwd_entry_stream_reset,
    .clean = &pwd_entry_clean
};

static int32_t pwd_entry_init(pwd_entry_t* p_this, const char* const p_filename) {
    SAH_TRACEZ_IN(ME);
    int32_t retval = -1;
    when_null_trace(p_this, exit, ERROR, "No password entry found");
    when_failed_trace(db_entry_init((db_entry_t*) p_this, p_filename),
                      exit,
                      ERROR,
                      "Failed to initialize database entry");

    lckpwdf();

    p_this->mp_pwent = NULL;
    p_this->m_free_pwent = false;

    p_this->m_db_entry.mp_vtable = &pwd_vtable;

    retval = 0;
exit:
    SAH_TRACEZ_OUT(ME);
    return retval;
}

pwd_entry_t* pwd_entry_new(const char* const p_filename) {
    SAH_TRACEZ_IN(ME);
    pwd_entry_t* new_entry = (pwd_entry_t*) calloc(1, sizeof(pwd_entry_t));

    when_null_oom(new_entry, exit, "new_entry");

    if(pwd_entry_init(new_entry, p_filename) != 0) {
        free(new_entry);
        new_entry = NULL;
        SAH_TRACEZ_ERROR(ME, "Failed to initialize new password entry");
        goto exit;
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return new_entry;
}
