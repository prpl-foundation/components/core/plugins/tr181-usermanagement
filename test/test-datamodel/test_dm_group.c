/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "test_datamodel.h"
#include "test_environment.h"
#include "dm_usermanagement.h"

#define ME "dm"

static const user_dm_object_t test_user_dm_object = {
    .m_alias = "testusr",
    .m_index = 8,
    .m_user_id = 666,
    .m_username = "test-user",
    .m_password = "",
    .m_hashed_password = "",
    .m_group_participation = DEFAULT_GROUP_PATH,
    .m_role_participation = DEFAULT_ROLE_PATH,
    .m_shell = DEFAULT_SHELL_PATH,
    .m_homedir = "/",
    .m_enable = true,
    .m_static_user = false,
    .m_language = "",
};

static const userdata_t test_user = {
    .username = "test-user",
    .previous_username = NULL,
    .user_id = 666,
    .password = "",
    .hashed_password = "",
    .home_dir = "/",
    .role_paths = DEFAULT_ROLE_PATH,
    .shell_path = "/bin/bash",
    .primary_group_id = 666,
    .all_group_ids.size = 0,
    .all_group_ids.list = NULL,
};

static const group_dm_object_t test_edit_group_dm_object = {
    .m_index = 78,
    .m_alias = "testgrp",
    .m_group_id = 666,
    .m_group_name = "test-group",
    .m_enable = true,
    .m_static_group = false,
    .m_role_participation = DEFAULT_ROLE_PATH,
};

static const groupdata_t test_edit_group = {
    .groupname = "test-group",
    .previous_groupname = NULL,
    .group_id = 666,
    .role_paths = DEFAULT_ROLE_PATH,
};

static amxd_object_t* dm_get_group_instance(const char* groupname) {
    amxd_object_t* groups = NULL;
    amxd_object_t* group = NULL;

    when_null(groupname, exit);
    groups = amxd_dm_findf(get_test_datamodel(), "Users.Group.");
    when_null_trace(groups, exit, INFO, "Group object is NULL");
    group = amxd_object_findf(groups, "[" "Groupname" " == '%s']", groupname);
    when_null_trace(group, exit, INFO, "Failed to find user with Groupname = %s", groupname);
exit:
    return group;
}

void test_add_group(UNUSED void** state) {
    groupdata_t group_data;
    group_dm_object_t group_dm_object;
    amxd_object_t* p_group_dm_obj = NULL;
    amxd_object_t* group = NULL;

    group_data.group_id = 987;
    group_data.groupname = "test-group";
    group_data.role_paths = "Users.Role.test-role.";

    group_dm_object.m_alias = "testgrp";
    group_dm_object.m_index = 5;
    group_dm_object.m_group_id = group_data.group_id;
    group_dm_object.m_group_name = group_data.groupname;
    group_dm_object.m_enable = true;
    group_dm_object.m_static_group = false;
    group_dm_object.m_role_participation = "Users.Role.test-role.";

    add_group_to_dm(get_test_datamodel(), &group_dm_object, &p_group_dm_obj);
    amxc_var_t* p_event_variant = convert_group_to_amxc_var(&group_dm_object, true);

    will_return(__wrap_sysconf_add_group, &group_data);
    will_return(__wrap_sysconf_add_group, 0);

    _sync_group_to_system("dm:instance-added", p_event_variant, NULL);

    will_return(__wrap_sysconf_del_group, &group_data);
    will_return(__wrap_sysconf_del_group, 0);

    group = dm_get_group_instance("test-group");

    assert_string_equal("Users.Role.test-role.", GET_CHAR(amxd_object_get_param_value(group, DM_ROLE_PARTICIPATION_NAME), NULL));

    amxd_object_delete(&p_group_dm_obj);
    _del_group_from_system("dm:instance-removed", p_event_variant, NULL);
    amxc_var_delete(&p_event_variant);
}

void test_add_group_with_empty_role(UNUSED void** state) {
    groupdata_t group_data;
    group_dm_object_t group_dm_object;
    amxd_object_t* p_group_dm_obj = NULL;
    amxd_object_t* group = NULL;

    group_data.group_id = 987;
    group_data.groupname = "test-group";
    group_data.role_paths = "";

    group_dm_object.m_alias = "testgrp";
    group_dm_object.m_index = 5;
    group_dm_object.m_group_id = group_data.group_id;
    group_dm_object.m_group_name = group_data.groupname;
    group_dm_object.m_enable = true;
    group_dm_object.m_static_group = false;
    group_dm_object.m_role_participation = "";

    add_group_to_dm(get_test_datamodel(), &group_dm_object, &p_group_dm_obj);
    amxc_var_t* p_event_variant = convert_group_to_amxc_var(&group_dm_object, true);

    will_return(__wrap_sysconf_add_group, &group_data);
    will_return(__wrap_sysconf_add_group, 0);

    _sync_group_to_system("dm:instance-added", p_event_variant, NULL);

    will_return(__wrap_sysconf_del_group, &group_data);
    will_return(__wrap_sysconf_del_group, 0);

    group = dm_get_group_instance("test-group");

    assert_string_equal("Users.Role.untrusted-role.", GET_CHAR(amxd_object_get_param_value(group, DM_ROLE_PARTICIPATION_NAME), NULL));

    amxd_object_delete(&p_group_dm_obj);
    _del_group_from_system("dm:instance-removed", p_event_variant, NULL);
    amxc_var_delete(&p_event_variant);
}

void test_edit_group_name(UNUSED void** state) {
    amxd_object_t* p_group_dm_obj = NULL;
    groupdata_t edited_group = test_edit_group;
    amxc_var_t* p_event_variant = NULL;
    group_dm_object_t edited_group_dm_object = test_edit_group_dm_object;

    edited_group.groupname = "plop";
    edited_group_dm_object.m_group_name = "plop";
    add_group_to_dm(get_test_datamodel(), &edited_group_dm_object, &p_group_dm_obj);

    p_event_variant = convert_group_to_amxc_var_edit_string(&test_edit_group_dm_object,
                                                            "Groupname",
                                                            "test-group",
                                                            "plop");
    will_return(__wrap_sysconf_edit_group, &edited_group);
    will_return(__wrap_sysconf_edit_group, 0);

    _edit_group_name_on_system("dm:object-changed", p_event_variant, NULL);

    amxd_object_delete(&p_group_dm_obj);
    amxc_var_delete(&p_event_variant);
}

void test_edit_group_role(UNUSED void** state) {
    amxd_object_t* p_group_dm_obj = NULL;
    groupdata_t edited_group = test_edit_group;
    amxc_var_t* p_event_variant = NULL;
    group_dm_object_t edited_group_dm_object = test_edit_group_dm_object;

    edited_group.role_paths = "Users.Role.new-role.";
    edited_group_dm_object.m_role_participation = "Users.Role.new-role.";
    add_group_to_dm(get_test_datamodel(), &edited_group_dm_object, &p_group_dm_obj);

    p_event_variant = convert_group_to_amxc_var_edit_string(&test_edit_group_dm_object,
                                                            "RoleParticipation",
                                                            DEFAULT_ROLE_PATH,
                                                            "Users.Role.new-role.");
    will_return(__wrap_sysconf_edit_group, &edited_group);
    will_return(__wrap_sysconf_edit_group, 0);

    _edit_group_role_participation_on_system("dm:object-changed", p_event_variant, NULL);

    amxd_object_delete(&p_group_dm_obj);
    amxc_var_delete(&p_event_variant);
}

void test_edit_group_id(UNUSED void** state) {
    amxd_object_t* p_group_dm_obj = NULL;
    groupdata_t edited_group = test_edit_group;
    amxc_var_t* p_event_variant = NULL;
    group_dm_object_t edited_group_dm_object = test_edit_group_dm_object;

    setup_default_dm(get_test_datamodel());

    // add group
    edited_group.group_id = 777;
    edited_group_dm_object.m_group_id = 777;
    add_group_to_dm(get_test_datamodel(), &edited_group_dm_object, &p_group_dm_obj);

    // add a user that belong to the group
    gid_t nogroup_gid_list[1] = {777};
    amxd_object_t* p_user_dm_obj = NULL;
    userdata_t expected_test_user = test_user;
    expected_test_user.primary_group_id = 777;
    expected_test_user.all_group_ids.size = 1;
    expected_test_user.all_group_ids.list = (gid_t*) &nogroup_gid_list;

    user_dm_object_t user_dm_object = test_user_dm_object;

    user_dm_object.m_group_participation = "Users.Group.testgrp";

    add_user_to_dm(get_test_datamodel(), &user_dm_object, &p_user_dm_obj);

    p_event_variant = convert_group_to_amxc_var_edit_string(&test_edit_group_dm_object,
                                                            "GroupID",
                                                            "666",
                                                            "777");
    will_return(__wrap_sysconf_edit_group, &edited_group);
    will_return(__wrap_sysconf_edit_group, 0);

    will_return(__wrap_sysconf_edit_user, &expected_test_user);
    will_return(__wrap_sysconf_edit_user, 0);

    _edit_group_id_on_system("dm:object-changed", p_event_variant, NULL);

    amxd_object_delete(&p_user_dm_obj);
    amxd_object_delete(&p_group_dm_obj);
    amxc_var_delete(&p_event_variant);
}
