/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "sysconf.h"

#include "dm_usermanagement.h"
#include "usermanagement_util.h"
#include "db_entry.h"
#include "grp_entry.h"
#include "pwd_entry.h"
#include "sdw_entry.h"
#include "user_entry.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <shadow.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <errno.h>

#define ME "sysconf"

#define PASSWD_FILENAME "/etc/passwd"
#define SHADOW_FILENAME "/etc/shadow"
#define GROUP_FILENAME "/etc/group"
#define TEMP_PASSWD_FILENAME "/etc/passwd.new"
#define TEMP_SHADOW_FILENAME "/etc/shadow.new"
#define TEMP_GROUP_FILENAME "/etc/group.new"
#define BACKUP_PASSWD_FILENAME "/etc/passwd.backup"
#define BACKUP_SHADOW_FILENAME "/etc/shadow.backup"
#define BACKUP_GROUP_FILENAME "/etc/group.backup"

typedef enum _operation_t {
    EDIT,
    DELETE,
    CREATE
} operation_t;

typedef struct passwd passwd_sys_t;
typedef struct spwd spwd_sys_t;
typedef struct group group_sys_t;

static const struct stat default_stat = {
    .st_mode = S_IFREG | 0600,
    .st_uid = 0,
    .st_gid = 0,
    .st_nlink = 1,
    .st_size = 0,
};

static int32_t iterate_through_db(db_entry_t* p_db_entry,
                                  const amxc_var_t* p_update_data,
                                  operation_t op) {
    SAH_TRACEZ_IN(ME);
    int32_t retval = -1;
    bool entry_available;
    bool matching_entry_found = false;

    when_null_trace(p_db_entry, exit, ERROR, "No database entry provided");
    retval = db_entry_stream_open(p_db_entry);
    when_failed_trace(retval, exit, ERROR, "Failed to open original database stream");
    retval = db_entry_entry_get_next(p_db_entry, &entry_available);
    when_failed_trace(retval, exit, ERROR, "Failed to open original database entry");

    while(entry_available) {
        bool updating_matching_entry = false;
        if(db_entry_entry_matches(p_db_entry, p_update_data)) {
            matching_entry_found = true;
            updating_matching_entry = true;
            if((op == EDIT) || (op == CREATE)) {
                retval = db_entry_entry_update(p_db_entry, p_update_data);
                when_failed_trace(retval, exit, ERROR, "Failed to update database entry");
            }
        }
        if(!updating_matching_entry || (op != DELETE)) {
            retval = db_entry_entry_put(p_db_entry);
            when_failed_trace(retval, exit, ERROR, "Failed to store database entry");
        }
        retval = db_entry_entry_get_next(p_db_entry, &entry_available);
        when_failed_trace(retval, exit, ERROR, "Failed to open original database entry");
    }
    if(!matching_entry_found && (op == CREATE)) {
        retval = db_entry_entry_create_new(p_db_entry, p_update_data);
        when_failed_trace(retval, exit, ERROR, "Failed to create a new database entry");
        retval = db_entry_entry_put(p_db_entry);
        when_failed_trace(retval, exit, ERROR, "Failed to store database entry");
    }
    retval = 0;

exit:
    db_entry_stream_close(p_db_entry);
    SAH_TRACEZ_OUT(ME);
    return retval;
}

static bool sysconf_contains_gid(gid_t tocheck, gid_list_t gid_list) {
    SAH_TRACEZ_IN(ME);
    bool rv = false;

    when_null(gid_list.list, exit);
    for(size_t i = 0; i < gid_list.size; i++) {
        if(gid_list.list[i] == tocheck) {
            rv = true;
            goto exit;
        }
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int32_t copy_file(const char* src_path, const char* dest_path, const struct stat* file_stat) {
    SAH_TRACEZ_IN(ME);
    int32_t retval = -1;
    FILE* src_file = NULL;
    FILE* dest_file = NULL;
    size_t bytes_read;
    char buffer[512];

    when_str_empty_trace(src_path, exit, ERROR, "No source path provided");
    when_str_empty_trace(dest_path, exit, ERROR, "No destination path provided");
    when_null_trace(file_stat, exit, ERROR, "No file stat given");

    src_file = fopen(src_path, "r");
    when_null_trace(src_file, exit, ERROR, "Failed to open the source %s during copy (%s)", src_path, strerror(errno));

    dest_file = fopen(dest_path, "w");
    when_null_trace(dest_file, exit, ERROR, "Failed to open the destination %s during copy (%s)", dest_path, strerror(errno));

    while((bytes_read = fread(buffer, 1, sizeof(buffer), src_file)) > 0) {
        when_true_trace(fwrite(buffer, 1, bytes_read, dest_file) != bytes_read,
                        exit, ERROR,
                        "Error writing to destination %s file during copy (%s)", dest_path, strerror(errno));
    }

    when_true_trace(ferror(src_file), exit, ERROR, "Error reading from source file %s (%s)", dest_path, strerror(errno));

    when_failed_trace(fchown(fileno(dest_file), file_stat->st_uid, file_stat->st_gid), exit, ERROR,
                      "Failed to change owner %s during copy (%s)", dest_path, strerror(errno));

    when_failed_trace(fchmod(fileno(dest_file), file_stat->st_mode), exit, ERROR,
                      "Failed to set file permissions %s during copy (%s)", src_path, strerror(errno));
    retval = 0;

exit:

    if(src_file != NULL) {
        fclose(src_file);
    }
    if(dest_file != NULL) {
        fclose(dest_file);
    }

    SAH_TRACEZ_OUT(ME);
    return retval;
}

static bool sysconf_grp_memlist_contains_user(const amxc_llist_t* p_member_list,
                                              const userdata_t* p_user_data,
                                              amxc_llist_it_t** pp_found_llist_it) {
    SAH_TRACEZ_IN(ME);
    bool rv = false;
    *pp_found_llist_it = NULL;

    when_null_trace(p_user_data, exit, ERROR, "No user data provided");
    when_null_trace(pp_found_llist_it, exit, ERROR, "No link list pointer provided");

    amxc_llist_for_each(mit, p_member_list) {
        amxc_var_t* p_member_variant = amxc_var_from_llist_it(mit);
        const char* p_member_string = GET_CHAR(p_member_variant, NULL);
        if(!STRING_EMPTY(p_member_string) &&
           !STRING_EMPTY(p_user_data->username) &&
           (strcmp(p_member_string, p_user_data->username) == 0)) {
            *pp_found_llist_it = mit;
            rv = true;
            goto exit;
        }
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static bool sysconf_db_entry_matches_any(const db_entry_t* p_db_entry,
                                         const amxc_llist_t* p_param_list,
                                         amxc_llist_it_t** pp_found_matching_llist_it) {
    SAH_TRACEZ_IN(ME);
    bool rv = false;

    when_null_trace(p_db_entry, exit, ERROR, "No database entry provided");
    when_null_trace(pp_found_matching_llist_it, exit, ERROR, "No link list pointer provided");

    *pp_found_matching_llist_it = NULL;
    amxc_llist_for_each(it, p_param_list) {
        amxc_var_t* p_param_var = amxc_var_from_llist_it(it);

        if(db_entry_entry_matches(p_db_entry, p_param_var)) {
            *pp_found_matching_llist_it = it;
            rv = true;
            goto exit;
        }
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int32_t sysconf_grp_entry_set_memlist(grp_entry_t* p_group_entry,
                                             const amxc_llist_t* p_new_memlist,
                                             amxc_var_t* p_data_skeleton) {
    SAH_TRACEZ_IN(ME);
    int32_t retval = -1;
    char* group_name = NULL;

    when_null_trace(p_group_entry, exit, ERROR, "No group entry provided");
    when_null_trace(p_group_entry->mp_grent, exit, ERROR, "No mp_grent found");

    group_name = p_group_entry->mp_grent->gr_name;

    amxc_var_add_key(cstring_t, p_data_skeleton, GROUPNAME_NAME, group_name);
    amxc_var_add_key(amxc_llist_t, p_data_skeleton, GROUPMEMLIST_NAME, p_new_memlist);

    retval = db_entry_entry_update((db_entry_t*) p_group_entry, p_data_skeleton);

exit:
    SAH_TRACEZ_OUT(ME);
    return retval;
}

static void remove_user_from_grp_memlist(amxc_llist_t* p_group_memlist, const char* username_to_remove) {
    when_null(p_group_memlist, exit);
    when_str_empty(username_to_remove, exit);

    amxc_llist_for_each(user_it, p_group_memlist) {
        amxc_var_t* p_user = amxc_var_from_llist_it(user_it);
        if((p_user != NULL) && (strcmp(GET_CHAR(p_user, NULL), username_to_remove) == 0)) {
            amxc_llist_it_take(user_it);
            variant_list_it_free(user_it);
            break;
        }
    }
exit:
    return;
}

static int32_t sysconf_update_group_entry(grp_entry_t* p_group_entry,
                                          const userdata_t* p_userdata,
                                          amxc_llist_t* p_skeleton_list) {
    SAH_TRACEZ_IN(ME);
    int32_t retval = -1;
    db_entry_t* p_db_entry = (db_entry_t*) p_group_entry;
    amxc_llist_t* p_group_memlist = NULL;
    amxc_llist_it_t* p_memlist_matching_entry = NULL;
    amxc_llist_it_t* p_skeleton_list_matching_entry = NULL;
    amxc_var_t* p_data_var = NULL;
    bool memlist_contains_user = false;
    bool user_part_of_group = false;

    when_null_trace(p_group_entry, exit, ERROR, "No group entry provided");
    when_null_trace(p_group_entry->mp_grent, exit, ERROR, "No mp_grent found");
    when_null_trace(p_group_entry->mp_grent->gr_mem, exit, ERROR, "No memlist found");

    p_group_memlist = memlist_to_amxc_llist(p_group_entry->mp_grent->gr_mem);
    memlist_contains_user = sysconf_grp_memlist_contains_user(p_group_memlist,
                                                              p_userdata,
                                                              &p_memlist_matching_entry);
    user_part_of_group = sysconf_db_entry_matches_any(p_db_entry,
                                                      p_skeleton_list,
                                                      &p_skeleton_list_matching_entry);
    if(user_part_of_group && !memlist_contains_user) {
        amxc_var_t* username_variant = NULL;
        p_data_var = amxc_var_from_llist_it(p_skeleton_list_matching_entry);
        amxc_llist_it_take(p_skeleton_list_matching_entry);
        amxc_var_new(&username_variant);
        amxc_var_set_type(username_variant, AMXC_VAR_ID_CSTRING);
        amxc_var_set(cstring_t, username_variant, p_userdata->username);

        // Username has changed remove it
        if(!STRING_EMPTY(p_userdata->previous_username)) {
            remove_user_from_grp_memlist(p_group_memlist, p_userdata->previous_username);
        }

        amxc_llist_append(p_group_memlist, &username_variant->lit);
        retval = sysconf_grp_entry_set_memlist(p_group_entry, p_group_memlist, p_data_var);
        when_failed_trace(retval, exit, ERROR, "Failed to update the memberlist of a User's group");
    } else if(!user_part_of_group && memlist_contains_user) {
        amxc_var_t* member_variant = NULL;
        amxc_llist_it_take(p_memlist_matching_entry);
        member_variant = amxc_var_from_llist_it(p_memlist_matching_entry);
        amxc_var_delete(&member_variant);

        amxc_var_new(&p_data_var);
        amxc_var_set_type(p_data_var, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(uint32_t, p_data_var, GROUPID_NAME, p_group_entry->mp_grent->gr_gid);

        retval = sysconf_grp_entry_set_memlist(p_group_entry, p_group_memlist, p_data_var);
        when_failed_trace(retval, exit, ERROR, "Failed to update the memberlist of a User's group");
    }

    retval = db_entry_entry_put(p_db_entry);
    when_failed_trace(retval, exit, ERROR, "Failed to store group entry");

exit:
    amxc_var_delete(&p_data_var);
    amxc_llist_delete(&p_group_memlist, &delete_variant_from_llist_it);
    SAH_TRACEZ_OUT(ME);
    return retval;
}

static int32_t sysconf_update_user_groups(const userdata_t* p_userdata,
                                          const char* filename,
                                          operation_t op) {
    SAH_TRACEZ_IN(ME);
    int32_t retval = -1;
    amxc_llist_t* datalist = NULL;
    grp_entry_t* p_group_entry = NULL;
    db_entry_t* p_db_entry = NULL;
    bool entry_available = false;
    struct stat group_file_stat = default_stat;

    when_failed_trace(stat(GROUP_FILENAME, &group_file_stat), exit, ERROR,
                      "Failed original group file doesn't exist (%s)", strerror(errno));

    if((op != DELETE) && (p_userdata->all_group_ids.list == NULL)) {
        when_failed_trace(copy_file(GROUP_FILENAME, filename, &group_file_stat),
                          exit,
                          ERROR,
                          "Failed to copy original Group file to new Group file: %s",
                          strerror(errno));
        retval = 0;
        goto exit;
    }

    amxc_llist_new(&datalist);
    for(size_t i = 0; i < p_userdata->all_group_ids.size; i++) {
        gid_t group_id = p_userdata->all_group_ids.list[i];
        amxc_var_t* group_data_variant;
        amxc_var_new(&group_data_variant);
        amxc_var_set_type(group_data_variant, AMXC_VAR_ID_HTABLE);

        amxc_var_add_key(uint32_t, group_data_variant, GROUPID_NAME, group_id);

        amxc_llist_append(datalist, &group_data_variant->lit);
    }

    p_group_entry = grp_entry_new(filename);
    when_null_trace(p_group_entry, exit, ERROR, "Failed to create new group entry");
    p_db_entry = (db_entry_t*) p_group_entry;

    retval = db_entry_stream_open(p_db_entry);
    when_failed_trace(retval, exit, ERROR, "Failed to open original group database stream");
    retval = db_entry_entry_get_next(p_db_entry, &entry_available);
    when_failed_trace(retval, exit, ERROR, "Failed to get next group database entry");
    while(entry_available) {
        retval = sysconf_update_group_entry(p_group_entry, p_userdata, datalist);
        when_failed_trace(retval, exit, ERROR, "Failed to update group database entry");

        retval = db_entry_entry_get_next(p_db_entry, &entry_available);
        when_failed_trace(retval, exit, ERROR, "Failed to get next group database entry");
    }

    db_entry_stream_close(p_db_entry);

exit:
    db_entry_delete(p_db_entry);
    amxc_llist_delete(&datalist, &delete_variant_from_llist_it);
    SAH_TRACEZ_OUT(ME);
    return retval;
}

static void sysconf_fill_userdata_variant(const userdata_t* p_user_data,
                                          amxc_var_t* const p_data) {
    SAH_TRACEZ_IN(ME);
    if((p_user_data == NULL) || (p_data == NULL)) {
        goto exit;
    }
    amxc_var_add_key(cstring_t, p_data, USERNAME_NAME, p_user_data->username);
    amxc_var_add_key(cstring_t, p_data, PREVIOUS_USERNAME_NAME, p_user_data->previous_username);

    amxc_var_add_key(uint32_t, p_data, USERID_NAME, p_user_data->user_id);
    if(p_user_data->hashed_password != NULL) {
        amxc_var_add_key(cstring_t, p_data, PASSWORD_NAME, p_user_data->hashed_password);
    }
    if(p_user_data->all_group_ids.list != NULL) {
        amxc_var_add_key(uint32_t, p_data, USERGROUPID_NAME, p_user_data->primary_group_id);
    }
    if(p_user_data->shell_path != NULL) {
        amxc_var_add_key(cstring_t, p_data, USERSHELL_NAME, p_user_data->shell_path);
    }
    if(p_user_data->home_dir != NULL) {
        amxc_var_add_key(cstring_t, p_data, USERHOMEDIR_NAME, p_user_data->home_dir);
    }

    amxc_var_add_key(bool, p_data, ENABLE_NAME, p_user_data->enable);

exit:
    SAH_TRACEZ_OUT(ME);
}

static int32_t sysconf_count_user_matches(const userdata_t* p_user_data,
                                          uint32_t* const p_count) {
    SAH_TRACEZ_IN(ME);
    int32_t retval = -1;
    amxc_var_t data_variant;
    pwd_entry_t* p_passwd_entry = NULL;
    db_entry_t* p_db_entry = NULL;

    amxc_var_init(&data_variant);
    amxc_var_set_type(&data_variant, AMXC_VAR_ID_HTABLE);
    sysconf_fill_userdata_variant(p_user_data, &data_variant);

    p_passwd_entry = pwd_entry_new(NULL);
    when_null_trace(p_passwd_entry, exit, ERROR, "Failed to create new password entry");
    p_db_entry = (db_entry_t*) p_passwd_entry;
    db_entry_stream_open(p_db_entry);

    retval = db_entry_matching_entry_count(p_db_entry, &data_variant, p_count);
    when_failed_trace(retval, exit, ERROR, "Failed to count amount of matching password entries");

exit:
    db_entry_delete(p_db_entry);
    amxc_var_clean(&data_variant);
    SAH_TRACEZ_OUT(ME);
    return retval;
}

static int32_t sysconf_check_user_attributes(const userdata_t* const user_data,
                                             bool* const attrs_valid) {
    SAH_TRACEZ_IN(ME);
    int32_t retval = -1;
    grp_entry_t* p_group_entry = NULL;
    db_entry_t* p_db_entry = NULL;
    amxc_var_t gid_htable;
    amxc_var_init(&gid_htable);

    when_null_trace(attrs_valid, exit, ERROR, "No boolean variable provided");
    *attrs_valid = false;
    when_null_trace(user_data, exit, ERROR, "No userdata provided");

    if((user_data->shell_path != NULL) && (access(user_data->shell_path, F_OK) != 0)) {
        SAH_TRACEZ_WARNING(ME, "Shell path  %s isn't accessible (user %s)", user_data->shell_path, user_data->username);
        goto exit_success;
    }

    if(user_data->all_group_ids.list == NULL) {
        *attrs_valid = true;
        goto exit_success;
    }

    if(!sysconf_contains_gid(user_data->primary_group_id, user_data->all_group_ids)) {
        SAH_TRACEZ_WARNING(ME, "gid %u isn't found on system for user %s", user_data->primary_group_id, user_data->username);
        goto exit_success;
    }

    amxc_var_set_type(&gid_htable, AMXC_VAR_ID_HTABLE);

    p_group_entry = grp_entry_new(NULL);
    when_null_trace(p_group_entry, exit, ERROR, "Failed to create new group entry for user %s", user_data->username);
    p_db_entry = (db_entry_t*) p_group_entry;

    retval = db_entry_stream_open(p_db_entry);
    when_failed_trace(retval, exit, ERROR, "Failed to open Group entry stream for user %s", user_data->username);

    for(size_t i = 0; i < user_data->all_group_ids.size; i++) {
        amxc_var_t* to_del = NULL;
        gid_t current_gid = user_data->all_group_ids.list[i];
        bool group_found = false;
        amxc_var_add_key(uint32_t, &gid_htable, GROUPID_NAME, current_gid);

        retval = db_entry_matches_any_entry(p_db_entry, &gid_htable, &group_found);
        when_failed_trace(retval, exit, ERROR, "Failed to loop through Group entries for user %s", user_data->username);

        when_false(group_found, exit_success);

        to_del = amxc_var_take_key(&gid_htable, GROUPID_NAME);
        amxc_var_delete(&to_del);
    }

    *attrs_valid = true;

exit_success:
    retval = 0;
exit:
    db_entry_delete(p_db_entry);
    amxc_var_clean(&gid_htable);
    SAH_TRACEZ_OUT(ME);
    return retval;
}

static bool sysconf_check_userdata(const userdata_t* const user_data, operation_t op) {
    SAH_TRACEZ_IN(ME);
    bool retval = false;
    int32_t rv = -1;
    uint32_t user_match_count = 0;
    bool user_attrs_valid = true;

    when_true_status(op == DELETE, exit, retval = true);

    rv = sysconf_count_user_matches(user_data, &user_match_count);
    when_failed_trace(rv, exit, ERROR, "Failed to check existence of User");

    when_true_trace((op == EDIT) && (user_match_count != 1u), exit, INFO, "Edition failed");
    when_true_trace((op == CREATE) && (user_match_count > 1u), exit, INFO, "Creation failed");

    rv = sysconf_check_user_attributes(user_data, &user_attrs_valid);
    when_failed_trace(rv, exit, ERROR, "Failed to check attributes of User");

    when_false(user_attrs_valid, exit);
    retval = true;

exit:
    SAH_TRACEZ_OUT(ME);
    return retval;
}

static int32_t sysconf_count_group_matches(const groupdata_t* const p_group_data,
                                           uint32_t* const p_count) {
    SAH_TRACEZ_IN(ME);
    int32_t retval = -1;
    amxc_var_t group_data;
    grp_entry_t* p_group_entry = NULL;
    db_entry_t* p_db_entry = NULL;

    when_null_trace(p_count, exit, ERROR, "No count parameter provided");
    *p_count = 0;

    amxc_var_init(&group_data);
    amxc_var_set_type(&group_data, AMXC_VAR_ID_HTABLE);

    when_null_trace(p_group_data, exit, ERROR, "No group data provided");

    amxc_var_add_key(uint32_t, &group_data, GROUPID_NAME, p_group_data->group_id);
    amxc_var_add_key(cstring_t, &group_data, GROUPNAME_NAME, p_group_data->groupname);
    amxc_var_add_key(cstring_t, &group_data, PREVIOUS_GROUPNAME_NAME, p_group_data->previous_groupname);

    p_group_entry = grp_entry_new(NULL);
    p_db_entry = (db_entry_t*) p_group_entry;
    when_null_trace(p_group_entry, exit, ERROR, "Failed to create new group entry");
    db_entry_stream_open(p_db_entry);

    when_failed_trace(db_entry_matching_entry_count(p_db_entry, &group_data, p_count),
                      exit,
                      ERROR,
                      "Failed to search for matching group entries")

    retval = 0;
exit:
    SAH_TRACEZ_OUT(ME);
    db_entry_delete(p_db_entry);
    amxc_var_clean(&group_data);
    return retval;
}

bool sysconf_check_groupdata_new(const groupdata_t* const group_data) {
    SAH_TRACEZ_IN(ME);
    bool retval = false;
    uint32_t group_match_count;

    when_failed_trace(sysconf_count_group_matches(group_data, &group_match_count),
                      exit,
                      ERROR,
                      "Failed to check existence of Group");

    retval = group_match_count < 2; // Add is allowed if group does not exist

exit:
    SAH_TRACEZ_OUT(ME);
    return retval;
}

bool sysconf_check_groupdata_edit(const groupdata_t* const group_data) {
    SAH_TRACEZ_IN(ME);
    bool retval = false;
    uint32_t group_match_count;

    when_failed_trace(sysconf_count_group_matches(group_data, &group_match_count),
                      exit,
                      ERROR,
                      "Failed to check existence of Group");

    retval = group_match_count >= 1;

exit:
    SAH_TRACEZ_OUT(ME);
    return retval;
}

bool sysconf_check_groupdata_del(const groupdata_t* const group_to_delete) {
    SAH_TRACEZ_IN(ME);
    bool ok_to_delete = false;
    struct passwd* pwd_entry = NULL;

    when_null_trace(group_to_delete, exit, ERROR, "No group to delete provided");

    setpwent();
    errno = 0;
    while((pwd_entry = getpwent()) != NULL) {
        errno = 0;
        if(pwd_entry->pw_gid == group_to_delete->group_id) {
            goto exit;
        }
    }
    when_true_trace(errno != 0, exit, ERROR, "Failed to get next group database entry");

    ok_to_delete = true;

exit:
    endpwent();
    SAH_TRACEZ_OUT(ME);
    return ok_to_delete;
}

static bool sysconf_check_groupdata(const groupdata_t* const group_data, operation_t op) {
    SAH_TRACEZ_IN(ME);
    bool retval = false;
    switch(op) {
    case CREATE:
        retval = sysconf_check_groupdata_new(group_data);
        break;
    case DELETE:
        retval = sysconf_check_groupdata_del(group_data);
        break;
    case EDIT:
        retval = sysconf_check_groupdata_edit(group_data);
        break;
    default:
        break;
    }
    SAH_TRACEZ_OUT(ME);
    return retval;
}


static int rename_file(const char* src_path, const char* dest_path, const struct stat* file_stat) {
    SAH_TRACEZ_IN(ME);
    int32_t retval = -1;
    FILE* dest_file = NULL;

    when_null_trace(file_stat, exit, ERROR, "No file stat given");

    when_failed_trace(rename(src_path, dest_path), exit, ERROR,
                      "Failed to raname %s during rename (%s)", dest_path, strerror(errno));
    dest_file = fopen(dest_path, "rw");
    when_null_trace(dest_file, exit, ERROR, "Failed to open the destination %s during rename (%s)", dest_path, strerror(errno));

    when_failed_trace(fchmod(fileno(dest_file), file_stat->st_mode), exit, ERROR,
                      "Failed to set file permissions %s during rename (%s)", dest_path, strerror(errno));
    when_failed_trace(fchown(fileno(dest_file), file_stat->st_uid, file_stat->st_gid), exit, ERROR,
                      "Failed to change owner %s during rename (%s)", dest_path, strerror(errno));

    retval = 0;

exit:
    if(dest_file != NULL) {
        fclose(dest_file);
    }
    SAH_TRACEZ_OUT(ME);
    return retval;
}

static int32_t sysconf_user_file_transaction(void) {
    SAH_TRACEZ_IN(ME);
    int32_t retval = -1;
    int32_t success = 0;
    bool dirty = false;
    struct stat passwd_file_stat = default_stat;
    struct stat shadow_file_stat = default_stat;
    struct stat group_file_stat = default_stat;

    when_failed_trace(stat(GROUP_FILENAME, &group_file_stat), exit, ERROR,
                      "Failed original group file doesn't exist (%s)", strerror(errno));
    when_failed_trace(stat(PASSWD_FILENAME, &passwd_file_stat), exit, ERROR,
                      "Failed original passwd file doesn't exist (%s)", strerror(errno));
    when_failed_trace(stat(SHADOW_FILENAME, &shadow_file_stat), exit, ERROR,
                      "Failed original shadow file doesn't exist (%s)", strerror(errno));

    success |= copy_file(PASSWD_FILENAME, BACKUP_PASSWD_FILENAME, &group_file_stat);
    success |= copy_file(SHADOW_FILENAME, BACKUP_SHADOW_FILENAME, &passwd_file_stat);
    success |= copy_file(GROUP_FILENAME, BACKUP_GROUP_FILENAME, &shadow_file_stat);
    when_true_trace(success != 0,
                    exit,
                    ERROR,
                    "Failed to backup the passwd, shadow or group file: %s",
                    strerror(errno));

    dirty = true;
    success |= rename_file(TEMP_PASSWD_FILENAME, PASSWD_FILENAME, &passwd_file_stat);
    success |= rename_file(TEMP_SHADOW_FILENAME, SHADOW_FILENAME, &shadow_file_stat);
    success |= rename_file(TEMP_GROUP_FILENAME, GROUP_FILENAME, &group_file_stat);
    when_true_trace(success != 0,
                    exit,
                    ERROR,
                    "Failed to set new passwd, shadow or group file: %s",
                    strerror(errno));
    dirty = false;

    retval = 0;

exit:
    if(dirty) {
        rename_file(BACKUP_PASSWD_FILENAME, PASSWD_FILENAME, &passwd_file_stat);
        rename_file(BACKUP_SHADOW_FILENAME, SHADOW_FILENAME, &shadow_file_stat);
        rename_file(BACKUP_GROUP_FILENAME, GROUP_FILENAME, &group_file_stat);
    }

    remove(BACKUP_PASSWD_FILENAME);
    remove(BACKUP_SHADOW_FILENAME);
    remove(BACKUP_GROUP_FILENAME);

    SAH_TRACEZ_OUT(ME);
    return retval;
}

static int32_t sysconf_action_user_passwd(const amxc_var_t* const p_user_variant, operation_t op) {
    SAH_TRACEZ_IN(ME);
    int32_t retval = -1;
    pwd_entry_t* p_pwd_entry = NULL;

    p_pwd_entry = pwd_entry_new(TEMP_PASSWD_FILENAME);
    when_null_trace(p_pwd_entry, exit, ERROR, "Failed to create new password entry");
    when_failed_trace(iterate_through_db((db_entry_t*) p_pwd_entry, p_user_variant, op),
                      exit,
                      ERROR,
                      "Iteration through existing passwd entries failed");

    retval = 0;

exit:
    db_entry_delete((db_entry_t*) p_pwd_entry);
    SAH_TRACEZ_OUT(ME);
    return retval;
}

static int32_t sysconf_action_user_shadow(const amxc_var_t* p_user_variant, operation_t op) {
    SAH_TRACEZ_IN(ME);
    int32_t retval = 1;
    sdw_entry_t* p_shadow_entry = NULL;

    p_shadow_entry = sdw_entry_new(TEMP_SHADOW_FILENAME);
    when_null_trace(p_shadow_entry, exit, ERROR, "Failed to create shadow entry");
    when_failed_trace(iterate_through_db((db_entry_t*) p_shadow_entry, p_user_variant, op),
                      exit,
                      ERROR,
                      "Iteration through existing shadow entries failed");

    retval = 0;

exit:
    db_entry_delete((db_entry_t*) p_shadow_entry);
    SAH_TRACEZ_OUT(ME);
    return retval;
}

static int32_t sysconf_action_user(const userdata_t* const user, operation_t op) {
    SAH_TRACEZ_IN(ME);
    int32_t retval = -1;
    bool files_locked = false;
    amxc_var_t* p_user_data_variant = NULL;

    when_false_trace(sysconf_check_userdata(user, op), exit, INFO, "Provided userdata in sysconf_action_user %s was invalid", user->username);

    amxc_var_new(&p_user_data_variant);
    amxc_var_set_type(p_user_data_variant, AMXC_VAR_ID_HTABLE);
    sysconf_fill_userdata_variant(user, p_user_data_variant);

    retval = sysconf_action_user_passwd(p_user_data_variant, op);
    when_failed_trace(retval, exit, ERROR, "Failed to sync User with new passwd file");

    retval = sysconf_action_user_shadow(p_user_data_variant, op);
    when_failed_trace(retval, exit, ERROR, "Failed to sync User with new shadow file");

    retval = lckpwdf();
    when_failed(retval, exit);
    files_locked = true;

    retval = sysconf_update_user_groups(user, TEMP_GROUP_FILENAME, op);
    when_failed_trace(retval, exit, ERROR, "Failed to update Groups of User");
    retval = sysconf_user_file_transaction();
    when_failed_trace(retval, exit, ERROR, "Failed to correctly modify passwd, shadow or group file");

exit:
    amxc_var_delete(&p_user_data_variant);
    if(files_locked) {
        ulckpwdf();
    }
    SAH_TRACEZ_OUT(ME);
    return retval;
}

int32_t sysconf_add_user(const userdata_t* const user) {
    SAH_TRACEZ_IN(ME);
    int32_t rv = ENTRY_EXISTS;
    struct passwd* pw = NULL;
    struct spwd* s_pw = NULL;
    errno = 0;

    when_null_trace(user, exit, ERROR, "No userdata provided");
    pw = getpwnam(user->username);

    if(errno != 0) {
        SAH_TRACEZ_ERROR(ME, "Error while retrieving user pwnam %s : %s", user->username, strerror(errno));
        rv = errno;
        goto exit;
    }

    if(pw != NULL) {
        // Existing user in sysconf. If sysconf is out of sync with dm, dm will take precedence.
        s_pw = getspnam(user->username);

        if(errno != 0) {
            SAH_TRACEZ_ERROR(ME, "Error while retrieving user spnam %s : %s", user->username, strerror(errno));
            rv = errno;
            goto exit;
        }

        if(s_pw == NULL) {
            // It should not appear, it means that /etc/shadow has not been edited through data model
            SAH_TRACEZ_INFO(ME, "Sync User %s exists (no entry found in %s)", user->username, SHADOW_FILENAME);
            rv = sysconf_action_user(user, CREATE);
        } else {
            SAH_TRACEZ_INFO(ME, "Edit User %s exists, editing entry", user->username);
            rv = sysconf_action_user(user, EDIT);
        }
    } else {
        // User doesn't exist in sysconf.
        pw = getpwuid(user->user_id);
        if(pw != NULL) {
            SAH_TRACEZ_WARNING(ME, "Existing user %s exists with the same uid %u as user %s", pw->pw_name, pw->pw_uid, user->username);
        }
        SAH_TRACEZ_INFO(ME, "Create User %s, creating entry", user->username);
        rv = sysconf_action_user(user, CREATE);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int32_t sysconf_edit_user(const userdata_t* const user) {
    SAH_TRACEZ_IN(ME);
    SAH_TRACEZ_INFO(ME, "Edit User %s exists, editing entry", user->username);
    int32_t rv = sysconf_action_user(user, EDIT);

    SAH_TRACEZ_OUT(ME);
    return rv;
}

int32_t sysconf_del_user(const userdata_t* const user_to_delete) {
    SAH_TRACEZ_IN(ME);
    int32_t rv = -1;
    userdata_t emptied_user;

    when_null_trace(user_to_delete, exit, ERROR, "No userdata to delete provided");
    emptied_user = *user_to_delete;

    emptied_user.all_group_ids.size = 0;
    emptied_user.all_group_ids.list = NULL;
    rv = sysconf_action_user(&emptied_user, DELETE);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int32_t sysconf_group_file_transaction(void) {
    SAH_TRACEZ_IN(ME);
    int32_t retval = -1;
    bool dirty = false;
    struct stat group_file_stat = default_stat;

    when_failed_trace(stat(GROUP_FILENAME, &group_file_stat), exit, ERROR,
                      "Failed original group file doesn't exist (%s)", strerror(errno));

    when_failed(copy_file(GROUP_FILENAME, BACKUP_GROUP_FILENAME, &group_file_stat), exit);
    dirty = true;
    when_failed(rename_file(TEMP_GROUP_FILENAME, GROUP_FILENAME, &group_file_stat), exit);
    dirty = false;
    remove(BACKUP_GROUP_FILENAME);
    retval = 0;

exit:
    if(dirty) {
        rename_file(BACKUP_GROUP_FILENAME, GROUP_FILENAME, &group_file_stat);
    }
    SAH_TRACEZ_OUT(ME);
    return retval;
}

static int32_t sysconf_action_group(const groupdata_t* const group, operation_t op) {
    SAH_TRACEZ_IN(ME);
    int32_t retval = -1;
    amxc_var_t* data_variant = NULL;
    grp_entry_t* group_entry = NULL;

    when_null_trace(group, exit, ERROR, "No groupdata provided");

    if(!sysconf_check_groupdata(group, op)) {
        SAH_TRACEZ_INFO(ME, "Provided groupdata in sysconf_action_group was invalid");
        goto exit;
    }

    amxc_var_new(&data_variant);
    amxc_var_set_type(data_variant, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, data_variant, GROUPID_NAME, group->group_id);
    amxc_var_add_key(cstring_t, data_variant, GROUPNAME_NAME, group->groupname);
    amxc_var_add_key(cstring_t, data_variant, PREVIOUS_GROUPNAME_NAME, group->previous_groupname);

    group_entry = grp_entry_new(TEMP_GROUP_FILENAME);
    when_null_trace(group_entry, exit, ERROR, "Failed to create group entry");

    retval = iterate_through_db((db_entry_t*) group_entry, data_variant, op);
    when_failed_trace(retval, exit, ERROR, "Iteration through existing group entries failed");

    retval = sysconf_group_file_transaction();
    when_failed_trace(retval, exit, ERROR, "Failed to correctly modify group file");

exit:
    amxc_var_delete(&data_variant);
    data_variant = NULL;
    db_entry_delete((db_entry_t*) group_entry);
    group_entry = NULL;
    SAH_TRACEZ_OUT(ME);
    return retval;
}

int32_t sysconf_add_group(const groupdata_t* const group) {
    SAH_TRACEZ_IN(ME);
    int32_t rv = -1;
    struct group* gr = NULL;
    errno = 0;

    when_null_trace(group, exit, ERROR, "No groupdata provided");
    gr = getgrnam(group->groupname);
    if((gr != NULL)) {
        if((gr->gr_gid != group->group_id) && (getgrgid(group->group_id) == NULL)) {
            amxp_subproc_t* groupmod = NULL;
            amxc_string_t gid_str;
            struct group* new_gr = NULL;

            amxp_subproc_new(&groupmod);
            amxc_string_init(&gid_str, 0);
            amxc_string_setf(&gid_str, "%u", group->group_id);
            amxp_subproc_start_wait(groupmod, 1000, (char*) "groupmod", "-g", amxc_string_get(&gid_str, 0), group->groupname, NULL);
            new_gr = getgrnam(group->groupname);
            if(new_gr == NULL) {
                SAH_TRACEZ_ERROR(ME, "Group %s after subproc groupmod gid (%u) failed", group->groupname, group->group_id);
            } else {
                SAH_TRACEZ_NOTICE(ME, "Group %s after subproc groupmod gid (%u/%u)", group->groupname, new_gr->gr_gid, group->group_id);
            }
            amxc_string_clean(&gid_str);
            amxp_subproc_delete(&groupmod);
        }
        if(errno == 0) {
            SAH_TRACEZ_INFO(ME, "There already exists a group with name %s and/or id %u", group->groupname, group->group_id);
            rv = ENTRY_EXISTS;
            goto exit;
        } else {
            SAH_TRACEZ_ERROR(ME, "Error while checking if group %s already exists: %s", group->groupname, strerror(errno));
            rv = errno;
            goto exit;
        }
    }
    rv = sysconf_action_group(group, CREATE);
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int32_t sysconf_edit_group(const groupdata_t* const group) {
    SAH_TRACEZ_IN(ME);
    SAH_TRACEZ_INFO(ME, "Edit User %s exists, editing entry", group->groupname);
    int32_t rv = sysconf_action_group(group, EDIT);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int32_t sysconf_del_group(const groupdata_t* const group_to_delete) {
    SAH_TRACEZ_IN(ME);
    int32_t rv = sysconf_action_group(group_to_delete, DELETE);
    SAH_TRACEZ_OUT(ME);
    return rv;
}
