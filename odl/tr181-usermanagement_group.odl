%define {
    select Users {
        /*
         * This object contains parameters relating to the characteristics of a user group.
         *
         * At most one enabled entry in this table can exist with a given value for
         * Groupname, or with a given value for GroupID.
         *
         * @version 1.0
         */
        %persistent object Group[] {
            on action add-inst call add_instance "GroupID";
            on action del-inst call delete_instance_if_false "StaticGroup";

            /*
             * A non-volatile handle used to reference this instance.
             *
             * This is intended only for use in protocol-independent "common" definitions,
             * and MUST NOT be used in protocol-specific definitions.
             *
             * @version 1.0
             */
            %unique %key %persistent string Alias;

            /*
             * Enables/disables this group object instance.
             *
             * If any user which belongs to a Group which becomes disabled is
             * currently accessing the device the the effect on that User should be
             * as if the Group were removed from that User's GroupParticipation list.
             *
             * @version 1.0
             */
            %persistent bool Enable = false;

            /*
             * Unique identifier of the group. Depending on the implementation
             * this can be the unique identifier assigned by the underlying system.
             *
             * @version 1.0
             */
            %persistent uint32 GroupID {
                on action validate call check_uniq "GroupID";
            }

            /*
             * Name of the group. MUST NOT be an empty string for an enabled entry.
             *
             * @version 1.0
             */
            %mutable %unique %key %persistent string Groupname {
                on action validate call check_maximum_length 64;
                on action validate call check_minimum_length 1;
                on action validate call check_uniq "Groupname";
            }

            /*
             * Comma-separated list (maximum list length 1024) of strings.
             * Each list item MUST be the Path Name of a row in the Role table.
             * These Roles are assigned to any User which is member of this Group,
             * for so long as they remain a member of the Group.
             *
             * @version 1.0
             */
            %persistent csv_string RoleParticipation {
                on action validate call check_maximum_length 1024;
                on action validate call csv_values_matches_regexp "^(Device\.)?(Users\.Role\..+)$";
                on action validate call check_is_object_ref { "'Device.Users.'" = "Users." };
                on action write call set_object_ref;
                on action destroy call destroy_object_ref;
            }

            /*
             * A static group is a fixed group that is always available in the system.
             * When set to true, the group cannot be removed.
             *
             * @version 1.0
             */
            %persistent bool StaticGroup = false;

            /**
            * Internal parameter used by the plugin itself to identify if an update has been initiated internally.
            * @version 1.0
            */
            %private bool IsInternalUpdateTrigger = false;
        }
    }
}

%populate {
    on event "dm:instance-added" of "Users.Group." call sync_group_to_system;

    on event "dm:object-changed" call edit_group_name_on_system
        filter 'path matches "Users\.Group\.[0-9]+\.$" &&
                contains("parameters.Groupname")';
    on event "dm:object-changed" call edit_group_id_on_system
        filter 'path matches "Users\.Group\.[0-9]+\.$" &&
                contains("parameters.GroupID")';
    on event "dm:object-changed" call edit_group_role_participation_on_system
        filter 'path matches "Users\.Group\.[0-9]+\.$" &&
                contains("parameters.RoleParticipation")';

    on event "dm:instance-removed" of "Users.Group." call del_group_from_system;
}
