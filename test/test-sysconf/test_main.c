/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "test_sysconf.h"

int main() {
    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup_teardown(test_add_group,
                                        backup_system_files,
                                        restore_system_files),
        cmocka_unit_test_setup_teardown(test_add_user_passwd,
                                        backup_system_files,
                                        restore_system_files),
        cmocka_unit_test_setup_teardown(test_add_user_shadow,
                                        backup_system_files,
                                        restore_system_files),
        cmocka_unit_test_setup_teardown(test_add_user_disabled_shadow,
                                        backup_system_files,
                                        restore_system_files),
        cmocka_unit_test_setup_teardown(test_add_user_group,
                                        backup_system_files,
                                        restore_system_files),
        cmocka_unit_test_setup_teardown(test_add_user_invalid_passwd_shadow,
                                        backup_system_files,
                                        restore_system_files),
        cmocka_unit_test_setup_teardown(test_add_user_disabled_invalid_passwd_shadow,
                                        backup_system_files,
                                        restore_system_files),
        cmocka_unit_test_setup_teardown(test_add_user_same_id_and_name,
                                        backup_system_files,
                                        restore_system_files),
        cmocka_unit_test_setup_teardown(test_add_user_same_id,
                                        backup_system_files,
                                        restore_system_files),
        cmocka_unit_test_setup_teardown(test_change_user_homedir,
                                        backup_system_files,
                                        restore_system_files),
        cmocka_unit_test_setup_teardown(test_change_user_shell,
                                        backup_system_files,
                                        restore_system_files),
        cmocka_unit_test_setup_teardown(test_change_user_groups,
                                        backup_system_files,
                                        restore_system_files),
        cmocka_unit_test_setup_teardown(test_change_user_password,
                                        backup_system_files,
                                        restore_system_files),
        cmocka_unit_test_setup_teardown(test_change_user_empty_password,
                                        backup_system_files,
                                        restore_system_files),
        cmocka_unit_test_setup_teardown(test_change_username,
                                        backup_system_files,
                                        restore_system_files),
        cmocka_unit_test_setup_teardown(test_change_userid,
                                        backup_system_files,
                                        restore_system_files),
        cmocka_unit_test_setup_teardown(test_add_user_nonexistent_shell,
                                        backup_system_files,
                                        restore_system_files),
        cmocka_unit_test_setup_teardown(test_add_user_nonexistent_group,
                                        backup_system_files,
                                        restore_system_files),
        cmocka_unit_test_setup_teardown(test_add_user_primary_group_not_in_grouplist,
                                        backup_system_files,
                                        restore_system_files),
        cmocka_unit_test_setup_teardown(test_change_user_nonexistent_shell,
                                        backup_system_files,
                                        restore_system_files),
        cmocka_unit_test_setup_teardown(test_change_user_nonexistent_groups,
                                        backup_system_files,
                                        restore_system_files),
        cmocka_unit_test_setup_teardown(test_add_incorrect_group,
                                        backup_system_files,
                                        restore_system_files),
    };

    return cmocka_run_group_tests(tests, test_setup_noop, test_teardown_noop);
}