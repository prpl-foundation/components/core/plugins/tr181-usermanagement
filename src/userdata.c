/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "userdata.h"
#include "groupdata.h"

#include "dm_usermanagement.h"
#include "usermanagement.h"
#include "usermanagement_util.h"

#define ME "dm"

void init_userdata(userdata_t* const to_init) {
    SAH_TRACEZ_IN(ME);

    when_null_trace(to_init, exit, ERROR, "No userdata to initialize provided");

    to_init->alias = NULL;
    to_init->user_id = 0;
    to_init->username = NULL;
    to_init->previous_username = NULL;
    to_init->hashed_password = NULL;
    to_init->password = NULL;
    to_init->home_dir = NULL;
    to_init->shell_path = NULL;
    to_init->role_paths = NULL;
    to_init->primary_group_id = 0;
    to_init->all_group_ids.size = 0;
    to_init->all_group_ids.list = NULL;
    to_init->enable = false;

exit:
    SAH_TRACEZ_OUT(ME);
}

void clean_userdata(userdata_t* const to_clean) {
    SAH_TRACEZ_IN(ME);
    when_null(to_clean, exit);

    free(to_clean->all_group_ids.list);
    to_clean->all_group_ids.list = NULL;

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void fill_user_data_from_dm(userdata_t* userdata, amxd_object_t* p_user_object) {

    when_null(userdata, exit);
    when_null(p_user_object, exit);

    userdata->user_id = GET_UINT32(amxd_object_get_param_value(p_user_object, DM_USERID), NULL);
    userdata->username = GET_CHAR(amxd_object_get_param_value(p_user_object, DM_USERNAME), NULL);
    userdata->password = GET_CHAR(amxd_object_get_param_value(p_user_object, DM_PASSWD_NAME), NULL);
    userdata->hashed_password = GET_CHAR(amxd_object_get_param_value(p_user_object, get_hashed_password_path()), NULL);
    userdata->home_dir = GET_CHAR(amxd_object_get_param_value(p_user_object, get_home_directory_path()), NULL);
    userdata->role_paths = GET_CHAR(amxd_object_get_param_value(p_user_object, DM_ROLE_PARTICIPATION_NAME), NULL);
    userdata->enable = GET_BOOL(amxd_object_get_param_value(p_user_object, DM_ENABLE_NAME), NULL);
    fill_userdata_shell(userdata, amxd_object_get_param_value(p_user_object, DM_SHELL_NAME));
    fill_userdata_groups(userdata, amxd_object_get_param_value(p_user_object, DM_GROUP_PARTICIPATION_NAME));
exit:
    return;
}

int32_t fill_userdata_groups(userdata_t* const p_to_fill,
                             const amxc_var_t* const group_path_csvlist) {
    SAH_TRACEZ_IN(ME);
    size_t i = 0;
    int32_t retval = -1;
    amxd_status_t status = amxd_status_ok;
    amxc_var_t* no_group_variant = NULL;
    amxc_llist_t* p_user_groups = amxc_var_dyncast(amxc_llist_t, group_path_csvlist);
    when_null_trace(p_to_fill, exit, ERROR, "No userdata provided");

    free(p_to_fill->all_group_ids.list);
    p_to_fill->all_group_ids.list = NULL;

    if(amxc_llist_is_empty(p_user_groups)) {
        SAH_TRACEZ_INFO(ME, "GroupParticipation of User contains no valid Groups, force nogroup-group");
        if(p_user_groups == NULL) {
            when_failed_trace(amxc_llist_new(&p_user_groups), exit, ERROR, "Failed to allocate p_user_groups");
            when_failed_trace(amxc_llist_init(p_user_groups), exit, ERROR, "Failed to init p_user_groups");
        }
        when_failed_trace(amxc_var_new(&no_group_variant), exit, ERROR, "Failed to allocate no_group_variant");
        when_failed_trace(amxc_var_set(cstring_t, no_group_variant, DM_USER_GROUP_PARTICIPATION_DEFAULT), exit, ERROR, "Failed to set no_group_variant");
        when_failed_trace(amxc_llist_append(p_user_groups, &no_group_variant->lit), exit, ERROR, "Failed to append p_user_groups");
    }

    p_to_fill->all_group_ids.list = (gid_t*) calloc(amxc_llist_size(p_user_groups), sizeof(gid_t));
    when_null_oom(p_to_fill->all_group_ids.list, exit, "all_group_ids.list");

    amxc_llist_iterate(it, p_user_groups) {
        const amxc_var_t* const group_path_variant = amxc_var_from_llist_it(it);
        uint32_t gid = get_group_gid(GET_CHAR(group_path_variant, NULL), &status);
        if(status != amxd_status_ok) {
            SAH_TRACEZ_WARNING(ME, "Failed to fetch ID of Group from datamodel");
            continue;
        }
        p_to_fill->all_group_ids.list[i] = gid;
        i++;
    }

    p_to_fill->all_group_ids.size = i;
    p_to_fill->primary_group_id = p_to_fill->all_group_ids.list[0];
    retval = 0;
exit:
    amxc_llist_delete(&p_user_groups, &delete_variant_from_llist_it);
    if(retval != 0) {
        amxc_var_delete(&no_group_variant);
        if(p_to_fill->all_group_ids.list != NULL) {
            free(p_to_fill->all_group_ids.list);
            p_to_fill->all_group_ids.list = NULL;
        }
    }
    SAH_TRACEZ_OUT(ME);
    return retval;
}

int32_t fill_userdata_shell(userdata_t* p_to_fill, const amxc_var_t* const p_shell_path) {
    SAH_TRACEZ_IN(ME);
    int32_t rv = -1;
    amxd_object_t* p_shell_object = NULL;
    const char* shell_path = NULL;

    when_null_trace(p_to_fill, exit, ERROR, "No userdata provided");

    if(p_shell_path != NULL) {
        shell_path = GET_CHAR(p_shell_path, NULL);
    }

    if(STRING_EMPTY(shell_path)) {
        shell_path = DM_USER_SUPPORTED_SHELL_DEFAULT;
    }

    p_shell_object = amxd_dm_findf(usermanagement_get_dm(), "%s", shell_path);
    p_to_fill->shell_path = GET_CHAR(amxd_object_get_param_value(p_shell_object, "Name"), NULL);
    rv = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}
